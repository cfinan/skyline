"""A central location for constants that are "cross-module". Note that
constants thate are module specific will be located in their respective module
files.
"""
# The names for expected columns in input data frames
PVALUE = "pvalue"
"""The expected name of the p-value column in any input `pandas.DataFrames
(`str`)
"""
CHR_NAME = "chr_name"
"""The expected name of the chromosome column in any input `pandas.DataFrames`
(`str`)
"""
START_POS = "start_pos"
"""The expected name of the start position column in any input
`pandas.DataFrame` (`str`)
"""
POS = "start_pos"
"""The expected name of the start position column in any input
`pandas.DataFrame`, this is an alias for ``START_POS`` (`str`)
"""

LD_FILE_COLUMNS = [
    'source_chr_name', 'source_start_pos', 'source_alleles',
    'ld_chr_name', 'ld_start_pos', 'ld_alleles', 'rsquare'
]
"""Columns in an LD file that has been downloaded from Ensembl and written
"""


GENE_FILE_COLUMNS = [
    'gene_name', 'exon_no', 'chr_name', 'start_pos',
    'end_pos', 'strand', 'is_coding', 'colour'
]
"""Columns in a gene file that has been downloaded from Ensembl and written
"""
