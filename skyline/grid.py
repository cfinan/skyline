"""For managing grids of plots.
"""
from matplotlib import gridspec
from matplotlib.backends.backend_pdf import PdfPages
from skyline import (
    figure,
)
import numpy as np
# import pprint as pp


TOP = 1
"""The code for the top grid position (`int`)
"""
BOTTOM = 2
"""The code for the bottom grid position (`int`)
"""
LEFT = 3
"""The code for the left grid position (`int`)
"""
RIGHT = 4
"""The code for the right grid position (`int`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BaseGrid(object):
    """A wrapper for producing grids of skyline plots. This can be used
    directly.

    Parameters
    ----------
    grid : `matplotlib.gridspec.GridSpec`
        The gridspec that forms the plotting grid.
    canvas : `NoneType` or `backend_agg.FigureCanvas`
        A canvas to pass to the figures when created. This is required as some
        plots need the canvas to measure the size of their components.
    figure_kwargs : `dict` or `NoneType`, optional, default: `NoneType`
        Keyword arguments passed through to the call to
        `skyline.figure.GenomicFigure`. Note that if you do not supply
        ``figsize``, then a default of 8.27x11.69 inches is used (A4 size).
    fillorder : `tuple` of (`int`, `int`)
        The tuple should have a length of two and the `int` should
        correspond to the fill directions `grid.LEFT`, `grid.RIGHT`,
        `grid.TOP`, `grid.BOTTOM`.
    """
    PAGE_WIDTH = 8.27
    """The default page width (`float`)
    """
    PAGE_HEIGHT = 11.69
    """The default page height (`float`)
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, grid, canvas=None, figure_kwargs=None,
                 fillorder=((LEFT, RIGHT), (TOP, BOTTOM))):
        self._grid = grid
        self.gfig = None
        self._canvas = canvas

        # Set up any figure Keyword arguments and the figure size
        self._figure_kwargs = figure_kwargs or dict()
        self._figure_kwargs = {
            **dict(figsize=(self.PAGE_WIDTH, self.PAGE_HEIGHT)),
            **self._figure_kwargs
        }

        # These variables will track the plot positions on the page
        self.row_start = None
        self.col_start = None
        self.current_plot = None
        self.current_page = None
        self.current_row = None
        self.current_col = None
        self._fill_map = None
        self._figure_stack = []

        # Set and validate the filling order
        self.set_fillorder(fillorder[0], fillorder[1])
        self.reset_fillmap()

        # Initialise the figure
        self.init_figure()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        """Initialise from the context manager. Placeholder for interface.

        Returns
        -------
        self : `skyline.grid.BaseGrid`
            The instance.
        """
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        """Initialise from the context manager. Placeholder for interface.

        Parameters
        ----------
        *args
            Errors that have been generated when the context manager exists,
            will be (NoneType, NoneType, NoneType) if exited cleanly.
        """
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self, *args, **kwargs):
        """Placeholder for interface.

        Parameters
        ----------
        *args
            Ignored
        **kwargs
            Ignored
        """
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self, *args, **kwargs):
        """Placeholder for interface.

        Parameters
        ----------
        *args
            Ignored
        **kwargs
            Ignored
        """
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def figure(self):
        """Return the current figure. (`skyline.figure.GenomicFigure`)
        """
        return self._gfig

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def figure_stack(self):
        """Return all the figures that have been created. With the first
        figure at 0 and the current figure at -1.
        (`list` of `skyline.figure.GenomicFigure`)
        """
        return self._figure_stack

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def next_page(self, figure=None):
        """Move on to the next page.

        Parameters
        ----------
        figure : `NoneType` or `skyline.figure.GenomicFigure`, optional,
        default: `NoneType`
            A figure to use, if not supplied then will use all the arguments
            given when the object was created to make a figure.
        """
        old_fig = self._gfig
        self._figure_stack.append(self._gfig)
        self.reset_fillmap()

        if figure is not None:
            self._gfig = figure
        else:
            # Initialise the figure
            self.init_figure()
        return old_fig

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def plot(self, function, *args, subplotspec=None, **kwargs):
        """Plot on to the grid.

        Parameters
        ----------
        function : `func`
            The plotting function to use, the only requirements are that it
            accepts the figure as the first positional argument and that it
            should have a ``tracks`` keyword argument that can accept
            a ``matplotlib.gridspec.SubplotSpec`` object. In practice, the
            plotting function can accept a ``tracks`` keyword argument and then
            call ``skyline.plots.common.get_gridspec``, that will check if the
            tracks argument is ``NoneType``, a ``matplotlib.gridspec.GridSpec``
            or a ``matplotlib.gridspec.SubplotSpec`` object and sort it out for
            you.
        *args
            Any positional arguments to ``function`` (other than the figure).
        subplotspec : `NoneType` or `matplotlib.gridspec.SubplotSpec` or
        `tuple` of `slice`, optional, default: `NoneType`
            The gridspec to plot into, by default, this is a single plotting
            cell in the grid that is incremented for each call to plot.
            However, if you want to have a few oversized plots you can supply
            their dimensions here. They are not allowed to overlap any thing
            that has been plotted previously. In practice, this is very rarely
            used.
        **kwargs
            Any keyword arguments to ``function``.

        See also
        --------
        `skyline.plots.common.get_gridspec`
        """
        # The user has specified a specific grid positions to plot into
        if subplotspec is not None:
            if self.has_overlap(subplotspec) is True:
                raise IndexError("overlapping plots")

            if not isinstance(subplotspec, gridspec.SubplotSpec):
                subplotspec = self._grid[subplotspec[0], subplotspec[1]]
        else:
            subplotspec = self.next_subplot()

        kwargs['tracks'] = subplotspec
        return function(self.figure, *args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def next_subplot(self):
        """Move to the next subplot position. The position that is moved to
        depends on the fillorder and if any plots have been manually placed on
        the grid.

        Returns
        -------
        subplot : `matplotlib.gridspec.SubplotSpec`
            The subplot that the next plot will be placed.
        """
        idx = 0
        while True:
            try:
                self._first_inc_func()
            except IndexError:
                try:
                    self._second_inc_func()
                except IndexError as e:
                    raise IndexError("plot full") from e
                self._first_inc_func()

            try:
                location = bool(
                    self._fill_map[self.current_row, self.current_col]
                )
                subplot = self._grid[self.current_row, self.current_col]
            except (ValueError, TypeError):
                if self.current_row is not None and \
                   self.current_col is not None:
                    raise
                self._second_inc_func()
                location = bool(
                    self._fill_map[self.current_row, self.current_col]
                )
                subplot = self._grid[self.current_row, self.current_col]

            if location is False:
                break
            idx += 1
        return subplot

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def has_overlap(self, subplotspec):
        """Check if the given subplot spec overlaps any previously plotted
        elements.

        Parameters
        ----------
        subplotspec : `matplotlib.gridspec.SubplotSpec` or `tuple` of `slice`
            The subplotspec to check.

        Returns
        -------
        overlap : `bool`
            ``True`` if the ``subplotspec`` overlaps a previous plotted element
            ``False`` if not.
        """
        rows, cols = None, None

        # If a subplot spec has been passed then extract the dimensions into
        # slices
        if isinstance(subplotspec, gridspec.SubplotSpec):
            rows = list(subplotspec.rowspan)
            cols = list(subplotspec.colspan)
            rows = slice(rows[0], rows[-1]+1)
            cols = slice(cols[0], cols[-1]+1)
        else:
            rows = subplotspec[0]
            cols = subplotspec[1]

        if np.sum(self._fill_map[rows, cols]) > 0:
            return True
        return False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_fillorder(self, first_inc, second_inc):
        """Set the automatic fill ordering.

        Parameters
        ----------
        first_inc : `tuple` of `int`
            The axis that will be incremented first.
        second_inc : `tuple` of `int`
            The axis that will be incremented second.

        Notes
        -----
        The ``first_inc`` is the axis that will be incremented first and the
        order that it will be incremented, So it could be
        (``skyliine.grid.LEFT``, ``skyline.grid.RIGHT``) the ``second_inc``, is
        the access that will be incremented when the first axis reaches its
        end positon. So could be (`skyline.grid.TOP`, `skyline.grid.BOTTOM`).
        In which case the fill order would be top-left to bottom right. if we
        swapped them around so ``first_inc`` is
        (`skyline.grid.TOP`, `skyline.grid.BOTTOM`) and ``second_inc`` is
        (`skyline.grid.LEFT`, `skyline.grid.RIGHT`). Then we would still start
        in the top left by fill column-wise instead.

        Please note, that this implementation might be changed in future to a
        single tuple of i.e. (`grid.TOP`, `grid.LEFT`), so the fill order will
        be from top-left to bottom-right as this will be more intuitive.

        Also note, this is not designed to be called after plotting has
        started.

        See also
        --------
        skyline.grid.TOP
        skyline.grid.BOTTOM
        skyline.grid.LEFT
        skyline.grid.RIGHT
        """
        try:
            for start, end in [first_inc, second_inc]:
                total = start + end
                if total != (LEFT + RIGHT) and total != (TOP + BOTTOM):
                    raise ValueError(
                        "increment axis should be grouped in LEFT/RIGHT and"
                        " TOP/BOTTOM and not LEFT/TOP etc..."
                    )
        except ValueError as e:
            if len(first_inc) != 2 or len(second_inc) != 2:
                raise ValueError(
                    "the increment axis should be length 2"
                ) from e
            raise

        # Now set up the functions that will sort out the increment positions
        self._first_inc_func = self._get_fill_func(first_inc)
        self._second_inc_func = self._get_fill_func(second_inc)

        if sum(first_inc) == (LEFT + RIGHT):
            self.row_start = self._get_init_pos(second_inc)
            self.col_start = self._get_init_pos(first_inc)
        else:
            self.row_start = self._get_init_pos(first_inc)
            self.col_start = self._get_init_pos(second_inc)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_fill_func(self, fillorder):
        """Get the function that corresponds to the fillorder.

        Parameters
        ----------
        fillorder : `tuple` of `int`
            The tuple should have a length of two and the `int` should
            correspond to the fill directions `grid.LEFT`, `grid.RIGHT`,
            `grid.TOP`, `grid.BOTTOM`.

        Returns
        -------
        fillorder_func : `function`
            The function that matches the ``fillorder``. This will handle the
            increment/decrement of the row or column numbers.

        Raises
        ------
        ValueError
            If the ``fillorder`` does not correspond to any of the expected
            fillorders ``(LEFT, RIGHT)``, ``(RIGHT, LEFT)``, ``(TOP, BOTTOM)``,
            ``(BOTTOM, TOP)``.
        """
        if fillorder == (LEFT, RIGHT):
            return self._x_pos_left_right
        elif fillorder == (RIGHT, LEFT):
            return self._x_pos_right_left
        elif fillorder == (TOP, BOTTOM):
            return self._y_pos_top_bottom
        elif fillorder == (BOTTOM, TOP):
            return self._y_pos_bottom_top
        else:
            raise ValueError("unknown fill order: '{0}'".format(fillorder))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_init_pos(self, fillorder):
        """Get the row or column initialisation position for the fill order.

        Parameters
        ----------
        fillorder : `tuple` of `int`
            The tuple should have a length of two and the `int` should
            correspond to the fill directions `grid.LEFT`, `grid.RIGHT`,
            `grid.TOP`, `grid.BOTTOM`.

        Returns
        -------
        initial_pos : `int`
            The row or column number where the fill order counter will start.

        Raises
        ------
        ValueError
            If the ``fillorder`` does not correspond to any of the expected
            fillorders ``(skyline.grid.LEFT, skyline.grid.RIGHT)``,
            ``(skyline.grid.RIGHT, skyline.grid.LEFT)``,
            ``(skyline.grid.TOP, skyline.grid.BOTTOM)``,
            ``(skyline.grid.BOTTOM, skyline.grid.TOP)``.
        """
        # TODO: make current_row and current_col property methods and use this
        # to determine the first row/column before a pot has been conducted.
        # This fudge would give a consistent interface with minimal code
        # changes
        if fillorder == (LEFT, RIGHT):
            return 0
        elif fillorder == (RIGHT, LEFT):
            return self._grid.ncols - 1
        elif fillorder == (TOP, BOTTOM):
            return 0
        elif fillorder == (BOTTOM, TOP):
            return self._grid.nrows - 1
        else:
            raise ValueError("unknown fill order: '{0}'".format(fillorder))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def reset_fillmap(self):
        """Reset the grid filled spaces to ``False``.

        This is called when a new page is created and does not have to be
        called by the user.
        """
        # self.current_page = 0
        self.current_row = None
        self.current_col = None
        self._fill_map = np.full(
            (self._grid.nrows, self._grid.ncols), False, dtype=bool
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def init_figure(self):
        """Initialise the figure for a page.
        """
        self._gfig = figure.GenomicFigure(**self._figure_kwargs)

        if self._canvas is not None:
            self._gfig.set_canvas(self._canvas(figure=self._gfig))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _x_pos_left_right(self):
        """Move the x position one increment to the right.

        Raises
        ------
        IndexError
            If the move takes the x position out of range.

        Notes
        -----
        This function will be assigned to either
        ``skyline.grid.BaseGrid._first_inc_func`` or
        ``skyline.grid.BaseGrid._second_inc_func`` depending on the fill order.
        """
        try:
            self.current_col += 1
        except TypeError:
            self.current_col = self.col_start

        try:
            self._fill_map[0, self.current_col]
        except IndexError:
            self.current_col = None
            raise

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _x_pos_right_left(self):
        """Move the x position one increment to the left.

        Raises
        ------
        IndexError
            If the move takes the x position out of range (i.e. < 0).

        Notes
        -----
        This function will be assigned to either
        ``skyline.grid.BaseGrid._first_inc_func`` or
        ``skyline.grid.BaseGrid._second_inc_func`` depending on the fill order.
        """
        try:
            self.current_col -= 1
        except TypeError:
            self.current_col = self.col_start

        if self.current_col < 0:
            self.current_col = None
            raise IndexError("reached left")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _y_pos_top_bottom(self):
        """Move the y position one increment to the bottom.

        Raises
        ------
        IndexError
            If the move takes the y position out of range.

        Notes
        -----
        This function will be assigned to either
        ``skyline.grid.BaseGrid._first_inc_func`` or
        ``skyline.grid.BaseGrid._second_inc_func`` depending on the fill order.
        """
        try:
            self.current_row += 1
        except TypeError:
            self.current_row = self.row_start

        try:
            self._fill_map[self.current_row, 0]
        except IndexError:
            self.current_row = None
            raise

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _y_pos_bottom_top(self):
        """Move the y position one increment to the top.

        Raises
        ------
        IndexError
            If the move takes the y position out of range (i.e. < 0).

        Notes
        -----
        This function will be assigned to either
        ``skyline.grid.BaseGrid._first_inc_func`` or
        ``skyline.grid.BaseGrid._second_inc_func`` depending on the fill order.
        """
        try:
            pass
        except TypeError:
            self.current_row = self.row_start

        if self.current_row < 0:
            self.current_row = None
            raise IndexError("reached top")


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PdfGrid(BaseGrid):
    """A wrapper for producing grids of skyline plots in a multipage PDF.

    Parameters
    ----------
    outfile : `str`
        The output PDF file name.
    *args
        The positional arguments passed to ``skyline.grid.BaseGrid``.
    savefig_kwargs : `dict`, optional, default: `NoneType`
        Any keyword arguments to
        ``matplotlib.backends.backend_pdf.PdfPages.savefig``. This is called
        when moving from one page to the next.
    **kwargs
        Any keyword arguments passed on to ``skyline.grids.BaseGrid``.

    Notes
    -----
    Please note that the ``PdfGrid.current_row`` and ``PdfGrid.current_col``
    attributes will be NoneType before a plot is given. So If you want to check
    if you are on the first plot in a page then use
    ``PdfGrid.current_row is None`` or ``PdfGrid.current_col is None``. I hope
    to change this behaviour in future as this is confusing.

    See also
    --------
    skyline.grid.BaseGrid
    matplotlib.backends.backend_pdf.PdfPages
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, outfile, *args, savefig_kwargs=None, **kwargs):
        self._outfile = outfile
        self._pdf = None
        self._savefig_kwargs = savefig_kwargs or dict()
        super().__init__(*args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def outfile(self):
        """Get the output PDF file name (`str`)
        """
        return self._outfile

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def pdf(self):
        """Get the pdf writer object, this will be ``NoneType`` if not open.
        (`matplotlib.backends.backend_pdf.PdfPages`)
        """
        return self._pdf

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def next_page(self, figure=None):
        """Move to the next page of the PDF document.

        Parameters
        ----------
        figure : `skyline.figure.GenomicFigure`
            A figure object to use in the next page, if not supplied then one
            is created internally. It is rare that you will want to use this.
            However, this may be useful if you want a single page to be a
            different size to what was passed to the constructor.
        """
        self._pdf.savefig(self.figure, **self._savefig_kwargs)
        super().next_page(figure=figure)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self, *args, **kwargs):
        """Open the PDF file for writing.

        Parameters
        ----------
        *args
            Ignored
        **kwargs
            Ignored
        """
        super().open()
        self._pdf = PdfPages(self.outfile)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self, *args, **kwargs):
        """Close the PDF file.

        Parameters
        ----------
        *args
            Ignored
        **kwargs
            Ignored
        """
        super().close()
        if self._pdf is not None:
            self._pdf.savefig(self.figure, **self._savefig_kwargs)
            self._pdf.close()
