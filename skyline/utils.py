"""Skyline utility functions.
"""
from skyline import constants as con
from matplotlib import colors
import numpy as np
import pandas as pd
import pysam
import configparser
import re
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def downscale(data, start_pos_factor=3.5e05, pvalue_factor=0.1,
              chr_name_col='chr_name', start_pos_col='start_pos',
              pvalue_col='pvalue'):
    """Downscale a GWAS dataset, by rounding in the x (position) and y (pvalue)

    Parameters
    ----------
    data : `pandas.DataFrame`
        The input data to work on.
    start_pos : `float`, optional, default: `3.5e05`
        The rounding factor. The start position data is divided by this,
        rounded to 0 digits and then multiplied back up.
    pvalue : `float`, optional, default: `0.1`
        The rounding factor. The pvalue data is divided by this,
        rounded to 0 digits and then multiplied back up.
    chr_name_col : `str`, optional, default: `chr_name`
        The name of the column containing the chromosome name data.
    start_pos_col : `str`, optional, default: `start_pos`
        The name of the column containing the start position data.
    pvalue_col : `str`, optional, default: `pvalue`
        The name of the column containing the p-value data.

    Returns
    -------
    data : `pandas.DataFrame`
        The downscaled dataset. Note that duplicated position/pvlaue data is
        removed prior to returning.
    """
    data = data.reset_index(drop=True)
    data[start_pos_col] = \
        ((data[start_pos_col]/start_pos_factor).round(0) * start_pos_factor).\
        astype(int)
    data[pvalue_col] = \
        (data[pvalue_col]/pvalue_factor).round(0) * pvalue_factor
    return data.drop_duplicates([chr_name_col, start_pos_col, pvalue_col])


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_genomewide_coords(infaa, core_regex=re.compile(r'.+')):
    """Get all the reference coordinate lengths from an indexed fasta file.

    Parameters
    ----------
    infaa : `str`
        The input fasta file. Must be indexed and the index is the same base
        name as ``infaa`` but with an index extension.
    """
    coords = {}
    with pysam.FastaFile(infaa) as faa:
        for i in faa.references:
            core = False
            if core_regex.match(i):
                core = True
            coords[i] = (faa.get_reference_length(i), core, 0)
    return coords


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def write_config_section(conf, section, coords):
    """
    """
    parser = configparser.ConfigParser()
    parser.optionxform = str
    parser.read(conf)
    parser[section] = dict(
        [(k, "{0},{1},{2}".format(v[0], str(v[1]).lower(), v[2])) for k, v in coords.items()]
    )

    with open(conf, 'w') as out:
        parser.write(out)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_config_section(conf, section):
    """
    """
    parser = configparser.ConfigParser()
    parser.optionxform = str
    parser.read(conf)

    coords = {}
    for k, v in parser[section].items():
        length, core, pad = v.split(',')

        if core.lower() == "true":
            core = True
        elif core.lower() == "false":
            core = False
        else:
            raise ValueError("unknown core: {0}".format(core))

        length, pad = int(length), int(pad)
        coords[k] = (length, core, pad)
    return coords


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_listed_cmap(style_obj, lower, upper, breaks):
    """Add a listed colour map to a style object or a dictionary of style
    attributes.

    Parameters
    ----------
    style_obj : `dict` or `skyline.styles.SimpleStyle`
        The styles object to add the column map to.
    lower : `int` or `float`
        The lower bounds of the colour map. Anything between this value and
        the minimum value in breaks is given the color defined in the
        ``style_obj``, the default colour.
    upper : `int` or `float`
        The upper bounds of the colour map. Anything between this value and
        the maximum value in breaks is given the color defined in the
        maximum value of the breaks.
    breaks : `list` of `tuple`
        The breakpoints of the listed color map. Each tuple should have
        (<data_value>, <color>).

    Returns
    -------
    style_obj : `dict`
        A `dict` that is compatible with the `matplotlib.Axes.scatter` and
        has a `matplotlib.colors.ListedColormap` added.

    See also
    --------
    matplotlib.colors.ListedColormap
    """
    breaks.sort(key=lambda x: x[0])
    boundaries = [lower] + [i[0] for i in breaks] + \
        [max(upper, breaks[-1][0])]

    try:
        # Get the base colour for the colour map
        base_color = style_obj.color
    except AttributeError:
        # Must be a dictionary not a style object
        base_color = style_obj['c']

    color_list = [base_color] + [i[1] for i in breaks]

    style_obj['cmap'] = colors.ListedColormap(color_list)
    style_obj['norm'] = colors.BoundaryNorm(
        boundaries, style_obj['cmap'].N, clip=True
    )
    return style_obj


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_top_assocs(*data, pvalue_threshold=8.303, index_ascending=False,
                   **kwargs):
    """Get the pseudo-independent top associations from one or more GWAS
    datasets.

    Parameters
    ----------
    *data : `pandas.DataFrame`
        One of more pandas data frames to get the top hits from. Must have
        ``chr_name``, ``start_pos`` and ``pvalue`` columns.
    pvalue_threshold : `float`
        The p-value threshold to define a top association
    index_ascending : `bool`
        If the pvalues are -log10() transformed then set this to ``True``
    **kwargs
        Keyword arguments to ``get_proximity_clumps``

    Notes
    -----
    If multiple datasets are supplied, the top associated are extracted from
    all of them, then made unique before being processed. Independence is
    determined based on proximity clumping of the data rather than LD clumping
    or conditional analysis. So should only be considered approximate. This
    probably should not be used for anything other than generating a quick
    label set for plotting.
    """
    select_cols = [con.CHR_NAME, con.START_POS, con.PVALUE]
    if index_ascending is True:
        top_hits = pd.concat(
            [i.loc[i[con.PVALUE] >= pvalue_threshold, select_cols]
             for i in data]
        )
    else:
        top_hits = pd.concat(
            [i.loc[i[con.PVALUE] <= pvalue_threshold, select_cols]
             for i in data]
        )

    top_hits = top_hits.sort_values(
        con.PVALUE, ascending=not index_ascending
    ).drop_duplicates()
    top_hits = get_proximity_clumps(
        top_hits, index_ascending=index_ascending, **kwargs
    )
    return top_hits.loc[top_hits.clump_index == True].sort_values(
        [con.CHR_NAME, con.START_POS]
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# def get_nearest_genes(rest_client, species, top_hits, **kwargs):
#     """
#     """
#     nearest_genes = []
#     for i in top_hits[
#             [con.CHR_NAME, con.START_POS]].itertuples(index=False):
#         gene = utils.get_nearest_gene(rest_client, species, i[0], i[1], **kwargs)
#         try:
#             display_name = gene['external_name']
#         except KeyError:
#             display_name = gene['id']

#         nearest_genes.append(
#             (gene['id'], display_name, gene['seq_region_name'], gene['start'],
#              gene['end'], gene['strand'], gene['min_dist'])
#         )
#     return nearest_genes


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_center(start, end):
    """
    """
    if end < start:
        raise ValueError("end > start")
    return int(start + ((end - start)/2))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def index_sort(x, reverse=False):
    """Get the index of each element in a list in sort order.

    Parameters
    ----------
    x : `list`
        The list to rank sort
    reverse : `bool`, optional, default: `False`
        Return the reverse of the rank sort

    Returns
    -------
    index_order : `list` of `int`
        The elements of the list in sort order but expressed as their index
        position in the original list.

    Notes
    -----
    So for example, if you have a list ``['C', 'B', 'A', 'D']``. This would return
    ``[2, 1, 0, 3]``, so the sorted list but expressed as the element number
    in the original list.
    """
    return [j[0] for j in sorted(
        [(i, x[i]) for i in range(len(x))], key=lambda x: x[1], reverse=True
    )]
    # output = [0] * len(inlist)
    # for i, x in enumerate(sorted(range(len(inlist)), key=lambda y: inlist[y])):
    #     output[x] = i
    # return output


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def prop_to_bp(value, length):
    """Evaluate ``value`` to determine if it is a proportion of a base pair
    value and adjust to base pair value (if needed)

    Parameters
    ----------
    value : `int` or `float`
        A positive integer or positive float or can also be 0.
    length : `int`
        The length to use in case value is a proportion. Positive integer

    Returns
    -------
    base_pair_value : `int` or `float`
        The value guaranteed to be in base pairs

    Notes
    -----
    This works as follows: If value is an integer it is treated as a base pair
    value if it is a float it is treated as a proportion.
    """
    if isinstance(value, float):
        return length * value
    elif isinstance(value, int):
        return value
    else:
        raise ValueError("unknown value: {0}".format(value))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def overlap_1d(line1, line2):
    """determine if line1 or line2 overlap in a 1d plane. Note that:
    if max line1 == min line2 (or visa versa) then this is not considered an
    overlap

    Parameters
    ----------
    line1 :`tuple` of `int` or `float`
        The min [0] and max [1] boundaries of the first line
    line2 :`tuple` of `int` or `float`
        The min [0] and max [1] boundaries of the second line

    Returns
    -------
    overlap :`int` or `float`
        The amount of overlap or 0 if there is no overlap
    """
    min1, max1 = line1
    min2, max2 = line2
    return max(0, min(max1, max2) - max(min1, min2))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_proximity_clumps(df, distance=1000000, index_col=con.PVALUE,
                         index_ascending=True, keep_data=False):
    """
    clump variant sites based on the distance that variants are away from each
    other

    Parameters
    ----------

    df : :obj:`DataFrame`
        The data to clump. The index of the data must be unique if
        keep_data=True and the data must contain a chr_name column and a
        start_pos column
    distance :`int` (optional)
        The distance (bp) that any two variants have to be away from each other
        in order to be clumped together. The default is 10000 bp
    index_col :`str` (optional)
        The index column to clump on, this defaults to the p-value column
    index_ascending :`bool` (optional)
        clumping algorthms will sort the data on the index column. This
        indicates if the data should be sorted in acending order, i.e. p-values
        from most to least significant, or deceanding (index_ascending=False),
        i.e. for -log10 transformed p-values. The default is True
    keep_data :`bool` (optional)
        Remove all other columns apart from the chr_name, start_pos and the
        index column (keep_data=False). The default is False
    """

    if keep_data is True and df.index[df.index.duplicated()].size > 0:
        raise RuntimeError(
            "the index must be unique if you are keeping the data intact"
        )

    # Check for duplicates and issue a warning?
    # df.index[df.index.duplicated()].size
    # cols = df.columns.values
    if keep_data is False:
        df = df[[con.CHR_NAME, con.START_POS, index_col]]

    # Get all the unique sites and sort on chr and pos
    df = df.sort_values(
        by=[con.CHR_NAME,
            con.START_POS,
            index_col],
        ascending=[True,
                   True,
                   index_ascending]
    ).drop_duplicates(
        [con.CHR_NAME, con.START_POS]
    ).sort_values(
        [con.CHR_NAME,
         con.START_POS]
    )

    # Get the dfference between adjacent sites
    df['site_diff'] = df[con.START_POS].diff().abs()

    # Determine if any adjacent sites occur on different chromosomes
    df['chr_diff'] = df[con.CHR_NAME] != df[con.CHR_NAME].shift()

    # Make sure that sites that start on a different chromosome are set to Nan
    df.loc[df['chr_diff'] == True, 'site_diff'] = np.nan
    idx = np.flatnonzero(
        (df['site_diff'].isnull()) | (df['site_diff'] > distance)
    )
    df['clump_id'] = 0
    df['clump_lead'] = ""
    df['clump_index'] = False
    for element, i in enumerate(idx):
        start = i
        try:
            end = idx[element+1]
            idx_rows = df.index[start:end]
        except IndexError:
            # The last region
            idx_rows = df.index[start:]

        top_hit = df.loc[idx_rows,
                         index_col].sort_values(
                             ascending=index_ascending).index[0]
        df.loc[idx_rows, 'clump_id'] = i
        df.loc[idx_rows, 'clump_lead'] = top_hit
        df.loc[top_hit, 'clump_index'] = True

    df = df.sort_values(by=['clump_id',
                            index_col],
                        ascending=[True,
                                   index_ascending])
    return df.sort_values(['chr_name', 'start_pos'])
