"""All the feature coordinate objects that are used in Skyline.
"""
import warnings
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class _BaseFeatureCoords(object):
    """The base class methods for feature coords. Do not use directly.

    Parameters
    ----------
    name : `str`, optional, default: `NoneType`
        A name for the feature. If not given then one is assigned.
    styles : `dict` of `dict`, optional, default: `NoneType`
        This holds the styles attached to the feature object. The name of the
        style is given in the dictionary key and the style dictionary is the
        values associated with it. This means that a feature can have multiple
        styles associated with it. If this is not given then a DEFAULT style
        is initialised from <class>.DEFAULT_STYLE`. Each style dict should
        contain parameters appropriate for the feature. Note that any supplied
        styles are merged with the default style.
    artists : `dict`, optional, default: `NoneType`
        If the feature has been used to create an artist this is a slot for it
        for easy access to the artists that is representing the feature. If not
        provided then a blank dict is used.
    """
    DEFAULT_STYLE_KEY = "DEFAULT_STYLE"
    """The default style key name (`str`)
    """
    DEFAULT_STYLE = dict()
    """The default style for the feature (`dict`)
    """
    _FEATURE_NO = 0
    """A class level internal counter, this will be incremented when objects
    of any child classes are created (`int`)
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, name=None, styles=None, artists=None):
        self._name = name
        if name is None:
            self._name = "{1}{0}".format(
                _BaseFeatureCoords._FEATURE_NO,
                self.__class__.__name__
            )
            _BaseFeatureCoords._FEATURE_NO += 1

        # Merge the default style with any incoming styles
        self._style = {
            **{self.DEFAULT_STYLE_KEY: self.DEFAULT_STYLE},
            **(styles or dict())
        }
        self._artist = artists or dict()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __copy__(self):
        """Make a shallow copy of the feature attributes.
        """
        args, kwargs = self.get_args()
        return self.__class__(
            *args, **kwargs
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def copy(self):
        """A method alias for the ``__copy__`` dunder.

        Returns
        -------
        copied_feature : `skyline.coords._BaseFeatureCoords`
            A shallow copy of the feature.
        """
        return self.__copy__()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_args(self):
        """Return all the arguments and keyword arguments for creating a copy.

        Returns
        -------
        args : `list`
            Arguments that can be used to make a copy.
        kwargs : `dict`
            Keyword arguments that can be used to make a copy.
        """
        return list(), dict(
            name=self._name,
            styles=self.styles.copy(),
            artists=self._artist
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def center_coord(start_pos, end_pos):
        """Return the center position for the coords.

        Parameters
        ----------
        start_pos : `int`
            The start position in base pairs.
        end_pos : `int`
            The end position in base pairs.

        Returns
        -------
        center : `int`
            The center position for the coords (start + half the difference
            between end and start)
        """
        return min(end_pos, start_pos) + (abs(end_pos - start_pos)/2)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def start_coord(start_pos, end_pos):
        """Return the start position for the coords.

        Parameters
        ----------
        start_pos : `int`
            The start position in base pairs.
        end_pos : `int`
            The end position in base pairs.

        Returns
        -------
        start_pos : `int`
            The start position for the coords (min).
        """
        return min(end_pos, start_pos)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def end_coord(start_pos, end_pos):
        """Return the end position for the coords.

        Parameters
        ----------
        start_pos : `int`
            The start position in base pairs.
        end_pos : `int`
            The end position in base pairs.

        Returns
        -------
        end_pos : `int`
            The end position for the coords (max).
        """
        return max(end_pos, start_pos)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def name(self):
        """The feature name (`str`).
        """
        return self._name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def style(self):
        """Get the default style (`dict`).

        Notes
        -----
        The default style is the style dict under the ``DEFAULT_STYLE`` key.
        """
        return self._style[self.DEFAULT_STYLE_KEY]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def styles(self):
        """The feature style, must be appropriate matplotlib arguemnts for the
        artist that the feature represents (`dict` of `dict`).

        Notes
        -----
        This is not often used, instead specific attributes are created for
        styles associated with features. These are attempted to be accessed by
        Track Axes that known what they will be called and are able to handle
        the errors if they are not present.
        """
        return self._style

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @styles.setter
    def styles(self, style):
        """Merge a style dictionary with existing styles. The incoming dict
        has priority.
        """
        self._style = {**self._style, **style}

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_style(self, name, style):
        """Add a style to the style dict.

        Parameters
        ----------
        name : `str`
            The name of the style being added.
        style : `Any`
            The styling parameters.

        Raises
        ------
        KeyError
            Please be aware the KeyError in this case is raised if the style
            already exists, not if it does not exist.

        Notes
        -----
        This is an alternative way to add a style to the feature. You can add
        via ``FeatureCoords.style[name]`` if you wish. This will overwrite any
        existing styles. However, use this method if you want to make sure that
        you are adding to an empty style slot.
        """
        if name in self._style:
            raise KeyError(f"style already defined: {name}")
        self._style[name] = style

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def artists(self):
        """An artist that the feature is representing (`matplotlib.Artist`)

        Notes
        -----
        This is not often used, instead specific attributes are created for
        styles associated with features. These are attempted to be accessed by
        Track Axes that known what they will be called and are able to handle
        the errors if they are not present.
        """
        return self._artist

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_artist(self, name, artist):
        """Add an artist to the artist dict.

        Parameters
        ----------
        name : `str`
            The name of the artist being added.
        artist : `matplotlib.Artist`
            The artist to add.

        Raises
        ------
        KeyError
            Please be aware the KeyError in this case is raised if the artist
            already exists, not if it does not exist.

        Notes
        -----
        This is an alternative way to add a artist to the feature. You can add
        via ``FeatureCoords.artists[name]`` if you wish. This will overwrite
        any existing artists with that name. However, use this method if you
        want to make sure that you are adding to an empty artist slot.
        """
        if name in self._artist:
            raise KeyError(f"artist already defined: {name}")
        self._artist[name] = artist

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @artists.setter
    def artists(self, artists):
        self._artist = {**self._artist, **artists}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class FeatureCoords(_BaseFeatureCoords):
    """A representation of the coordinates of a feature on a skyline plot.

    Parameters
    ----------
    chr_name : `str`
        The chromosome name of the feature
    start_pos : `int`
        The start position of the feature.
    *args
        Any arguments to be passed to the parent class.
    end_pos : `int` or `NoneType`, optional, default : `start_pos`
        The end position of the feature, if not provided will default to the
        start position.
    strand : `int`, optional, default: `1`
        The strand of the feature, defaults to the positive strand.
    coords_mapper : `coords.ChrCoords` or `coords.GenomewideCoords`
        A coordinate mapper to transform the feature coordinates to a different
        genomic scale used in the plot. This can be used by calling the
        ``.plot_start_pos``, ``.plot_end_pos`` and ``.plot_center_pos``
        methods.
    label : `str`, optional, default: ``
        A label to be associated with the feature. The default is an empty
        string.
    label_len : `int`, optional, default: `50`
        A maximum length of a label when ``FeatureCoords.display_label`` is
        called.
    label_oversize : `str`, optional, default: `...`
        Characters to be added to the end ``FeatureCoords.display_label`` in
        the event that the label's length is greater than ``label_len``.
    *kwargs
        Any keyword arguments to be passed to the parent class.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, chr_name, start_pos, *args, end_pos=None, strand=1,
                 coords_mapper=None, label="", label_len=50,
                 label_oversize="...", **kwargs):
        super().__init__(*args, **kwargs)
        self._chr_name = None
        self._start_pos = None
        self._end_pos = None
        self._strand = None
        self._label = label
        self._label_len = label_len
        self._label_oversize = label_oversize

        self._coords_mapper = coords_mapper
        self.chr_name = chr_name
        self._start_pos = int(start_pos)

        if end_pos is not None:
            self._end_pos = int(end_pos)

            if end_pos < self._start_pos:
                raise ValueError("end position must be >= than start position")

        self.strand = strand

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __len__(self):
        """Length in basepairs.
        """
        return self.end_pos - self.start_pos + 1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing.
        """
        coords = \
            f"{self.chr_name}:{self.start_pos}-{self.end_pos}|{self.strand}"
        return "<{0}({1})>".format(self.__class__.__name__, coords)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_args(self):
        """Return all the arguments and keyword arguments for creating a copy.

        Returns
        -------
        args : `list`
            Arguments that can be used to make a copy.
        kwargs : `dict`
            Keyword arguments that can be used to make a copy.
        """
        super_args, super_kwargs = super().get_args()

        return (
            super_args + [self.chr_name, self.start_pos],
            {
                **super_kwargs, **dict(
                    end_pos=self._end_pos,
                    coords_mapper=self._coords_mapper,
                    strand=self._strand,
                    label=self._label,
                    label_len=self._label_len,
                    label_oversize=self._label_oversize
                )
            }
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def chr_name(self):
        """The chromosome name (`str`)
        """
        return self._chr_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @chr_name.setter
    def chr_name(self, chr_name):
        self._chr_name = str(chr_name)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def start_pos(self):
        """The start position (`int`)
        """
        return self._start_pos

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @start_pos.setter
    def start_pos(self, start_pos):
        self._start_pos = int(start_pos)

        if self._start_pos > self.end_pos:
            raise ValueError("start position must be <= than end position")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def end_pos(self):
        """The end position (`int`)
        """
        # If end pos is None return start
        return self._end_pos or self._start_pos

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @end_pos.setter
    def end_pos(self, end_pos):
        try:
            self._end_pos = int(end_pos)
            if self._end_pos < self.start_pos:
                raise ValueError("end position must be >= than start position")
        except TypeError:
            if end_pos is not None:
                raise
            self._end_pos = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def strand(self):
        """The strand (`str`)
        """
        return self._strand

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @strand.setter
    def strand(self, strand):
        if strand != 1 and strand != -1:
            raise ValueError("strand must be 1 or -1")
        self._strand = strand

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def center_pos(self):
        """The center position, i.e. start + (end-start/2) (`int`).
        """
        return self.center_coord(self.start_pos, self.end_pos)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_pos(self, start, end):
        """Set the start and the end position at the same time.

        Parameters
        ----------
        start : `int`
            The start position, must be <= to the end position.
        end : `int`
            The end position, must be >= to the start position.
        """
        if end < start:
            raise ValueError("end position must be >= than start position")
        self._start_pos = start
        self._end_pos = end

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def plot_start_pos(self):
        """The start position expressed according to any coordinates mapper
        that has been associated with the feature (`int`).

        Raises
        ------
        TypeError
            If a ``coords_mapper`` object is not defined.
        """
        try:
            return self._coords_mapper.get_coords(
                self.chr_name, self.start_pos
            )
        except AttributeError as e:
            if self._coords_mapper is None:
                raise TypeError(
                    "coord mapping not defined, needs to be for plot "
                    "coordinate calls"
                ) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def plot_end_pos(self):
        """The end position expressed according to any coordinates mapper
        that has been associated with the feature.(`int`).

        Raises
        ------
        TypeError
            If a ``coords_mapper`` object is not defined.
        """
        try:
            return self._coords_mapper.get_coords(
                self.chr_name, self.end_pos
            )
        except AttributeError as e:
            if self._coords_mapper is None:
                raise TypeError(
                    "coord mapping not defined, needs to be for plot "
                    "coordinate calls"
                ) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def plot_center_pos(self):
        """The center position expressed  according to any coordinates mapper
        that has been associated with the feature (`int`).
        """
        return self.center_coord(self.plot_start_pos, self.plot_end_pos)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_coords_mapper(self, coords_mapper):
        """Add a coordinate mapping object.

        Parameters
        ----------
        coords_mapper : `coords.ChrCoords` or `coords.GenomewideCoords`
            A coordinate mapper to transform the feature coordinates to a
            different genomic scale used in the plot. This can be used by
            calling the ``.plot_start_pos``, ``.plot_end_pos`` and
            ``.plot_center_pos`` methods.

        Raises
        ------
        AttributeError
            If a ``coords_mapper`` object is already associated with the
            feature.

        Notes
        -----
        Please note that once a ``coords_mapper`` is associated with the
        feature then in can't be implicitly overwritten, rather,
        ``FeatureCoords.delete_coords_mapper`` should be called first.
        """
        if self._coords_mapper is None:
            # Make sure the coordinate mapper chromosomes contain the feature
            if coords_mapper.has_chromosome(self.chr_name) is False:
                raise KeyError(
                    "feature not in coordinate mapper chromosomes:"
                    " {0}".format(self.chr_name)
                )
            self._coords_mapper = coords_mapper
        else:
            raise AttributeError(
                "coords mapper already set, you must explicitly delete"
                " unwanted mappers"
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def delete_coords_mapper(self):
        """Remove a coordinate mapping object from the feature (`str`).
        """
        self._coords_mapper = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def coords_mapper(self):
        """The coordinates mapper object (if available) inheriting from...
        (`skyline.coords.PassthroughCoords`)
        """
        # If end pos is None return start
        return self._coords_mapper

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def label(self):
        """The label (`str`)
        """
        # If end pos is None return start
        return self._label

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @label.setter
    def label(self, label):
        self._label = label

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def display_label(self):
        """The display label this is guarenteed to be
        ``FeatureCoords.label_len`` long (`str`)
        """
        label_len = self._label_len - len(self._label_oversize)
        label = self.label

        if len(label) <= self._label_len:
            return label

        # Oversize
        return (
            label[:self._label_len - len(self._label_oversize)] +
            self._label_oversize
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class Variant(FeatureCoords):
    """A representation of the coordinates and style of a genetic variant.

    Parameters
    ----------
    chr_name : `str`
        The chromosome name of the variant.
    start_pos : `int`
        The start position of the variant.
    ref_allele : `str`
        The reference allele of the variant.
    alt_allele : `str`
        The alternate allele of the variant.
    *args
        Any additional arguments passed to the parent class.
    **kwargs
        Any additional keyword arguments passed to the parent class.

    Notes
    -----
    The end position is governed by the length of the reference allele and
    can't be changed.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, chr_name, start_pos, ref_allele, alt_allele, *args,
                 **kwargs):
        end_pos = start_pos + len(ref_allele) - 1
        super().__init__(chr_name, start_pos, *args, end_pos=end_pos,
                         **kwargs)
        self._ref_allele = ref_allele
        self._alt_allele = alt_allele
        self._uni_id = "{0}_{1}_{2}_{3}".format(
            chr_name, start_pos, *(sorted([ref_allele, alt_allele]))
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing.
        """
        coords = \
            f"{self.chr_name}:{self.start_pos}-{self.end_pos}|{self.strand}"
        return "<{0}({1}, ref_allele={2}, alt_allele={3})>".format(
            self.__class__.__name__, coords, self.ref_allele, self.alt_allele
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_args(self):
        """Return all the arguments and keyword arguments for creating a copy.

        Returns
        -------
        args : `list`
            Arguments that can be used to make a copy.
        kwargs : `dict`
            Keyword arguments that can be used to make a copy.
        """
        super_args, super_kwargs = super().get_args()

        return (
            super_args + [self.ref_allele, self.alt_allele],
            super_kwargs
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def end_pos(self):
        """The end position (`int`)
        """
        return super().end_pos

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @end_pos.setter
    def end_pos(self, end_pos):
        raise ValueError("variant end is controlled by ref_allele length")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_pos(self, start, end):
        """Set the start and the end position at the same time.

        Parameters
        ----------
        start : `int`
            The start position, must be <= to the end position.
        end : `int`
            The end position, must be >= to the start position.
        """
        raise ValueError("variant end is controlled by ref_allele length")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def ref_allele(self):
        """The reference allele. (`str`)
        """
        return self._ref_allele

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def alt_allele(self):
        """The alternate allele. (`str`)
        """
        return self._alt_allele

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def uni_id(self):
        """Create a universal ID uni_id for a variant.

        Parameters
        ----------
        chr_name : `str`
            The chomosome name.
        start_pos : `int`
            The start position.
        *alleles
            The alleles for the variant.

        Returns
        -------
        uni_id : `str`
            The universal identifier for the variant. That is:
            <chr_name>_<start_pos>_<allele1>_<allele2> where allele1 and
            allele2 are the ref/alt in sort order.
        """
        return self._uni_id


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class Exon(FeatureCoords):
    """A representation of the coordinates and style of a genetic variant.

    Parameters
    ----------
    *args
        Any arguments passed to the parent class.
    utr_region : `tuple`, optional, default: `NoneType`
        The coordinates of the UTR region of the exon, if ``NoneType``, then
        then no UTR region is defined and the whole exon boundary is assumed
        to be coding. The UTR region bounds must be located within the exon
        bounds. The tuple should be ``(start_integer, end_integer)``.
    **kwargs
        Any keyword arguments passed to the parent class.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, utr_region=None, **kwargs):
        self._utr_region = None
        super().__init__(*args, **kwargs)
        self.utr_region = utr_region

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing.
        """
        coords = \
            f"{self.chr_name}:{self.start_pos}-{self.end_pos}|{self.strand}"
        return "<{0}({1}, utr_region={2}, coding_region={3})>".format(
            self.__class__.__name__, coords, self.utr_region,
            self.coding_region
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_args(self):
        """Return all the arguments and keyword arguments for creating a copy.

        Returns
        -------
        args : `list`
            Arguments that can be used to make a copy.
        kwargs : `dict`
            Keyword arguments that can be used to make a copy.
        """
        super_args, super_kwargs = super().get_args()

        return (
            super_args,
            {**super_kwargs, **dict(utr_region=self.utr_region)}
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def coding_region(self):
        """Return the coding region for the exon. If no coding region exists
        this will be ``NoneType`` otherwise (`int`, `int`).
        """
        return self._coding_region

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def utr_region(self):
        """Return the UTR region for the exon. If no UTR exists this will be
        ``NoneType`` otherwise (`int`, `int`).
        """
        return self._utr_region

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @utr_region.setter
    def utr_region(self, utr_region):
        """Set the coding region for the exon.

        Parameters
        ----------
        utr_region : `tuple` of (`int`, `int`) or ``NoneType``
            The UTR region. If ``NoneType`` then the whole exon bounds is
            considered coding.
        """
        coding_region = None
        utr_region = None
        if utr_region is None:
            self._coding_region = (self.start_pos, self._end_pos)
            return
        elif utr_region[1] < utr_region[0]:
            raise ValueError("UTR region end pos < start pos")
        elif utr_region[0] < self.start_pos or utr_region[1] > self.end_pos:
            raise ValueError("UTR region is outside of exon")

        # UTR is aligned with the start pos of the exon
        if utr_region[0] == self.start_pos:
            self._coding_region = (utr_region[1], self.end_pos)
        elif utr_region[1] == self.end_pos:
            # UTR is aligned with the end pos of the exon
            self._coding_region = (self.start_pos, utr_region[0])
        else:
            warnings.warn(
                "UTR region not at start/end of exon, can't handle this yet"
            )
        # If the UTR is the whole exon then make the coding region None
        if self._coding_region[1] - self._coding_region[0] == 0:
            self._coding_region = None
        self._utr_region = utr_region


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class SimpleTranscript(FeatureCoords):
    """A representation of the coordinates and style of a transcript.

    Parameters
    ----------
    *args
        Any arguments passed to the parent class.
    trans_id : `str`, optional, default: ``
        Any formal IDs for the transcript, this is distinct from the label but
        is used as a label if no label is given to the feature.
    biotype : `str`, optional, default: ``
        The biotype of the transcript.
    **kwargs
        Any keyword arguments passed to the parent class.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, trans_id="", biotype=None, **kwargs):
        self.trans_id = trans_id
        self.biotype = biotype

        super().__init__(*args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing.
        """
        coords = \
            f"{self.chr_name}:{self.start_pos}-{self.end_pos}|{self.strand}"
        return (
            "<{0}({1}, label={2}, trans_id={3}, n_exons={4}, biotype={5})>".
            format(
                self.__class__.__name__, coords, self.label, self.trans_id,
                len(self.exons), self.biotype
            )
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_args(self):
        """Return all the arguments and keyword arguments for creating a copy.

        Returns
        -------
        args : `list`
            Arguments that can be used to make a copy.
        kwargs : `dict`
            Keyword arguments that can be used to make a copy.
        """
        super_args, super_kwargs = super().get_args()

        return (
            super_args,
            {
                **super_kwargs,
                **dict(
                    trans_id=self.trans_id,
                    biotype=self.biotype
                )
            }
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def label(self):
        """The label (`str`)
        """
        # if the label is not set then we use the ID as the label
        if super().label == "":
            return self.trans_id
        return super().label

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @label.setter
    def label(self, label):
        self._label = label

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def exons(self):
        """Whilst this does not have any exons this interface is provided, so
        it can be handled by exon plotting patches.
        (`list` of `skyline.features.Exon`)
        """
        return [
            Exon(self.chr_name, self.start_pos, end_pos=self.end_pos,
                 coords_mapper=self.coords_mapper)
        ]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class ExonTranscript(SimpleTranscript):
    """A representation of the coordinates and style of a transcript.

    Parameters
    ----------
    *args
        Any arguments passed to the parent class.
    is_coding : `bool`, optional, default: `True`
        Is the exon a coding exon.
    **kwargs
        Any keyword arguments passed to the parent class.

    Notes
    -----
    The start/end position is governed by the exons and can't be changed.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, exons, *args, **kwargs):
        self._exons = []
        self._add_exons(exons)
        self._check_exons
        chr_name, start_pos, end_pos = self.get_bounds()
        super().__init__(chr_name, start_pos, *args, end_pos=end_pos, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_args(self):
        """Return all the arguments and keyword arguments for creating a copy.

        Returns
        -------
        args : `list`
            Arguments that can be used to make a copy.
        kwargs : `dict`
            Keyword arguments that can be used to make a copy.
        """
        super_args, super_kwargs = super().get_args()

        exon_copy = [i.copy() for i in self.exons]
        return (
            super_args,
            {
                **super_kwargs,
                **dict(exons=exon_copy)
            }
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def start_pos(self):
        """The start position (`int`)
        """
        return super().start_pos

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @start_pos.setter
    def start_pos(self, end_pos):
        raise ValueError(
            "ExonTranscript start is controlled by the exons"
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def end_pos(self):
        """The end position (`int`)
        """
        return super().end_pos

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @end_pos.setter
    def end_pos(self, end_pos):
        raise ValueError(
            "ExonTranscript end is controlled by the exons"
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def exons(self):
        """Return exon features. (`list` of `skyline.features.Exon`)
        """
        return self._exons

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_pos(self, start, end):
        """Set the start and the end position at the same time.

        Parameters
        ----------
        start : `int`
            The start position, must be <= to the end position.
        end : `int`
            The end position, must be >= to the start position.
        """
        raise ValueError("ExonTranscript start/end is controlled by the exons")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_exons(self, exons):
        """Add exons to the transcript.

        Parameters
        ----------
        exons : `list` of `skyline.features.Exon`
            The exon features to add.

        Raises
        ------
        KeyError
            If the exons are on different chromosomes.
        IndexError
            If exon coordinates overlap.
        """
        self._add_exons(exons)
        self._check_exons
        chr_name, start_pos, end_pos = self.get_bounds()
        super().set_pos(start_pos, end_pos)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_coords_mapper(self, coords_mapper):
        """Add a coordinate mapping object to the transcript and the exons.

        Parameters
        ----------
        coords_mapper : `coords.ChrCoords` or `coords.GenomewideCoords`
            A coordinate mapper to transform the feature coordinates to a
            different genomic scale used in the plot. This can be used by
            calling the ``.plot_start_pos``, ``.plot_end_pos`` and
            ``.plot_center_pos`` methods.

        Raises
        ------
        AttributeError
            If a ``coords_mapper`` object is already associated with the
            feature.

        Notes
        -----
        Please note that once a ``coords_mapper`` is associated with the
        feature then in can't be implicitly overwritten, rather,
        ``FeatureCoords.delete_coords_mapper`` should be called first.
        """
        super().add_coords_mapper(coords_mapper)

        # Now add to exons
        for e in self._exons:
            e.add_coords_mapper(coords_mapper)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def delete_coords_mapper(self):
        """Remove any coordinate mapper object from the transcript and all of
        it's exons.
        """
        super().delete_coords_mapper()

        # Now add to exons
        for e in self._exons:
            e.delete_coords_mapper()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_bounds(self):
        """Get the coordinate bounds for the transcript.

        Returns
        -------
        chr_name : `str`
            The chromsome name.
        start_pos : `int`
            The start position.
        end_pos : `int`
            The end position.
        """
        return (
            self._exons[0].chr_name,
            self._exons[0].start_pos,
            self._exons[-1].end_pos
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _add_exons(self, exons):
        """Add the exons and sort.

        Parameters
        ----------
        exons : `list` of `skyline.features.Exon`
        """
        if len(exons) == 0:
            raise IndexError("no exons to add")

        for i in exons:
            if not issubclass(i.__class__, Exon):
                raise TypeError("exons must be skyline.features.Exon objects")

        self._exons.extend(exons)
        self._exons.sort(key=lambda x: x.start_pos)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_exons(self):
        """Check that all the exons are valid.

        Raises
        ------
        KeyError
            If the exons are on different chromosomes.
        IndexError
            If exon coordinates overlap.
        """
        prev = self._exons[0]
        for i in range(1, len(self._exons)):
            if i.chr_name != prev.chr_name:
                raise KeyError("exons on different chromosomes")

            if i.start < prev.start:
                raise IndexError("exons overlap")
            prev = i

