"""Defining a genomic figure
"""
from matplotlib import figure


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _GenomicBase(object):
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GenomicFigure(figure.Figure, _GenomicBase):
    """
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, **kwargs):
        """
        """
        super().__init__(*args, **kwargs)
        # self.set_tight_layout(True)
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_track(self, ax, *args, **kwargs):
        """
        """
        self.add_axes(ax, *args, **kwargs)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Figure(figure.Figure):
    """
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_subfigure(self, subplotspec, subfigure_class=GenomicFigure,
                      **kwargs):
        """
        Add a `~.figure.SubFigure` to the figure as part of a subplot
        arrangement.

        Parameters
        ----------
        subplotspec : `.gridspec.SubplotSpec`
            Defines the region in a parent gridspec where the subfigure will
            be placed.

        Returns
        -------
        `.figure.SubFigure`

        Other Parameters
        ----------------
        **kwargs
            Are passed to the `~.figure.SubFigure` object.

        See Also
        --------
        .Figure.subfigures
        """
        sf = subfigure_class(self, subplotspec, **kwargs)
        self.subfigs += [sf]
        return sf
