"""Some coordinate mapping classes
"""
from skyline import utils, constants as con
from skyline.example_data import examples
import pandas as pd
# import math


# TODO: A REST GenomewideCoords to get coordinate data from Ensembl
# TODO: A data GenomewideCoords to get coordinate data from pandas.DataFrame
# TODO: And single chromosome equivalents

_LENGTH = 'length'
_PAD = 'pad'
_ACTIVE = 'active'


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PassthroughCoords(object):
    """This acts as a base class and a coordinate object that will simply
    allow coordinates to pass through it without changing them.

    Parameters
    ----------
    start : `int`, optional, default: `1`
        The start position, thisis optional and is present for interface
        purposes and just in case a passthrough coordinates object is ever
        used to initialise an Axes.
    end : `int`, optional, default: `100`
        The end position, thisis optional and is present for interface
        purposes and just in case a passthrough coordinates object is ever
        used to initialise an Axes.
    *args
        Any additional arguments (ignored)
    **kwargs
        Any additional keyword arguments (ignored)
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, start=1, end=100, *args, **kwargs):
        self._start = start
        self._end = end
        self._chr_length = self._end - self._start + 1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def start(self):
        """Return the start position of the coordinate range  (`int`)
        """
        return self._start

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def end(self):
        """Return the start position of the coordinate range (`int`)
        """
        return self._end

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def length(self):
        """Return the length of the coordinate range (`int`)
        """
        return self._chr_length

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def has_chromosome(self, chr_name):
        """Determine if the coordinates object has the chromosome.

        Parameters
        ----------
        chr_name : `str`
            The name of the chromosome to check

        Returns
        -------
        chromosome_present : `bool`
            This will always be `True`.
        """
        return True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_coords(self, chr_name, pos):
        """This provides the same interface as `skyline.coords.ChrCoords` and
        `skyline.coords.GenomewideCoords` and just passes the ``pos`` argument
        back to the user.

        Parameters
        ----------
        chr_name : `str`
            The name of the chromosome to check, there because it is expected.
            it is however, ignored.
        pos : `int`
            The position to get the coordinates for - returned as is.

        Returns
        -------
        position : `int`
            The position that was passed.
        """
        return pos


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ChrCoords(PassthroughCoords):
    """A single chromosome "mapper" this is able to provide the chromosome
    length if required and is designed to have the same interface as
    `skyline.coords.GenomewideCoords`

    Parameters
    ----------
    chr_name : `str`
        The name of the chromosome.
    start : `int`
        The start position, thisis optional and is present for interface
        purposes and just in case a passthrough coordinates object is ever
        used to initialise an Axes.
    end : `int`
        The end position, thisis optional and is present for interface
        purposes and just in case a passthrough coordinates object is ever
        used to initialise an Axes.
    *args
        Any additional arguments passed to base class.
    **kwargs
        Any additional keyword arguments passed to base class.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, chr_name, start, end, *args, **kwargs):
        super().__init__(start, end, *args, **kwargs)
        self._chr_name = str(chr_name)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def chr_name(self):
        """Get the chromosome name for the coords object (`str`)
        """
        return self._chr_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_coords(self, chr_name, pos):
        """This provides the same interface as
        `skyline.coords.GenomewideCoords` and just passes the ``pos`` argument
        back to the user.

        Parameters
        ----------
        chr_name : `str`
            The name of the chromosome to check
        pos : `int`
            The position to get the coordinates for.

        Returns
        -------
        position : `int`
            The position that was passed.

        Raises
        ------
        KeyError
            If the ``chr_name``, is not the same chromosome as the instance of
            ``skyline.coords.ChrCoords``.
        """
        if chr_name != self.chr_name:
            raise KeyError("unknown chromsome: '{0}'".format(chr_name))
        return pos

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def has_chromosome(self, chr_name):
        """Determine if the coordinates object has the chromosome.

        Parameters
        ----------
        chr_name : `str`
            The name of the chromosome to check

        Returns
        -------
        chromosome_present : `bool`
            `True` if the chromosome is present, `False` if not.
        """
        return str(chr_name) == self.chr_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_active_chr_name(self):
        """Get all the active chromosome names. In the case of
        `skyline.coords.ChrCoords`, this is a list with a solitary chromosome

        Returns
        -------
        active_chr_names : `list` of `str`
            The single chromosome covered by the `skyline.coords.ChrCoords`
            object.
        """
        return [self.chr_name]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_chr_name_index(self, chr_name):
        """Get the index position of a chromsome.

        Returns
        -------
        index position : `int`
            The index position of the chromsome.

        Raises
        ------
        ValueError
            If the chromosome argument is not an active chromosome.

        Notes
        -----
        The index position is the 0 based integer position of the chromosome,
        which is the actual order you want them arranged on the plot.
        """
        if chr_name == self.chr_name:
            # As there is a single chromsome it's index position is 0
            return 0
        raise ValueError("not an active chromosome: {0}".format(chr_name))


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GenomewideCoords(ChrCoords):
    """Map between a chr_name:position coordinate system to a genome wide
    coordinate system for the purpose of producing genomewide plots.

    Parameters
    ----------
    chr_lengths : `dict`
        The keys of the dictionary are the chromosome names and the values are
        tuples of chromosome length and padding base pairs or the values could
        be just an `int` of chromosome length in base pairs.
    default_pad : `int`
        In the event of the padding for a chromosome being 0, use this padding
        value. Padding values are in base pairs.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, chr_lengths, *args, default_pad=0, **kwargs):
        self.chr_lengths = chr_lengths
        self._default_pad = default_pad
        self.refresh()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def start(self):
        """Return the genome-wide start coordinate, currently this is 1 but
        this will change in future (`int`).
        """
        return 1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def end(self):
        """Return the genome-wide start coordinate, currently this is the
        genome length - 1 (`int`).
        """
        return self.genome_length - 1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def length(self):
        """Return the genome-wide length (`int`).
        """
        return self.genome_length

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def _expand_data(cls, data):
        """The data field for any particular chromosome could be different
        things. This method works out what we have and fills in the missing
        values accordingly.

        Parameters
        ----------
        data : `tuple` or `int`
            The `tuple` could either be of length 3 with ``[0]`` = chr length
           ``[1]`` the core boolean (don't worry about this for now) ``[2]``
           the padding for the chromosome (in bp). Or it could be just ``[0]``
           and ``[2]`` for simply the chromosome length in base pairs.

        Returns
        -------
        length : `int`
            The chromosome length in base pairs.
        pad : `int`
            The amount of padding (in bp) to apply after the chromosome.
        core : `bool`
            Is the chromosome a core chromosome (don't worry about this).
        """
        core = True
        pad = 0
        try:
            length, core, pad = data
        except ValueError:
            try:
                length, pad = data
            except ValueError:
                # Still too many
                length = data
        return length, pad, core

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def get_genomewide_offset(cls, chr_lengths, default_pad=0,
                              use_zero_start=True):
        """Convert the chromosome length data into the number of base pairs
        offset into the genome.

        Parameters
        ----------
        chr_lengths : `dict`
            The keys of the dictionary are the chromosome names and the values
            are tuples of chromosome length and padding base pairs or the
            values could be just an `int` of chromosome length in base pairs.
        default_pad : `int`, optional, default: 0
            The default padding that is added to each chromosome that has an
            existing padding of 0. Given that the default for this parameter
            is 0, then the default situation is to do nothing.

        Returns
        -------
        chr_lengths :  : `dict`
            The keys of the dictionary are the chromosome names and the values
            are nested `dict` with the keys ``length``, ``pad`` and ``active``.
        genomewide_offsets : `dict`
            The keys of the dictionary are chromosome names (`str`) and the
            values are offsets+padding (`int`) in base pairs.
        genome_length : `int`
            The total lengths of the genome (including padding applied to
            the chromosomes).

        Notes
        -----
        The first chromosome starts at 1 and the genomewide offsets includes
        the padding around each chromosome. The order of the chromosomes in
        the "genome" is dependent on the dictionary order. Later versions of
        Python maintain dictionaries in the order that the elements were added.
        """
        norm_chr_lengths = {}
        gw_lengths = {}
        gw_dist = 0
        if use_zero_start is False:
            raise NotImplementedError(
                "all currently genome wide coords start at 0"
            )

        for chr_name, data in chr_lengths.items():
            chr_name = str(chr_name)

            if isinstance(data, (tuple, list)):
                length, pad, core = cls._expand_data(data)
                norm_chr_lengths[chr_name] = {
                    _LENGTH: length, _PAD: pad, _ACTIVE: core
                }
            else:
                length, pad, core = data[_LENGTH], data[_PAD], data[_ACTIVE]
                norm_chr_lengths[chr_name] = data

            if core is False:
                continue
            length = int(length)
            pad = int(pad)
            if pad == 0:
                pad = default_pad

            gw_lengths[chr_name] = gw_dist
            gw_dist += (length + pad)
        return norm_chr_lengths, gw_lengths, gw_dist

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_inactive(self, chr_names):
        """Get the genomewide coordinates for a chromosome and start position

        Parameters
        ----------
        chr_name : `list` of `str`
            The chromosome names to set as inactive.
        """
        if isinstance(chr_names, pd.DataFrame):
            chr_names_present = chr_names[con.CHR_NAME].unique().tolist()
            chr_names = [
                i for i in self.chr_lengths.keys()
                if i not in chr_names_present
            ]
        self._toggle_active(chr_names, False)
        self.refresh()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_all_inactive(self):
        """Set all the chromosome names to inactive.
        """
        self.set_inactive(list(self.chr_lengths.keys()))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_active(self, chr_names):
        """Get the genomewide coordinates for a chromosome and start position

        Parameters
        ----------
        chr_name : `list` of `str`
            The chromosome names to set as inactive.
        """
        if isinstance(chr_names, pd.DataFrame):
            chr_names = chr_names[con.CHR_NAME].unique().tolist()
        self._toggle_active(chr_names, True)
        self.refresh()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_active_chr_name(self):
        """Get all the active chromosome names.

        Returns
        -------
        active_chr_names : `list` of `str`
            The active chromosome names, they are returned in the order they
            were supplied in.
        """
        return list(self.genome_offset.keys())

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_coords(self, chr_name, pos):
        """Get the genomewide coordinates for a chromosome and start position

        Parameters
        ----------
        chr_name : `str`
            The chromosome name. It should be a string but is cast into a
            string internally just to be sure.
        pos : `int`
            The position to convert

        Returns
        -------
        genomewide_position : `int`
            The distance in base pairs from the genome start
        """
        chr_name = str(chr_name)
        try:
            return self.genome_offset[chr_name] + pos - 1
        except KeyError as e:
            raise KeyError(
                "chromosome not in the genome: '{0}".format(chr_name)
            ) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def has_chromosome(self, chr_name):
        """Determine if the coordinates object had the chromosome

        Parameters
        ----------
        chr_name : `str`
            The name of the chromosome to check

        Returns
        -------
        chromosome_present : `bool`
            `True` if the chromosome is present, `False` if not.
        """
        return chr_name in self.get_active_chr_name()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_start(self, chr_name):
        """Get the genomewide coordinates for a chromosome and start position

        Parameters
        ----------
        chr_name : `str`
            The chromosome name. It should be a string but is cast into a
            string internally just to be sure.
        pos : `int`
            The position to convert

        Returns
        -------
        genomewide_position : `int`
            The distance in base pairs from the genome start
        """
        chr_name = str(chr_name)
        try:
            return self.genome_offset[chr_name]
        except KeyError as e:
            raise KeyError(
                "chromosome not in the genome: '{0}".format(chr_name)
            ) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_end(self, chr_name):
        """Get the genomewide coordinates for a chromosome and start position

        Parameters
        ----------
        chr_name : `str`
            The chromosome name. It should be a string but is cast into a
            string internally just to be sure.
        pos : `int`
            The position to convert

        Returns
        -------
        genomewide_position : `int`
            The distance in base pairs from the genome start
        """
        chr_name = str(chr_name)
        active = self.get_active_chr_name()
        try:
            chr_name_idx = active.index(chr_name)
        except ValueError as e:
            raise KeyError(
                "chromosome not in the genome: '{0}".format(chr_name)
            ) from e

        try:
            return self.genome_offset[active[chr_name_idx+1]] - 1
        except IndexError:
            return self.genome_length

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_center(self, chr_name):
        """Get the genomewide coordinates for a chromosome and start position

        Parameters
        ----------
        chr_name : `str`
            The chromosome name. It should be a string but is cast into a
            string internally just to be sure.
        pos : `int`
            The position to convert

        Returns
        -------
        genomewide_position : `int`
            The distance in base pairs from the genome start
        """
        start = self.get_start(chr_name)
        end = self.get_end(chr_name)
        return start + round((end - start)/2, 0)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def refresh(self):
        """Recalculate the genome-wide offsets and length
        """
        # Convert the chr lengths into offsets into the genome
        self.chr_lengths, self.genome_offset, self.genome_length = \
            self.__class__.get_genomewide_offset(
                self.chr_lengths, default_pad=self._default_pad
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_chr_name_index(self, chr_name):
        """Get the index position of a chromsome.

        Returns
        -------
        index position : `int`
            The index position of the chromsome.

        Raises
        ------
        ValueError
            If the chromosome argument is not an active chromosome
        """
        return self.get_active_chr_name().index(chr_name)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _toggle_active(self, chr_names, toggle_to):
        """Set multiple chromosome names to ``toggle_to``

        Parameters
        ----------
        chr_names : `list` of `str`
            The chromosome names to toggle
        toggle_to : `bool`
            `True` means set to active `False` means set to inactive
        """
        for i in chr_names:
            i = str(i)
            try:
                self.chr_lengths[i][_ACTIVE] = toggle_to
            except KeyError as e:
                raise KeyError(
                    "chromosome not in the genome: '{0}".format(i)
                ) from e


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class HumanNCBI36(GenomewideCoords):
    """A convenience class that loads up the chromosome/genome lengths for
    human genome assembly NCBI36.

    Parameters
    ----------
    *args
        Arguments to `skyline.coords.GenomewideCoords`. If any chr_lengths are
        passed they are ignored.
    **kwargs
       Keyword arguments to `skyline.coords.GenomewideCoords`.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, **kwargs):
        super().__init__(
            utils.read_config_section(
                examples.get_data('reference_lengths'), 'coords.human.b36'
            ),
            *args, **kwargs
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class HumanGRCh37(GenomewideCoords):
    """A convenience class that loads up the chromosome/genome lengths for
    human genome assembly GRCh37.

    Parameters
    ----------
    *args
        Arguments to `skyline.coords.GenomewideCoords`. If any chr_lengths are
        passed they are ignored.
    **kwargs
       Keyword arguments to `skyline.coords.GenomewideCoords`.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, **kwargs):
        """
        """
        super().__init__(
            utils.read_config_section(
                examples.get_data('reference_lengths'), 'coords.human.b37'
            ),
            *args, **kwargs
        )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class HumanGRCh38(GenomewideCoords):
    """A convenience class that loads up the chromosome/genome lengths for
    human genome assembly GRCh38.

    Parameters
    ----------
    *args
        Arguments to `skyline.coords.GenomewideCoords`. If any chr_lengths are
        passed they are ignored.
    **kwargs
       Keyword arguments to `skyline.coords.GenomewideCoords`.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, **kwargs):
        """
        """
        super().__init__(
            utils.read_config_section(
                examples.get_data('reference_lengths'), 'coords.human.b38'
            ),
            *args, **kwargs
        )
