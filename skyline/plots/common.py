"""Some functions that are used by more than one plotting function.
"""
from matplotlib import gridspec


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_gridspec(spec, nrows=1, ncols=1, **spec_kwargs):
    """Get a grid spec to work on, this will assess if a nested gridspec is
    required.

    Parameters
    ----------
    spec : `gridspec.GridSpec` or `gridspec.SubplotSpec` or `NoneType`
        The spec to asses and act upon.
    nrows : `int`, optional, default: `1`
        The number of rows in any created `gridspec.Gridspec` objects.
    ncols : `int`, optional, default: `1`
        The number of columns in any created `gridspec.Gridspec` objects.
    **spec_kwargs
        Any further keyword arguments to use to create `gridspec.Gridspec`
        objects.

    Returns
    -------
    working_spec : `gridspec.GridSpec`
        A GridSpec that can be used to position plotting components.

    Notes
    -----
    What this function does depends upon the value of ``spec``. If ``spec`` is
    ``NoneType``, this is assumed that we need to create a GridSpec to work
    with and the values of ``nrows``, ``ncols`` and ``**spec_kwargs`` are used
    to create a new `gridspec.Gridspec` object that is returned. If spec is a
    `gridspec.Gridspec` then it is assumed that the user has pre-supplied a
    `gridspec.Gridspec` object so it is checked for enough rows and columns
    based on the ``nrows``, ``ncols`` and``**spec_kwargs``. If it is a
    `gridspec.SubplotSpec`, it is assumed that a nested gridspec is happening
    and once again a new gridspec is created using ``nrows``, ``ncols`` and
    ``**spec_kwargs`` but with the `gridspec.SubplotSpec` as the parent.
    """
    # We need to create one
    if spec is None:
        return gridspec.GridSpec(
            nrows, ncols, **spec_kwargs
        )

    # User supplied gridspec means we test to see if it has the correct
    # dimentions
    if isinstance(spec, (gridspec.GridSpec, gridspec.GridSpecFromSubplotSpec)):
        if nrows < spec.nrows:
            raise IndexError(
                "not enough rows in spec: needed {0}, got {1}".format(
                    nrows, spec.nrows
                )
            )

        if ncols < spec.ncols:
            raise IndexError(
                "not enough columns in spec: needed {0}, got {1}".format(
                    ncols, spec.ncols
                )
            )
        return spec
    elif isinstance(spec, gridspec.SubplotSpec):
        # Create the gridspec from the subplot spec
        return gridspec.GridSpecFromSubplotSpec(
            nrows, ncols, spec, **spec_kwargs
        )
    else:
        raise TypeError("unknown spec: {0}".format(type(spec)))
