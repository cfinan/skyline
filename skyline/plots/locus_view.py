"""Higher level plotting interface functions for locus view style plots
"""
from skyline import (
    utils,
    coords,
    features,
    ensembl,
    axes,
    colors
)
from skyline.patches import genes as gp
from skyline.tracks import (
    scatter,
    genes,
    linkers
)
from skyline.plots import (
    common
)
from itertools import cycle
import warnings
import pandas as pd
# import pprint as pp


_GWAS_DATA_PVALUE = 'pvalue'
"""The column name for the pvalue column in the GWAS data frame (`str`)
"""
_SCATTER_UNITS = scatter.LocusViewScatter.UNIT_TEXT_COORDS
"""Import the scatter units for the LV scatter plot component (`str`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RegionDataContainer(object):
    """A container class used to pass region information to locus view plots.

    Parameters
    ----------
    data : `pandas.DataFrame`
        A region slice of GWAS data must have ``chr_name``, ``start_pos``,
        ``effect_allele``, ``other_allele`` and ``pvalue`` columns.
    lead_variant : `skyline.features.Variant`
        A lead variant feature.
    label : `str`, optional, default: ``
        A label for the region.
    ld_data : `list` of `tuple` or `pandas.DataFrame`, optional, \
    default: `NoneType`
        LD data to use to colour the points in the plot. If `NoneType` and the
        rest client is defined, then that will be used to retrieve LD data for
        the population. If not then the points will be coloured with "no LD".
        If it is a `list` of `tuple` then each tuple is a pair of variants with
        an r2 measure (float). Each variant in the pair is a tuple with:
        (chr_name, start_pos (<ALLELES>), variation ID). If it is a
        `pandas.DataFrame`, then it should be a square matrix with both rows
        and columns being ``uni_ids`` - that is
        <chr_name>_<start_pos>_<allele1>_<allele2> where allele1 and allele2
        are the ref/alt in sort order.
    population_name : `str`, optional, default: `1000GENOMES:phase_3:EUR`
        The population that is used by the REST client to supply the pairwise
        LD values with the lead variant. This is only required if ``ld_data``
        is ``None``.
    species : `str`, optional, default: `human`
        The species for the gene lookup if using the rest client. This is only
        required if ``ld_data`` is ``None``.
    style : `dict`, optional, default: `NoneType`
        Any styling to be used for the lead variant. ``NoneType``, will use
        default styling in
        `skyline.tracks.scatter.LocusViewScatter.LEAD_VARIANT_STYLE`.
    ylim : `tuple` of (`float`, `float`)
        Specific y-min, y-max bounds to be used for the plot represented by the
        region, if ``NoneType``, then matplotlib will decide the y-limits.

    Notes
    -----
    This only serves the purpose of holding data in situations where multiple
    regions need to be handled in a single plot. It is slightly cleaner than
    using tuples or dicts.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, data, lead_variant, label="", ld_data=None,
                 population_name='1000GENOMES:phase_3:EUR', species='human',
                 style=None, ylim=None):
        self.data = data
        self.lead_variant = lead_variant
        self.label = label
        self.ylim = ylim

        if ld_data is not None:
            self.ld_data = ld_data
        else:
            self.ld_data = list()
        self.population_name = population_name
        self.species = species
        self.style = style

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing
        """
        lab = "label='{0}'".format(self.label)
        d = "data=({0})".format(",".join([str(i) for i in self.data.shape]))
        return "<{0}({1}, {2})>".format(
            self.__class__.__name__, lab, d
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def lead_variant_in_region(self, r):
        """Crop the container data to a region boundary.

        Parameters
        ----------
        region : `skyline.featues.FeatureCoords`
            A region to crop to.
        copy : `bool`, optional, default: `True`
            Create a copy of the region.

        Raises
        ------
        KeyError
            If the lead variant is not within the cropped region.
        """
        selector = (
            (self.data.chr_name == r.chr_name) &
            (self.data.start_pos >= r.start_pos) &
            (self.data.start_pos <= r.end_pos)
        )

        return self.lead_variant.uni_id in self.data.loc[selector].index

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def crop_to_region(self, r, copy=True):
        """Crop the container data to a region boundary.

        Parameters
        ----------
        region : `skyline.featues.FeatureCoords`
            A region to crop to.
        copy : `bool`, optional, default: `True`
            Create a copy of the region.

        Raises
        ------
        KeyError
            If the lead variant is not within the cropped region.
        """
        if self.lead_variant_in_region is False:
            raise KeyError(
                "lead variant not in region bounds: {self.lead_variant.uni_id}"
            )

        selector = (
            (self.data.chr_name == r.chr_name) &
            (self.data.start_pos >= r.start_pos) &
            (self.data.start_pos <= r.end_pos)
        )

        self.data = self.data.loc[selector].copy()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def locus_scatter(gfig, region, region_data, coord_region=None, tracks=None,
                  top_pad_length=1,
                  scatter_length=10,
                  genes_length=5,
                  scatter_gene_gap_length=1,
                  ruler_length=2,
                  linker_length=2,
                  blank_tracks=None,
                  rest_client=None,
                  scatter_marker_style=None,
                  scatter_lead_variant_style=None,
                  scatter_lead_variant_label_style=None,
                  scatter_plot_legend=True,
                  scatter_legend_patch=None,
                  scatter_legend_kwargs=None,
                  gene_coords=None,
                  gene_length_pad=5000,
                  gene_label_height=0.5,
                  gene_label_pad=0.01,
                  gene_label_style=None,
                  gene_track_top_pad=0,
                  gene_track_bottom_pad=0,
                  gene_row_style=None,
                  gene_patch_style=None,
                  gene_patch=gp.ExonGene,
                  gene_row_pad=0,
                  min_gene_rows=1,
                  gene_patch_pointbreaks=[1000, 10000],
                  gene_label_dir=True,
                  gene_trace_style=None,
                  # annotation_matrix=None,
                  # annotation_grid_true_style=None,
                  # annotation_grid_false_style=None,
                  ruler_unit_text_coords=(0.5, 2.0),
                  ruler_unit_text_style=None,
                  scatter_gap_length=1,
                  plot_lead_variant_label=True,
                  biotypes=None,
                  separate_gene_strands=False,
                  yaxis_label=True,
                  yaxis_label_text=scatter.LocusViewScatter.MLOG_PVALUE_TEXT,
                  yaxis_label_style=None,
                  yaxis_label_pos=(-.1, 0.5)):
    """Produce a locus zoom style plot with multiple loci stacked and a single
    region.

    Parameters
    ----------
    gfig : `skyline.GenomicFigure`
        The figure on which to apply the plot.
    region : `skyline.features.FeatureCoords`
        The region boundary to plot.
    region_data : `list` of `skyline.plots.locus_zoom.RegionDataContainer`
        The data, lead variant and LD values for the regions we want to plot.
    coord_region : `skyline.coords.ChrCoords`, optional, default: `NoneType`
        A coordinates object to use in the plot region. If not supplied then
        one is created from the ``region`` object.
    tracks : `matplotlib.SubplotSpec`, optional, default: `NoneType`
        A grid locations to place the plot. If a subplotspec then a grid spec
        will be created with it, if ``NoneType``, then a grid spec will be
        created with enough space to hold the plot.
    top_pad_length : `int`, optional, default: `1`
        The number of gridspec slots to use for padding above the first scatter
        plot.
    scatter_length : `int`, optional, default: `10`
        The number of gridspec slots to use for each scatter plot.
    genes_length : `int`, optional, default: `5`
        The number of gridspec slots to use for the gene track. If separate
        ``separate_gene_strands`` is ``True`` then, this is the number of
        slots for each gene strand.
    scatter_gene_gap_length : `int`, optional, default: `1`
        The number of gripspec row slots to leave between the scatter plot
        and the gene track.
    ruler_length : `int`, optional, default: `2`
        The number of gridspec slots to use for the coordinate ruler track.
    linker_length : `int`, optional, default: `2`
        The number of gridspec slots to use for the gene linker.
        These will only be used if an blank traks are supplied.
    blank_tracks : `list` of `int`
        Include space for additional tracks that will not be plotted by locus
        view. These are additional info that the use might want to include
        about the genes. Adding blank tracks will also add a linker and
        trailing equally spaced lines.
    rest_client : `ensembl_rest_client.client.Rest`, optional, \
    default: `NoneType`
        The rest client object that can be used to retrieve LD data and/or gene
        annotations. Only needs supplying if no LD data is present in the
        ``RegionDataContainer`` objects and no gene data is supplied.
    scatter_marker_style : `dict`, optional, default: `NoneType`
        The style and default colour (for variants with no LD) for the scatter
        plot marker. Note that, the colour map for the LD values is handled
        internally. The default colour should be given as the ``c`` argument
        and not ``color``. Must be appropriate for ``Axes.scatter``, useful
        options are ``s`` (size), and ``marker``.
    scatter_lead_variant_style : `dict`, optional, default: `NoneType`
        The default style parameters for the lead variant plotting marker on
        the scatter plot. Must be appropriate for ``Axes.scatter``. A
        ``NoneType`` value defaults to
        `skyline.tracks.scatter.LocusViewScatter.LEAD_VARIANT_STYLE`.
    scatter_lead_variant_label_style : `dict`, optional, default: `NoneType`
        The default style parameters for the lead variant label on
        the scatter plot. These must be appropriate for `matplotlib.text.Text`.
        A `NoneType` value defaults to.
        `skyline.tracks.scatter.LocusViewScatter.LEAD_VARIANT_LABEL_STYLE`.
    scatter_plot_legend : `bool`, optional, default: `True`
        Plot the legend on the scatter plot.
    scatter_legend_patch : `dict`, optional, default: `NoneType`
        Keyword arguments to the rectangle patches used in the legend. These
        should be general arguments used in all the patches. If you want fine
        grained control of the legend you should set ``plot_legend=False`` and
        handle the call to `skyline.tracks.scatter.LocusViewScatter.legend`.
        ``NoneType`` uses the class defaults:
        `skyline.tracks.scatter.LocusViewScatter.LEGEND_PATCH_STYLE`.
    scatter_legend_kwargs : `dict`, optional, default: `NoneType`
        Arguments to the `skyline.tracks.scatter.LocusViewScatter.legend` call.
        It is possible to completely override the default arguments with this,
        including the legend handles. If it is `NoneType`, then this will
        default to:
        `skyline.tracks.scatter.LocusViewScatter.LEGEND_ARGS`
    gene_coords : `list` of `skyline.features.SimpleTranscript`, optional,
    default: `NoneType`
        Gene coordinates to plot. Must inherit from
        `skyline.features.SimpleTranscript`. If ``NoneType``, then gene
        coordinates are obtained from Ensembl using the rest client. If this is
        not available then an error will be raised.
    gene_row_style : `NoneType` or `list` of `dict`, optional, \
    default: `NoneType`
        Colours for the backgrounds of the individual gene rows. The colours
        are recycled if number of gene rows is > than the length of
        ``gene_row_style``. Each dict should contain keyword arguments for
        a `matplotlib.Axes.axhspan`
    gene_patch_style : `dict` or `NoneType`
        A style patameters to use for formatting of the gene patches or
        ``NoneType`` to use the default style parameters. The keyword argument
        in the dict should be appropriate for the
        `matplotlib.patches.PathPatch`.
    gene_patch : `skyline.patches.genes.BlockGene`, optional,
    default: `skyline.patches.genes.BlockGene`
        The patch (plotting symbol) for the gene. This should be a class that
        must be a subclass of `skyline.patches.genes.BlockGene`.
    gene_row_pad : `int`, optional, default: `0`
        The number of basepairs to add to the start or end of the gene to
        artificially increase it's virtual length (not plotted length). This
        can be used to influence how the genes are perceived to overlap on the
        gene track. Setting this higher will result in more gene rows and less
        crowding onto any single row.
    min_gene_rows : `int`, optional, default: `1`
        The minimum number of gene rows in the gene track. This can be used to
        ensure the scaling of genes/labels is the same across multiple tracks.
    gene_patch_pointbreaks : `list` of `int`, optional, \
    default: `[1000, 10000]`
        The base pair cutoffs that are used to place a directional arrow head
        on a gene patch for gene patches that support it (currently only
        skyline.patches.genes.BlockGene). If the patch does not support arrow
        breakpoints, then this will be ignored.
    gene_label_dir : `bool`, optional, default: `True`
        Add a directional symbol to the gene label, this is either a ``>`` for
        genes on the forward strand or a ``<`` for genes on the reverse strand.
    gene_trace_style : `dict`, optional, default: `NoneType`
        The styling parameters to be used for gene traces, these are
        projections from each gene to the outside of the gene track.
    annotation_matrix : `pandas.DataFrame`, optional, default: `NoneType`
        An annotation matrix to plot below the genes. This is a boolean matrix
        with the annotation categories as row indexes and the gene IDs as
        column indexes.
    annotation_grid_true_style : `dict`, optional, default: `NoneType`
        The styling parameters for the true annotation grid categories.
    annotation_grid_false_style : `dict`, optional, default: `NoneType`
        The styling parameters for the false annotation grid categories.
    scatter_unit_text_coords : `tuple`, optional, default: `(0, -0.06)`
        The x,y placement coordinates of the unit string in ``coord_units``.
        The default will place it in the bottom left of the plot. These are not
        `matplotlib.axis.offsetText` but rather it is the positions of the
        xaxis label operating as offset text.
    scatter_unit_text_style : `dict` or `NoneType`, optional, \
    default: `NoneType`
         Keyword arguments to `matplotlib.text.Text`, that are used to format
         ``coord_units[1]``. If ``NoneType`` then this defaults to:
         ``skyline.scatter.ChrScatter.DEFAULT_OFFSET_TEXT_STYLE()``.
    plot_lead_variant_label : `bool`, optional, default: `True`
        Should the lead variant label be plotted.
    biotypes : `NoneType` or `list` of `str`, optional: default: `NoneType`
        If using the rest client then restrict the gene list to only genes
        matching the requested biotypes. ``NoneType`` will return any
        overlapping gene regardless of biotype.
    separate_gene_strands : `bool`, optional, default: `False`
        The population that is used by the REST client to supply the pairwise
        LD values with the lead variant.
    yaxis_label_text : `str`, optional, default: `$-\\log_{{10}}(P)$'`
        The y-axis label text, The default is the latex formatted -log10(P).

    Returns
    -------
    axes : `tuple` of `skyline.axes.BaseGenomicAxes`
        The axes used in the plot. This is comprised of (in this order):
        ``ruler_track`` : ``skyline.tracks.labels.TopRulerTrack``
        ``gene_track`` : ``skyline.tracks.genes.GeneTrack``
        ``linker`` : ``skyline.tracks.linkers.PosToUniformLinkerTrack``
        ``bool_grid`` : ``skyline.tracks.grids.BooleanGrid``
        ``scatter_axes`` : ``skyline.tracks.scatter.LocusViewScatter``
    gene_coords : `list` of `skyline.features.ExonTranscript`
        The gene coordinate features used in the plot.
    annotation_matrix : `pandas.DataFrame`, optional, default: `NoneType`
        An annotation matrix to plot below the genes. This is a boolean matrix
        with the annotation categories as row indexes and the gene IDs as
        column indexes.
    ld_data : `list` of `tuple` or `pandas.DataFrame`
        If the ``ld_data`` has been supplied then this will be the same object.
        However, if created then this will be the data extracted from the rest
        API and will have the following structure. In the `list` each tuple is
        a pair of variants with an r2 measure (float). Each variant in the pair
        is a tuple with: (chr_name, start_pos (<ALLELES>), variation ID).

    Raises
    ------
    ValueError
        If the flank is <= 0

    Notes
    -----
    """
    row_start = 0
    row_end = 0
    col_start = 0
    scatter_axes = []
    gene_track = None
    linker = None
    bool_grid = None
    annotation_matrix = None
    blank_tracks = blank_tracks or []

    # Make sure there is a region span
    if len(region) == 0:
        raise ValueError("region has no size")

    # Make sure the regions have data
    for i in region_data:
        i.crop_to_region(region)
        if i.data.shape[0] <= 1:
            raise ValueError(
                "not enough data, nrows == {0}".format(i.data.shape[0])
            )

    # Validate or create a coordinates region object
    coord_region = _set_region(coord_region, region)

    # Initialise the tracks if needed
    if len(blank_tracks) == 0:
        # if annotation_matrix is None:
        linker_length = 0

    # Calculate the space required for the plot and define a gridspec
    # for it
    nrows = sum(
        [
            top_pad_length,
            ruler_length,
            (
                (scatter_length * len(region_data))
                +
                (scatter_gap_length * (len(region_data) - 1))
            ),
            scatter_gene_gap_length,
            genes_length + (genes_length * separate_gene_strands),
            linker_length,
        ] + blank_tracks
    )

    tracks = common.get_gridspec(
        tracks, nrows=nrows, ncols=1, wspace=0, hspace=0
    )

    # Get any LD data (if needed)
    _set_ld_data(region_data, region, rest_client)

    # Set the gene coords - this will download of they are not supplied
    gene_coords = _set_gene_coords(
        gene_coords, region, rest_client, species=region_data[0].species,
        biotypes=biotypes
    )
    # Make sure we have some genes, for now I have specifically
    # error-ed out until I have tested what will happen (to avoid
    # unexpected behaviour)
    if len(gene_coords) == 0:
        raise ValueError(
            "there are no genes in the region, need to handle this"
        )
    row_end = row_start + ruler_length

    # A ruler track at the top
    ruler = linkers.TopRulerTrack(
        gfig,
        tracks[row_start:row_end, col_start:col_start+1],
        coord_region,
        unit_text_coords=ruler_unit_text_coords,
        unit_text_style=ruler_unit_text_style
    )
    gfig.add_track(ruler)

    # The scatterplot is given an offset of one above it
    row_end = row_end + top_pad_length
    dummy_start = row_end
    plot_legend = scatter_plot_legend
    for idx, rd in enumerate(region_data):
        row_start = row_end + (scatter_gap_length * bool(idx))
        row_end = row_start + scatter_length

        lz_scatter = scatter.LocusViewScatter(
            gfig,
            tracks[row_start:row_end, col_start:col_start+1],
            coord_region,
            rd.data,
            _GWAS_DATA_PVALUE,
            rd.lead_variant,
            rd.ld_data,
            # ld_breaks=...
            marker_style=scatter_marker_style,
            lead_variant_style=scatter_lead_variant_style,
            legend_patch=scatter_legend_patch,
            plot_legend=plot_legend,
            plot_lead_variant_label=plot_lead_variant_label,
            lead_variant_label_style=scatter_lead_variant_label_style,
            legend_kwargs=scatter_legend_kwargs,
            phenotype=rd.label,
            ylim=rd.ylim
        )
        lz_scatter.xaxis.set_visible(False)
        # if idx != 1:
        lz_scatter.yaxis.set_label_text(None)
        # gfig.add_track(lz_scatter)
        scatter_axes.append(lz_scatter)
        # We will only plot a single legend
        plot_legend = False

    plot_bottom_blank = linkers.BlankTrack(
        gfig,
        tracks[dummy_start:nrows, col_start:col_start+1],
        coord_region,
    )
    gfig.add_track(plot_bottom_blank)

    # A hidden axes that sits below the scatter plot area
    scatter_bottom_blank = linkers.BlankTrack(
        gfig,
        tracks[dummy_start:row_end, col_start:col_start+1],
        coord_region,
    )
    if yaxis_label is True:
        scatter_bottom_blank.yaxis.set_visible(True)
        scatter_bottom_blank.yaxis.set_label_text(
            yaxis_label_text, fontdict=yaxis_label_style
        )
        scatter_bottom_blank.yaxis.set_label_coords(*yaxis_label_pos)
        scatter_bottom_blank.set_yticks([])
        scatter_bottom_blank.set_yticklabels([])

    gfig.add_track(scatter_bottom_blank)
    for i in scatter_axes:
        gfig.add_track(i)

    gene_track_kwargs = dict(
        gene_row_pad=gene_row_pad,
        gene_length_pad=gene_length_pad,
        gene_label_height=gene_label_height,
        gene_label_pad=gene_label_pad,
        gene_patch=gene_patch,
        track_bottom_pad=gene_track_bottom_pad,
        track_top_pad=gene_track_top_pad,
        gene_label_style=gene_label_style,
        gene_patch_style=gene_patch_style,
        gene_row_style=gene_row_style,
        add_label_dir=gene_label_dir,
        gene_patch_pointbreaks=gene_patch_pointbreaks
    )
    row_start = row_end + scatter_gene_gap_length

    if separate_gene_strands is False:
        # Add the gap between the scatter plot and the gene track
        row_end = row_start + genes_length
        gene_track = genes.GeneTrack(
            gfig, tracks[row_start:row_end, col_start:col_start+1],
            coord_region, gene_coords,
            min_gene_rows=min_gene_rows,
            **gene_track_kwargs
        )
        gene_track.xaxis.set_visible(False)
        gene_track.spines['bottom'].set_visible(False)
        gfig.add_track(gene_track)
    else:
        for_tracks, rev_tracks = -1, 0
        min_rows = min_gene_rows
        idx = 0
        while True:
            rs = row_start
            re = rs + genes_length
            if for_tracks != min_rows:
                for_gene_track = genes.GeneTrack(
                    gfig, tracks[rs:re, col_start:col_start+1],
                    coord_region, gene_coords,
                    min_gene_rows=min_rows,
                    plot_strand=1,
                    **gene_track_kwargs
                )
                for_tracks = for_gene_track.n_tracks
            rs = re
            re += genes_length
            if rev_tracks != min_rows:
                rev_gene_track = genes.GeneTrack(
                    gfig, tracks[rs:re, col_start:col_start+1],
                    coord_region, gene_coords,
                    min_gene_rows=min_rows,
                    plot_strand=-1,
                    **gene_track_kwargs
                )
                rev_tracks = rev_gene_track.n_tracks
            idx += 1
            if for_tracks == rev_tracks:
                break
            min_rows = max(for_tracks, rev_tracks)
        for_gene_track.xaxis.set_visible(False)
        for_gene_track.spines['bottom'].set_visible(False)
        gfig.add_track(for_gene_track)
        rev_gene_track.xaxis.set_visible(False)
        rev_gene_track.spines['bottom'].set_visible(False)
        gfig.add_track(rev_gene_track)

    subspecs = []
    if len(blank_tracks) > 0:
        # The gene traces will join with the linker
        gene_track.add_gene_traces(trace_style=gene_trace_style,
                                   direction='down')

        # Add the gap between the scatter plot and the gene track
        row_start = row_end
        row_end = row_start + linker_length

        links = []
        for gf in gene_coords:
            links.append(
                features.SimpleTranscript(
                    gf.chr_name, gf.plot_start_pos,
                    end_pos=gf.plot_end_pos, label=gf.label,
                    trans_id=gf.trans_id, biotype=gf.biotype,
                    styles=gf.styles
                )
            )
        linker = linkers.PosToUniformLinkerTrack(
            gfig, tracks[row_start:row_end, col_start:col_start+1],
            coord_region, links, uniform_order='start'
        )
        gfig.add_track(linker)

        subspecs = []
        rs = row_end
        for i in blank_tracks:
            re = rs + i
            subspecs.append(tracks[rs:re, col_start:col_start+1])
            rs = re
    # linker = None
    # bool_grid = None
    # # If there is an annotation matrix then we will want to plot a linker and
    # # the annotation matrix
    # if annotation_matrix is not None:
    #     # The gene traces will join with the linker
    #     gene_track.add_gene_traces(trace_style=gene_trace_style,
    #                                direction='down')

    #     # Add the gap between the scatter plot and the gene track
    #     row_start = row_end
    #     row_end = row_start + linker_length

    #     linker = linkers.PosToUniformLinkerTrack(
    #         gfig, tracks[row_start:row_end, col_start:col_start+1],
    #         coord_region,
    #         [(i[0], i[1], i[2]) for i in gene_track.get_gene_bounds()],
    #         uniform_order='start'
    #     )
    #     gfig.add_track(linker)
    #     gene_order = [i[2] for i in sorted(linker.get_skyline_link_coords(),
    #                                        key=lambda x: x[1][0])]

    #     # Add the gap between the scatter plot and the gene track
    #     row_start = row_end
    #     row_end = row_start + annot_length

    #     try:
    #         annotation_matrix = annotation_matrix[gene_order]
    #         bool_grid = grids.BooleanGrid(
    #             gfig, tracks[row_start:row_end, col_start:col_start+1],
    #             coord_region, annotation_matrix,
    #             true_style=annotation_grid_true_style,
    #             false_style=annotation_grid_false_style
    #         )
    #         gfig.add_track(bool_grid)
    #     except KeyError:
    #         # Skip bad matricies, here gene_order not match columns in
    #         # annotation grid
    #         warnings.warn(
    #             "bad matrix: expected: {0}, got: {1}".format(
    #                 ",".join(gene_order),
    #                 ",".join(annotation_matrix.columns.tolist())
    #             )
    #         )
    #         annotation_matrix = None

    return (
        tuple([
            plot_bottom_blank, scatter_bottom_blank,
            ruler, gene_track, linker, bool_grid,
            *scatter_axes]),
        region_data, gene_coords, annotation_matrix, tracks, subspecs,
        row_end
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_coords(coord_region, lead_variant, flank):
    region = features.FeatureCoords(
        lead_variant.chr_name,
        lead_variant.start_pos - flank,
        end_pos=lead_variant.end_pos + flank
    )
    return _set_region(coord_region, region)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_region(coord_region, region):
    """Validate and create a coordinates object if necessary.

    Parameters
    ----------
    coord_region : `skyline.coords.ChrCoords` or `NoneType`
        A coordinates object to use in the plot region. If ``NoneType`` then
        one is created from the lead variant position and the ``flank`` length.
    flank : `int`
        The flanking region around the lead variant. This is used with the lead
        variant to create a coords object if one is not supplied.

    Returns
    -------
    coords : `skyline.coords.ChrCoords`
        A coordinates object.
    """
    if isinstance(coord_region, coords.ChrCoords):
        return coord_region

    if coord_region is None:
        # create
        return coords.ChrCoords(
            region.chr_name,
            region.start_pos,
            region.end_pos
        )

    raise TypeError("'coord_region' should be a ChrCoords object or NoneType")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_ld_data(region_data, region, rest_client):
    """Initialise the LD data based on what the user has passed to the parent
    function.

    Parameters
    ----------
    ld_data : `list` of `tuple` or `pandas.DataFrame` or `NoneType`
        LD data,if not `NoneType` then it is simply returned. If ``NoneType``,
        then it will be constructed if the rest client is defined.
    lead_variant : `tuple`
        The tuple should have the following structure. ``[0]`` chr_name,
        ``[1]`` start position ``[2]`` reference allele
        ``[3]`` alternate allele ``[4]`` (optional) variant ID.
    rest_client :  `ensembl_rest_client.client.Rest` or `NoneType`
        The rest client object that can be used to retrieve LD data
    flank : `int`
        The flanking region around the lead variant. This is used if the rest
        client is supplied and no LD data or gene annotations are supplied.
    species : `str`, optional, default: `human`
        The species for the gene lookup if using the rest client.
    population_name : `str`, optional, default: `1000GENOMES:phase_3:EUR`
        The population that is used by the REST client to supply the pairwise
        LD values with the lead variant.

    Returns
    -------
    ld_data : `list` of `tuple` or `pandas.DataFrame`
        The LD data to use in the plot. This could also be an empty list.
    """
    ld_cache = {}
    for rd in region_data:
        if isinstance(rd.ld_data, (list, pd.DataFrame)):
            if len(rd.ld_data) > 0:
                continue

        if rest_client is None:
            continue

        cache_key = (
            rd.lead_variant.uni_id,
            rd.species,
            rd.population_name
        )
        if cache_key in ld_cache:
            rd.ld_data = ld_cache[cache_key]
            continue

        try:
            # Use the rest client to get the LD data
            rd.ld_data = ensembl.get_ld_data(
                rest_client,
                rd.lead_variant.chr_name,
                rd.lead_variant.start_pos,
                rd.lead_variant.ref_allele,
                rd.lead_variant.alt_allele,
                # TODO: need to work out where the lead variant is in the
                # region and take the max away from the region edge. As it
                # stands this might not get everything we want or be overkill
                window_size=len(region)/1000,
                species=rd.species,
                population_name=rd.population_name
            )
            ld_cache[cache_key] = rd.ld_data
        except KeyError as e:
            warnings.warn(str(e))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_gene_coords(gene_coords, region, rest_client, species='human',
                     biotypes=None, canonical=True):
    """Initialise the LD data based on what the user has passed to the parent
    function.

    Parameters
    ----------
    gene_coords : `list` of [`tuple` or `list`] or `NoneType`
        Gene coordinates to plot. Each tuple has the stucture ``[0]``
        chromosome name (`str`), ``[1]`` coords (`tuple` of `tuple`),  ``[2]``
        strand (`int`) either 1 or -1, ``[3]`` gene name (`str`). The
        coordinates at ``[1]`` should represent the exon structure of the gene
        and a flag to indicate if the exon is coding. So,
        ``[(exon_1_start, exon_1_end, coding), (exon_2_start, exon_3_end,
        coding), (exon_3_start, exon_3_end, coding)]``. The start and end
        coordinates of each exon should be ``int`` and the ``coding`` should be
        ``bool``, with ``True`` being yes and ``False`` being no. It is fine to
        omit the coding attribute, in which case everything will default to
        ``True``. If the exon structure is unknown then you can put the gene
        boundaries in there ``[(gene_start, gene_end)]``. If ``NoneType`` then
        The rest API will be used to get all the genes overlapping the
        ``lead_variant`` +/- the flanking region.
    lead_variant : `tuple`
        The tuple should have the following structure. ``[0]`` chr_name,
        ``[1]`` start position ``[2]`` reference allele
        ``[3]`` alternate allele ``[4]`` (optional) variant ID.
    rest_client :  `ensembl_rest_client.client.Rest` or `NoneType`
        The rest client object that can be used to retrieve LD data
    flank : `int`
        The flanking region around the lead variant. This is used if the rest
        client is supplied and no LD data or gene annotations are supplied.
    species : `str`, optional, default: `human`
        The species for the gene lookup if using the rest client.
    biotypes : `NoneType` or `list` of `str`, optional: default: `NoneType`
        If using the rest client then restrict the gene list to only genes
        matching the requested biotypes. ``NoneType`` will return any
        overlapping gene regardless of biotype.

    Returns
    -------
    ld_data : `list` of `tuple` or `pandas.DataFrame`
        The LD data to use in the plot. This could also be an empty list.

    Raises
    ------
    ValueError
        If the ``gene_coords`` are ``NoneType`` and there is no REST API
        available to get them with.
    """
    error_msg = ("no gene coordinates supplied and no way to get "
                 "them. please supply a rest client object")

    if isinstance(gene_coords, list):
        # We just check the main data type and not the whole structure
        # TODO: Do a full error check of the structure
        return gene_coords

    if gene_coords is None and rest_client is None:
        raise ValueError(error_msg)

    # Path to a gene file
    gene_coords = list()
    if isinstance(gene_coords, str):
        try:
            gene_coords = utils.read_gene_data(gene_coords)
            return gene_coords
        except FileNotFoundError:
            if rest_client is None:
                raise ValueError(error_msg)
    else:
        gene_coords = ensembl.get_genes_within(
            rest_client, region.chr_name, region.start_pos,
            end_pos=region.end_pos, species=species, biotypes=biotypes,
            canonical=canonical
        )
    return gene_coords


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def locus_pvalue(gfig, data, tracks=None, data_styles=None, xlabel="Locus X",
                 ylabel="Locus Y", scatter_label="Variant", **kwargs):
    """Plot a -log10(p-value) vs. -log10(p-value) plot. This is helpful if you
    want to demonstrate relationships (or lack of) between p-values.

    Parameters
    ----------
    gfig : `skyline.GenomicFigure`
        The figure on which to apply the plot.
    data : `pandas.DataFrame`
        the data to plot and any additional boolean columns indicating points
        that should be coloured. See the notes for more details.
    tracks : `matplotlib.SubplotSpec`, optional, default: `NoneType`
        A grid locations to place the plot. If a subplotspec then a grid spec
        will be created with it, if ``NoneType``, then a grid spec will be
        created with enough space to hold the plot.
    xlabel : `str, optional, default: `Locus X`
        The x-axis label. This is concatenated with a ``-log10(pvalue)`` label.
         Set to `NoneType` to not plot any x-axis labels.
    ylabel : `str, optional, default: `Locus Y`
        The y-axis label. This is concatenated with a ``-log10(pvalue)`` label.
         Set to `NoneType` to not plot any y-axis labels.
    scatter_label : `str`, optional, default: `Variant`
        The label for the base (not specifically coloured) scatter plot points.
        This will be used in the default call to `axes.legend`.
    **kwargs
        Keyword styling parameters for the base points in the scatter plot.

    Returns
    -------
    scatter_plot : `skyline.axes.BaseGenomicAxes`
        A skyline axes containing the scatter plot.

    Notes
    -----
    This expects a data frame with the p-values already logged and with the
    columns ``pvalue_x``, ``pvalue_y`` that correspond to the x, y axis
    values respectively.

    Any additional columns with a boolean data type are taken to be indicators
    of subsets. That is, these are points that should be coloured a different
    colour if the value in the column is `True` and the base colour if the
    value is `False`. The style for the coloured points can be given in a dict
    to the data_styles parameter. These styles are merged with the default
    styles for a locus view type scatter plot. If not supplied then a color
    cycler is used to colour the points. The label for the coloured points is
    taken from the column name in the data frame.
    """
    # Get a color cycler for any coloured points that do not have styles
    # defined
    col_list = cycle(colors.COLOR_CYCLES)
    data_styles = data_styles or dict()

    # We interpret any boolean columns as requiring coloured overplotting
    colored_scatter = data.dtypes[data.dtypes == 'bool'].index.tolist()

    # If we are colouring points then we generate a subset array of points that
    # are the base colour
    subset = []
    if len(colored_scatter) > 0:
        subset = data[colored_scatter].sum(axis=1) == 0
    else:
        # No colour, take all of them
        subset = [True] * data.shape[0]

    # Get the tracks depending on what the user has given
    tracks = common.get_gridspec(
        tracks, nrows=1, ncols=1, wspace=0, hspace=0
    )

    # Create the axes
    ax = axes.BaseGenomicAxes(
        gfig, tracks[:, :], coords.PassthroughCoords()
    )

    # Make adjustments to the base setup to display the axis
    ax.yaxis.set_visible(True)
    ax.spines['left'].set_visible(True)
    ax.xaxis.set_visible(True)
    ax.spines['bottom'].set_visible(True)

    # Set some sensible defaults
    ax.xaxis.set_tick_params(labelsize=4)
    ax.yaxis.set_tick_params(labelsize=4)

    kwargs = ax.get_skyline_style_kwargs(
        kwargs, scatter.LocusViewScatter.MARKER_STYLE
    )
    ax.scatter(
        data.loc[subset].pvalue_x,
        data.loc[subset].pvalue_y,
        label=scatter_label,
        **kwargs
    )

    # We interpret any boolean columns as requiring coloured
    # overplotting
    colored_scatter = data.dtypes[data.dtypes == 'bool'].index.tolist()
    log = scatter.LocusViewScatter.MLOG_PVALUE_TEXT
    # Loop through the columns that want overplotting
    for i in colored_scatter:
        try:
            scatter_kwargs = ax.get_skyline_style_kwargs(
                data_styles[i], kwargs
            )
        except KeyError:
            scatter_kwargs = ax.get_skyline_style_kwargs(
                dict(c=next(col_list)), kwargs
            )
        subset = data.loc[data[i] == True]
        ax.scatter(subset.pvalue_x, subset.pvalue_y, label=i,
                   **scatter_kwargs)

    if xlabel is not None:
        ax.xaxis.set_label_text(
            f"{log} - {xlabel}", dict(fontsize=5)
        )
    if ylabel is not None:
        ax.yaxis.set_label_text(
            f"{log} - {ylabel}", dict(fontsize=5)
        )

    gfig.add_track(ax)
    return ax


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def locus_ld_pvalue(gfig, region1, region2, tracks=None, ld_breaks=None,
                    lead_variant_style=None, **kwargs):
    """Plot a -log10(p-value) vs. -log10(p-value) plot. With the same LD
    colours as used by a locus view scatter plot.

    Parameters
    ----------
    gfig : `skyline.GenomicFigure`
        The figure on which to apply the plot.
    region1 : `skyline.plots.locus_view.RegionDataContainer`
        The x-axis region. The lead variant and LD values are taken from here.
    region2 : `skyline.plots.locus_view.RegionDataContainer`
        The y-axis region.
    tracks : `matplotlib.SubplotSpec`, optional, default: `NoneType`
        A grid locations to place the plot. If a subplotspec then a grid spec
        will be created with it, if ``NoneType``, then a grid spec will be
        created with enough space to hold the plot.
    ld_breaks : `list` of `tuple`, optional, default: `NoneType`
        The LD break points and their plot colours. Each tuple should have the
        break point (r2 between 0-1 (float)) at ``[0]`` and the colour at
        ``[1]``. Note that the upper limit of 1.0 should not be included in
        this but the lower limit of 0 can be. If ``NoneType``, then the
        defaults in ``skyline.tracks.scatter.LocusViewScatter.LD_BREAKS`` are
        used.
    lead_variant_style : `dict`, optional, default: `NoneType`
        The default style parameters for the lead variant plotting marker on
        the scatter plot. Must be appropriate for ``Axes.scatter``. A
        ``NoneType`` value defaults to
        `skyline.tracks.scatter.LocusViewScatter.LEAD_VARIANT_STYLE`.
    **kwargs
        Keyword styling parameters for points that are not represented in the
        LD data.

    Returns
    -------
    scatter_plot : `skyline.axes.BaseGenomicAxes`
        A skyline axes containing the scatter plot.

    Raises
    ------
    KeyError
        If the lead variant is not present in both region1 and region2.

    Notes
    -----
    This expects a data in the region data containers with the p-values already
    logged. The containers should also have LD data in them. The labels for the
    x/y axis are taken from the label attribute of the region data container.

    See also
    --------
    skyline.plots.locus_view.locus_pvalue
    skyline.tracks.scatter.LocusViewScatter
    """
    # Assign defaults
    lead_variant_style = (
        lead_variant_style or scatter.LocusViewScatter.LEAD_VARIANT_STYLE
    )
    ld_breaks = (
        ld_breaks or scatter.LocusViewScatter.LD_BREAKS
    )

    # Make sure the LD breaks are from low to high
    ld_breaks.sort(key=lambda x: x[0])

    # Make sure the lead variant is valid
    if region1.lead_variant.uni_id not in region2.data.index:
        raise KeyError("region1 lead variant does not intersect with region2")

    # Merge the region data
    x = pd.merge(
        region1.data.pvalue, region2.data.pvalue, left_index=True,
        right_index=True
    )
    x = x.merge(
        region1.ld_data.loc[region1.lead_variant.uni_id, ],
        left_index=True, right_index=True
    )

    # Apply the LD styling
    data_styles = dict()
    for idx, ld in enumerate(ld_breaks):
        ld, c = ld
        colname = r'$\geq {0}$'.format(ld)
        data_styles[colname] = dict(c=c)

        # Any null values will not be selected
        selector = ~x[region1.lead_variant.uni_id].isnull()
        selector = selector & (x[region1.lead_variant.uni_id] >= ld)
        try:
            selector = selector & \
                (x[region1.lead_variant.uni_id] < ld_breaks[idx+1][0])
        except IndexError:
            pass
        selector = (x.index != region1.lead_variant.uni_id) & selector
        x[colname] = selector

    # Now add the lead variant
    x['lead variant'] = (x.index == region1.lead_variant.uni_id)
    data_styles['lead variant'] = lead_variant_style

    # plot
    return locus_pvalue(
        gfig, x, tracks=tracks, xlabel=region1.label,
        data_styles=data_styles,
        ylabel=region2.label, scatter_label="no LD", **kwargs)
