"""Higher level plotting interface functions
"""
from skyline import coords
from skyline.tracks import (
    scatter,
    linkers,
    labels
)
from skyline import (
    styles,
)
from skyline.plots import (
    common
)

# import pprint as pp

_GWAS_DATA_PVALUE = 'pvalue'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def manhattan(skyfig, data, coord_region, tracks=None, label_data=None,
              loci_label_style=None, line_style=None, chr_style=None,
              chr_label_style=None, scatter_length=3, chr_label_length=1,
              label_length=5, linker_length=1, chr_padding=1E7, start_row=0,
              start_col=0, value_column=None, xpad_left=0, xpad_right=0,
              sig_limit_color_cut=styles.LIMITS, ylim=None, chr_label_rows=2,
              chr_label_valign='center'):
    """Produce a manhattan plot, this is a scatter plot of p-values on a
    genomic scale.

    Parameters
    ----------
    skyfig : `skyline.GenomicFigure`
        The figure on which to apply the plot.
    data : `pandas.DataFrame`
        GWAS data must have chromosome name, start position and -log10 p-value
        columns.
    coord_region : `skyline.coords.GenomewideCoords`
        A coordinates object to use in the plot region. The active (present)
        chromosomes will be set prior to plotting.
    tracks : `NoneType` or `matplotlib.gridspec.SubplotSpec` or
    `matplotlib.GridSpec`, optional, default: `NoneType`
        A gridspec object to arrange the tracks, if not supplied then one is
        created with enough space for the plot. This is based on the number of
        row slots used for ``scatter_length``, ``chr_label_length``,
        ``label_length``, ``linker_length``. If a ``SubplotSpec`` is given then
        a new GridSpec is created within that.
    label_data : `NoneType` or `list` of `skyline.tracks.labels.LabelCoords`,
    optional, default: `NoneType`
        Any labels to add to the manhattan plot. If this is `NoneType` or an
        empty list then no label track is created.
    loci_label_style : `NoneType` or `dict`, optional, default: `NoneType`
        A style dictionary to use for any labels on the plot. A label is
        defined by the ``skyline.tracks.labels.LabelCoords`` objects in the
        ``label_data`` list. Note that these might have their own specific
        style parameters that will override this setting. If this is
        ``NoneType`` then the default style of the
        ``skyline.tracks.labels.Labels`` is used.
    line_style : `NoneType` or `dict`,optional, default: `NoneType`
        The styles used for the label linker lines. These should be appropriate
        for the `matplotlib.axes.axline` method call.
    chr_style : `NoneType` or `list` of `str`, optional, default: `NoneType`
        The colours used for the chromosomes in the scatter plot. If the list
        is < the number of chromosomes being plotted then the colours are
        recycled. The colours should be expressed in a way that is compatible
        with matplotlib, this is typically a string of colour names or hex
        codes.
    chr_label_style : `NoneType` or `dict`, optional, default: `NoneType`
        The style for the text that depicts the chromosome name in the
        chromosome label track. The keys of the dict should be appropriate for
        ``matplotlib.text.Text``.
    scatter_length : `int`, optional, default: `3`
        The number of gridspec slots (rows occupied by the SubplotSpec) to use
        for the scatter plot.
    chr_label_length : `int`, optional, default: `5`
        The number of rows occupied by the SubplotSpec to use for the
        chromosome label length. Setting to <= 0 will turn it off.
    label_length : `int`, optional, default: `1`
        The number of rows occupied by the SubplotSpec to use for a label
        track. Setting to <=0 will turn this off and the linker track off.
    linker_length : `int`, optional, default: `2`
        The number of rows occupied by the SubplotSpec to use for the link
        between the label track and the scatter plot. Setting to <=0 will turn
        this off and the linker track off.
    chr_padding : `int`, optional, default: `1E7`
        The additional empty gap added to the end of the chromosomes so that
        plotting points on one chromsome do not overlap the next chromosome.
    start_row : `int`, optional, default: `0`
        The start row position for the plot in the GridSpec. This only needs to
        be used if the user has supplied their own GridSpec and want to provide
        some sort of offset for the manhattan plot.
    start_col : `int`, optional, default: `0`
        The start column position for the plot in the GridSpec. This only needs
        to be used if the user has supplied their own GridSpec and want to
        provide some sort of offset for the manhattan plot. This will commonly
        be 0 as most skyline plots are a single column.
    value_column : `NoneType` or `str`, optional, default: `NoneType`
        The name of the column in ``data`` with the -log10 transformed pvalue
        data in it.
    xpad_left : `int` or `float`, optional, default: `0`
        Padding to add to the left bound xaxis limit. If it is a float it is
        treated as a proportion, if it is an integer it is treated as
        basepairs.
    xpad_right : `int` or `float`, optional, default: `0`
        Padding to add to the right bound xaxis limit. If it is a float it is
        treated as a proportion, if it is an integer it is treated as
        basepairs.
    sig_limit_color_cut : `NoneType` or `list` of `tuple`, optional,
    default: `matplotlib.styles.LIMITS`
        The breakpoints of the listed color map that will define -log10(pvalue)
        cutoff values, above which the points will be coloured differently to
        the ``chromosome_style``. Each tuple should have:
        ``(<cutoff_data_value>, <color>)``. Setting to ``NoneType``, will turn
        off significance colours.
    ylim : `NoneType` or `tuple` of (`int or `float`), optional,
    default: `NoneType`
        The limits for the y-axis, if `NoneType`, then they are extracted from
        the data.
    chr_label_rows : `int`, optional, default: `2`
        The number of rows that the chromosome labels on the y-axis should
        occupy, this should be used if you have overlapping chromosome labels.
    chr_label_valign : `str`, optional, default: `center`
        The vertical alignment of the chromosome label track, this has it's own
        specialist alignment parameter as the whole block of rows is aligned as
        one.

    Returns
    -------
    mh_scatter : `skyline.tracks.scatter.GenomicScatter`
        The genomic scatterplot axes.
    chr_labels : `skyline.tracks.labels.ChrLabels`
        The axes containing the chromosome labels.
    label_track : `skyline.tracks.labels.Labels`
        The axes containing the loci labels.
    linker_track : `skyline.tracks.linkers.LineLinkerTrack`
        The axes containing the linkers between the loci labels and their
        genomic positions (in the line_track).
    line_track : `skyline.tracks.linkers.LowerBlankTrack`
        The axes containing the drop lines from the linker track.
    end_row : `int`
        The last row in the gridspec that has a plotting axes in it.

    Notes
    -----
    If no genomic tracks (matplotlib.GridSpec) is provided then
    ``row_start``/``column_start`` are set to 0. If one of ``label_length`` or
    ``linker_length`` is 0 then both are set to 0.
    """
    mh_scatter = None
    chr_labels = None
    label_track = None
    linker_track = None

    # Make sure that the default value column is set
    value_column = value_column or _GWAS_DATA_PVALUE

    # Get the unique of the chromosome
    active_chrs = data.chr_name.unique().tolist()

    start_row, tracks, label_track, linker_track, line_track = \
        _init_genomic_scatter(
            skyfig, coord_region, active_chrs, scatter_length,
            start_row=start_row, label_data=label_data,
            label_length=label_length, start_col=start_col,
            linker_length=linker_length, chr_label_length=chr_label_length,
            tracks=tracks, xpad_left=xpad_left, xpad_right=xpad_right,
            line_style=line_style, loci_label_style=loci_label_style
        )

    end_row = start_row + scatter_length

    mh_scatter = scatter.GenomicScatter(
        skyfig, tracks[start_row:end_row], coord_region, data,
        value_column, xpad_left=xpad_left, xpad_right=xpad_right,
        sig_limit_color_cut=sig_limit_color_cut, ylim=ylim,
        chromosome_style=chr_style
    )
    skyfig.add_track(mh_scatter)
    start_row = end_row
    end_row = start_row + chr_label_length
    if chr_label_length > 0:
        # ##### Add a chromosome label track ######
        chr_labels = labels.ChrLabels(
            skyfig,
            tracks[start_row:end_row, start_col], coord_region,
            chr_label_style=chr_label_style,
            rows=chr_label_rows,
            verticalalignment=chr_label_valign,
            xpad_left=xpad_left, xpad_right=xpad_right
        )
        skyfig.add_track(chr_labels)
    return mh_scatter, chr_labels, label_track, linker_track, line_track, \
        end_row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def miami(skyfig, top_data, bottom_data, coord_region, tracks=None,
          label_data=None, loci_label_style=None, line_style=None,
          chr_style=None, chr_label_style=None, scatter_length=3,
          chr_label_length=2, label_length=5, linker_length=1, chr_padding=1E7,
          start_row=0, start_col=0, value_column=None, xpad_left=0,
          xpad_right=0, sig_limit_color_cut=styles.LIMITS, ylim=None,
          chr_label_rows=2, chr_label_valign='center'):
    """Produce a Miami style plot, this is two Manhattan plots aligned with
    each other.

    Parameters
    ----------
    skyfig : `skyline.GenomicFigure`
        The figure on which to apply the plot.
    top_data : `pandas.DataFrame`
        GWAS data must have chr_name, start_pos, -log10 p-value columns.
        This data will sit in the top miami plot.
    bottom_data : `pandas.DataFrame`
        GWAS data must have chr_name, start_pos, -log10 p-value columns.
        This data will sit in the bottom miami plot.
    coord_region : `skyline.coords.GenomewideCoords`
        A coordinates object to use in the plot region. The active (present)
        chromosomes will be set prior to plotting.
    tracks : `NoneType` or `matplotlib.gridspec.SubplotSpec` or
    `matplotlib.GridSpec`, optional, default: `NoneType`
        A gridspec object to arrange the tracks, if not supplied then one is
        created with enough space for the plot. This is based on the number of
        row slots used for ``scatter_length``, ``chr_label_length``,
        ``label_length``, ``linker_length``. If a ``SubplotSpec`` is given then
        a new GridSpec is created within that.
    label_data : `list` of `skyline.tracks.labels.LabelCoords`
        Any labels to add to the manhattan plot. If this is `NoneType` or an
        empty list then no label track is created.
    loci_label_style : `NoneType` or `dict`, optional, default: `NoneType`
        A style dictionary to use for any labels on the plot. A label is
        defined by the ``skyline.tracks.labels.LabelCoords`` objects in the
        ``label_data`` list. Note that these might have their own specific
        style parameters that will override this setting. If this is
        ``NoneType`` then the default style of the
        ``skyline.tracks.labels.Labels`` is used.
    line_style : `NoneType` or `dict`, optional, default: `NoneType`
        The styles used for the label linker lines. These should be appropriate
        for the `matplotlib.axes.axline` method call.
    chr_style : `NoneType` or `list` of `str`, optional, default: `NoneType`
        The colours used for the chromosomes in the scatter plot. If the list
        is < the number of chromosomes being plotted then the colours are
        recycled. The colours should be expressed in a way that is compatible
        with matplotlib, this is typically a string of colour names or hex
        codes.
    chr_label_style : `NoneType` or `dict`, optional, default: `NoneType`
        The style for the text that depicts the chromosome name in the
        chromosome label track. The keys of the dict should be appropriate for
        ``matplotlib.text.Text``.
    scatter_length : `int`, optional, default: `3`
        The number of gridspec slots (rows occupied by the SubplotSpec) to use
        for the scatter plot.
    chr_label_length : `int`, optional, default: `5`
        The number of rows occupied by the SubplotSpec to use for the
        chromosome label length. Setting to <= 0 will turn it off.
    label_length : `int`, optional, default: `1`
        The number of rows occupied by the SubplotSpec to use for a label
        track. Setting to <=0 will turn this off and the linker track off.
    linker_length : `int`, optional, default: `2`
        The number of rows occupied by the SubplotSpec to use for the link
        between the label track and the scatter plot. Setting to <=0 will turn
        this off and the linker track off.
    chr_padding : `int`, optional, default: `1E7`
        The additional empty gap added to the end of the chromosomes so that
        plotting points on one chromsome do not overlap the next chromosome.
    start_row : `int`, optional, default: `0`
        The start row position for the plot in the GridSpec. This only needs to
        be used if the user has supplied their own GridSpec and want to provide
        some sort of offset for the manhattan plot.
    start_col : `int`, optional, default: `0`
        The start column position for the plot in the GridSpec. This only needs
        to be used if the user has supplied their own GridSpec and want to
        provide some sort of offset for the manhattan plot. This will commonly
        be 0 as most skyline plots are a single column.
    value_column : `NoneType` or `str`, optional, default: `NoneType`
        The name of the column in ``data`` with the -log10 transformed pvalue
        data in it.
    xpad_left : `int` or `float`, optional, default: `0`
        Padding to add to the left bound xaxis limit. If it is a float it is
        treated as a proportion, if it is an integer it is treated as
        basepairs.
    xpad_right : `int` or `float`, optional, default: `0`
        Padding to add to the right bound xaxis limit. If it is a float it is
        treated as a proportion, if it is an integer it is treated as
        basepairs.
    sig_limit_color_cut : `NoneType` or `list` of `tuple`, optional,
    default: `matplotlib.styles.LIMITS`
        The breakpoints of the listed color map that will define -log10(pvalue)
        cutoff values, above which the points will be coloured differently to
        the ``chromosome_style``. Each tuple should have:
        ``(<cutoff_data_value>, <color>)``. Setting to ``NoneType``, will turn
        off significance colours.
    ylim : `NoneType` or `tuple` of (`int or `float`), optional,
    default: `NoneType`
        The limits for the y-axis, if `NoneType`, then they are extracted from
        the data.
    chr_label_rows : `int`, optional, default: `2`
        The number of rows that the chromosome labels on the y-axis should
        occupy, this should be used if you have overlapping chromosome labels.
    chr_label_valign : `str`, optional, default: `center`
        The vertical alignment of the chromosome label track, this has it's own
        specialist alignment parameter as the whole block of rows is aligned as
        one.

    Returns
    -------
    top : `skyline.tracks.scatter.GenomicScatter`
        The top genomic scatterplot axes.
    bottom : `skyline.tracks.scatter.GenomicScatter`
        The bottom genomic scatterplot axes.
    chr_labels : `skyline.tracks.labels.ChrLabels`
        The axes containing the chromosome labels.
    label_track : `skyline.tracks.labels.Labels`
        The axes containing the loci labels.
    linker_track : `skyline.tracks.linkers.LineLinkerTrack`
        The axes containing the linkers between the loci labels and their
        genomic positions (in the line_track).
    line_track : `skyline.tracks.linkers.LowerBlankTrack`
        The axes containing the drop lines from the linker track.
    end_row : `int`
        The last row in the gridspec that has a plotting axes in it.

    Notes
    -----
    If no genomic tracks (matplotlib.GridSpec) is provided then
    ``row_start``/``column_start`` are set to 0. If one of ``label_length`` or
    ``linker_length`` is 0 then both are set to 0.
    """
    top = None
    bottom = None
    chr_labels = None
    label_track = None
    linker_track = None

    # Make sure that the default value column is set
    value_column = value_column or _GWAS_DATA_PVALUE

    # Get the unique of the chromosome
    active_chrs = list(
        set(top_data.chr_name).intersection(set(bottom_data.chr_name))
    )

    start_row, tracks, label_track, linker_track, line_track = \
        _init_genomic_scatter(
            skyfig, coord_region, active_chrs, scatter_length*2,
            start_row=start_row, label_data=label_data,
            label_length=label_length, start_col=start_col,
            linker_length=linker_length, chr_label_length=chr_label_length,
            tracks=tracks, xpad_left=xpad_left, xpad_right=xpad_right,
            line_style=line_style, loci_label_style=loci_label_style,
            drop_line_length=scatter_length
        )

    if ylim is None:
        ylim = (
            0, max(
                max(top_data[value_column]),
                max(bottom_data[value_column])
            ) * 1.01
        )

    end_row = start_row + scatter_length

    top = scatter.GenomicScatter(
        skyfig, tracks[start_row:end_row], coord_region, top_data,
        value_column, xpad_left=xpad_left, xpad_right=xpad_right,
        sig_limit_color_cut=sig_limit_color_cut, ylim=ylim,
        chromosome_style=chr_style
    )

    start_row = end_row
    end_row = start_row + chr_label_length
    if chr_label_length > 0:
        # ##### Add a chromosome label track ######
        chr_labels = labels.ChrLabels(
            skyfig,
            tracks[start_row:end_row, start_col], coord_region,
            chr_label_style=chr_label_style,
            rows=chr_label_rows,
            verticalalignment=chr_label_valign,
            xpad_left=xpad_left, xpad_right=xpad_right,
        )
        skyfig.add_track(chr_labels)

    start_row = end_row
    end_row = start_row + scatter_length

    bottom = scatter.GenomicScatter(
        skyfig, tracks[start_row:end_row], coord_region, bottom_data,
        value_column, xpad_left=xpad_left, xpad_right=xpad_right,
        sig_limit_color_cut=sig_limit_color_cut, ylim=ylim,
        chromosome_style=chr_style
    )
    bottom.invert_yaxis()
    skyfig.add_track(top)
    skyfig.add_track(bottom)
    return top, bottom, chr_labels, label_track, linker_track, line_track, \
        end_row


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_genomewide_coords(coord_region, active_chrs):
    """Validate and create a coordinates object if necessary.

    Parameters
    ----------
    coord_region : `skyline.coords.GenomewiderCoords`
        A coordinates object to use in the plot region.
    active_chrs : `list` of `str`
        The active chromosomes to set in the coordinate region.

    Returns
    -------
    coords : `skyline.coords.ChrCoords`
        A coordinates object.
    """
    if not issubclass(coord_region.__class__, coords.GenomewideCoords):
        raise TypeError(
            "'coord_region' should be a GenomewideCoords object or NoneType"
        )
    coord_region.set_all_inactive()
    coord_region.set_active(active_chrs)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_genomic_scatter(skyfig, coord_region, active_chrs, scatter_length,
                          drop_line_length=None, label_data=None,
                          label_length=0, linker_length=0, chr_label_length=0,
                          tracks=None, xpad_left=None, xpad_right=None,
                          line_style=None, start_row=0, start_col=0,
                          loci_label_style=None):
    """A common genomewide scatter plot initialiser.

    This is called by both the miami and manhattan plot functions to process
    the command line arguments and set up tracks.

    Parameters
    ----------
    skyfig : `skyline.GenomicFigure`
        The figure on which to apply the plot.
    coord_region : `skyline.coords.GenomewideCoords`
        A coordinates object to use in the plot region. The active (present)
        chromosomes will be set prior to plotting.
    active_chrs : `list` of `str`
        The active chromosomes to set in the coordinate region.
    scatter_length : `int`, optional, default: `3`
        The number of gridspec slots (rows occupied by the SubplotSpec) to use
        for the scatter plot.
    drop_line_length : `NoneType` or `int`, optional, default: `NoneType`
        The length of the drop line track. This has lines down from the linker
        track. If not supplied, then this will default to the length of the
        linker track.
    label_data : `NoneType` or `list` of `skyline.tracks.labels.LabelCoords`
    optional, default `NoneType`
        Any labels to add to the manhattan plot. If this is `NoneType` or an
        empty list then no label track is created.
    label_length : `int`, optional, default: `0`
        The number of rows occupied by the SubplotSpec to use for a label
        track. Setting to <=0 will turn this off and the linker track off.
    linker_length : `int`, optional, default: `0`
        The number of rows occupied by the SubplotSpec to use for the link
        between the label track and the scatter plot. Setting to <=0 will turn
        this off and the linker track off.
    chr_label_length : `int`, optional, default: `0`
        The number of rows occupied by the SubplotSpec to use for the
        chromosome label length. Setting to <= 0 will turn it off.
    tracks : `NoneType` or `matplotlib.gridspec.SubplotSpec` or
    `matplotlib.GridSpec`, optional, default: `NoneType`
        A gridspec object to arrange the tracks, if not supplied then one is
        created with enough space for the plot. This is based on the number of
        row slots used for ``scatter_length``, ``chr_label_length``,
        ``label_length``, ``linker_length``. If a ``SubplotSpec`` is given then
        a new GridSpec is created within that.
    xpad_left : `int` or `float`, optional, default: `0`
        Padding to add to the left bound xaxis limit. If it is a float it is
        treated as a proportion, if it is an integer it is treated as
        basepairs.
    xpad_right : `int` or `float`, optional, default: `0`
        Padding to add to the right bound xaxis limit. If it is a float it is
        treated as a proportion, if it is an integer it is treated as
        basepairs.
    line_style : `NoneType` or `dict`, optional, default: `NoneType`
        The styles used for the label linker lines. These should be appropriate
        for the `matplotlib.axes.axline` method call.
    start_row : `int`, optional, default: `0`
        The start row position for the plot in the GridSpec. This only needs to
        be used if the user has supplied their own GridSpec and want to provide
        some sort of offset for the manhattan plot.
    start_col : `int`, optional, default: `0`
        The start column position for the plot in the GridSpec. This only needs
        to be used if the user has supplied their own GridSpec and want to
        provide some sort of offset for the manhattan plot. This will commonly
        be 0 as most skyline plots are a single column.
    loci_label_style : `NoneType` or `dict`, optional, default: `NoneType`
        A style dictionary to use for any labels on the plot. A label is
        defined by the ``skyline.tracks.labels.LabelCoords`` objects in the
        ``label_data`` list. Note that these might have their own specific
        style parameters that will override this setting. If this is
        ``NoneType`` then the default style of the
        ``skyline.tracks.labels.Labels`` is used.

    Returns
    -------
    start_row : `int`
        The row in the gridspec that the scatter plot should be started from.
    tracks : `matplotlib.GridSpec`
        A gridspec object to arrange the tracks.
    label_track : `skyline.tracks.labels.Labels`
        The axes containing the loci labels.
    linker_track : `skyline.tracks.linkers.LineLinkerTrack`
        The axes containing the linkers between the loci labels and their
        genomic positions (in the line_track).
    line_track : `skyline.tracks.linkers.LowerBlankTrack`
        The axes containing the drop lines from the linker track.
    """
    label_track = None
    linker_track = None
    line_track = None

    # Set the drop line length if needed if, not supplied then it will
    # default to the scatter length
    drop_line_length = drop_line_length or scatter_length

    # Validate or create a coordinates region object
    _set_genomewide_coords(
        coord_region, active_chrs
    )

    # If the label or linker is not defined then it is not used
    has_labels = True
    if label_data is None or label_length == 0 or linker_length == 0:
        label_length = 0
        linker_length = 0
        has_labels = False

    # Make sure the gridspec is properly defined
    nrows = sum(
        [scatter_length, label_length, linker_length, chr_label_length]
    )
    tracks = common.get_gridspec(
        tracks, nrows=nrows, ncols=1, wspace=0, hspace=0
    )

    if has_labels is True:
        label_track = labels.Labels(
            skyfig, tracks[start_row:label_length, start_col],
            coord_region, label_data, label_style=loci_label_style,
            xpad_left=xpad_left, xpad_right=xpad_right
        )
        end = label_length + linker_length

        links = []
        for i, j in label_track.label_plot_coords:
            links.append(linkers.LinkCoords(i, j))

        linker_track = linkers.LineLinkerTrack(
            skyfig, tracks[label_length:end, start_col], coord_region, links,
            linker_style=line_style, xpad_left=xpad_left, xpad_right=xpad_right
        )

        start_row = end

        # The end row here is the space that will be occupied by the
        # scatter plot (or top scatter plot)
        end_row = start_row + drop_line_length
        line_track = linkers.LowerBlankTrack(
            skyfig, tracks[start_row:end_row], coord_region,
            xpad_left=xpad_left, xpad_right=xpad_right
        )

        # Line style could be NoneType, in which case we can't do **
        line_style = line_style or dict()
        line_style = {**styles.LINE_LINK_STYLE, **line_style}
        line_track.axvlines_from_linker(links, **line_style)

        skyfig.add_track(label_track)
        skyfig.add_track(linker_track)
        skyfig.add_track(line_track)

    return start_row, tracks, label_track, linker_track, line_track
