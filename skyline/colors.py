"""Some predefined colours to use and utility functions for handling colors.
"""
from matplotlib import colors


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def add_alpha(color, alpha):
    """Utility function for adding an alpha to a colour. Added as I keep on
    forgetting how to do this!

    Parameters
    ----------
    color : `color`
        An object that can be interpreted  as a colour by `matplotlib`
    alpha : `float`
        The amount of alpha to add to the colour. 1 is no alpha and 0 is full
        alpha.

    Returns
    -------
    color_with_alpha : `color`
        The colour with the alpha added.
    """
    alpha_col = colors.to_rgba(color, alpha=alpha)
    return colors.to_hex(alpha_col, keep_alpha=True)


MANHATTAN_GREY_ODD = '#525252'
"""The grey colour used as a default on Manhattan plots for odd chromosomes
(`str`)
"""
MANHATTAN_GREY_EVEN = '#B3B3B3'
"""The grey colour used as a default on Manhattan plots for even chromosomes
(`str`)
"""
SIGNIF_PURPLE = '#9464AA'
"""Purple in as the default suggestive significance in Manhattan plots
(`str`)
"""
GITLAB_ORANGE = '#FB8624'
"""Orange colour used in the GitLab logo and also used as the default
significance in Manhattan plots (`str`)
"""
CHR_LIGHT_GREY1 = '#F2F2F2'
CHR_LIGHT_GREY2 = '#B2B2B2'
CHR_BLUE1 = '#6495ED'
CHR_BLUE2 = '#00006C'
BLACK = "black"
DARK_GREY = "dark_grey"
COL3 = '#C2D9E6'
COL4 = '#9FB8C603'
COL5 = '#6F8B9A03'
SIGNIF_GREEN = '#00CD66'
GENE_BLUE = "#00006C"
BACKGROUND_GREY = "#D2D2D2"

COLOR_CYCLES = [
    SIGNIF_PURPLE,
    SIGNIF_GREEN,
    GITLAB_ORANGE,
    CHR_BLUE1,
    COL3,
    COL4,
    COL5
]
