"""A helper module for interaction with the Ensembl REST API.
"""
from ensembl_rest_client import client, utils
from skyline import utils as su, features
import pprint as pp


# This should probably be available in the rest API
CODING_BIOTYPES = [
    'Protein_Coding', 'IG_C_gene', 'IG_D_gene', 'IG_J_gene', 'IG_LV_gene',
    'TR_D_gene', 'IG_V_gene', 'TR_C_gene', 'TR_J_gene', 'TR_V_gene'
]
"""The biotypes that represent coding genes in Ensembl (`list` of `str`)
"""
_CODING_BIOTYPES = [i.lower() for i in CODING_BIOTYPES]
"""A private lowercase version of ``skyline.ensembl.CODING_BIOTYPES`` used
for matching against biotypes in Ensembl (`list` of `str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_human_b37_rest_client(ping=False):
    """A helper function to return the human GRCh37 Ensembl Rest Client object.

    Returns
    -------
    rest_client : `ensembl_rest_client.client.Rest`
        A rest client object targeted against GRCh37
    """
    return client.Rest(url='https://grch37.rest.ensembl.org', ping=ping)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_human_b38_rest_client(ping=False):
    """A helper function to return the human GRCh38 Ensembl Rest Client object.

    Returns
    -------
    rest_client : `ensembl_rest_client.client.Rest`
        A rest client object targeted against GRCh38
    """
    return client.Rest(url='https://rest.ensembl.org', ping=ping)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_ld_data(rest_client, chr_name, start_pos, ref_allele, alt_allele,
                species='human', population_name='1000GENOMES:phase_3:EUR',
                **kwargs):
    """Get variants in LD with the variant coords and alleles, within a
    certain region size.

    Parameters
    ----------
    rest_client : `ensembl_rest_client.client.Rest`
        The rest client object.
    chr_name : `str`
        The chromosome name for the variant.
    start_pos : `int`
        The start position for the variant.
    ref_allele : `str`
        The reference allele for the variant.
    alt_allele : `str`
        The alternate allele for the variant.
    species : `str`, optional, default: `human`
        The species for the LD panel.
    species : `str`, optional, default: `1000GENOMES:phase_3:EUR`
        The population for the LD panel, default is 1000 genomes European.
    **kwargs
        Keyword aruments to `ensembl_rest_client.Rest.get_ld`. Note, the
        ``attrib`` argument is ignored and on by default.

    Returns
    -------
    var_ld : `list` or `tuple`
        Each tuple is a pair of variants with an r2 measure (float). Each
        variant in the pair is a tuple with:
        (chr_name, start_pos (<ALLELES>), variation ID).

    Raises
    ------
    KeyError
        If the chromosome coordinates are not associated with an rsID

    See also
    --------
    enseml_rest_client.utils.get_ld
    """
    # Use the rest API to get the LD measures
    ld = utils.get_ld(rest_client, chr_name, start_pos, ref_allele, alt_allele,
                      species=species, population_name=population_name,
                      **kwargs)
    # Process to a more streamlined version and return
    var_ld = []
    for i in ld:
        try:
            var_ld.append(
                (
                    (
                        chr_name,
                        start_pos,
                        (ref_allele, alt_allele),
                        i['source_variant']['id']
                    ),
                    (
                        i['ld_variant']['seq_region_name'],
                        i['ld_variant']['start'],
                        tuple(i['ld_variant']['alleles']),
                        i['variation']
                    ), float(i['r2'])
                )
            )
        except KeyError:
            # Sometimes no `seq_region_name`
            pass
    return var_ld


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_genes_within(rc, chr_name, start_pos, end_pos=None, species='human',
                     biotypes=None, canonical=False):
    """Get the genes overlapping ``chr_name:start_pos-end_pos`` and their
    exon/utr structure.

    Parameters
    ----------
    rc : `ensembl_rest_client.client.Rest`
        An appropriate rest client instance to perform the query.
    start_pos : `int`
        The start position in bp
    end_pos : `int` or `NoneType`, optional, default: `NoneType`
        The end position in base pairs. If not provided it will default to the
        start position.
    species : `str`, optional, default: `human`
        The species for the gene lookup.
    biotypes : `NoneType` or `list` of `str`, optional: default: `NoneType`
        Restrict the gene list to only genes matching the requested biotypes.
       ``NoneType`` will return any overlapping gene regardless of biotype.
       The biotypes are evaluated on a per-transcript basis not gene.
    canonical : `bool`, optional, default: `False`
        Only return canonical transcripts for each gene.

    Returns
    -------
    list : `skyline.features.ExonTranscript`
        Transcripts with their exon structures defined.

    See also
    --------
    enseml_rest_client.utils.get_genes_within
    """
    gene_data = []

    # Make end pos the same as start pos if not defined
    end_pos = end_pos or start_pos

    # Firstly we get the gene IDs within the region of interest
    genes = utils.get_genes_within(
        rc, species, chr_name, start_pos, end_pos=end_pos,
        use_center=False, biotypes=biotypes
    )
    # Now, loop through and get the exons for the genes
    for i in genes:
        trans = get_gene_transcripts(rc, i['id'], canonical=canonical)
        if biotypes is None:
            gene_data.extend(trans)
        else:
            gene_data.extend([t for t in trans if t.biotype in biotypes])

    return gene_data


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_gene_transcripts(rc, gene_id, canonical=False):
    """Get the transcripts, exon.utr structure for a gene.
    exon/utr structure.

    Parameters
    ----------
    rc : `ensembl_rest_client.client.Rest`
        An appropriate rest client instance to perform the query.
    gene_id : `str`
        The ensembl gene identifier. i.e. ``ENSG*`` in humans.
    canonical : `bool`, optional, default: `False`
        Only return canonical transcripts for each gene.

    Returns
    -------
    list : `skyline.features.ExonTranscript`
        Transcripts with their exon structures defined.

    See also
    --------
    enseml_rest_client.utils.get_lookup_id
    """
    # print(canonical)
    gene_data = rc.get_lookup_id(gene_id, expand=True, utr=True)
    try:
        gene_name = gene_data['display_name']
    except KeyError:
        gene_name = gene_data['id']

    transcripts = gene_data['Transcript']
    # Get the canonical ones
    if canonical is True:
        transcripts = [
            t for t in transcripts if t['is_canonical'] == 1
        ]
        if len(transcripts) != 1:
            pp.pprint(transcripts)
            raise RuntimeError("expected single canonical transcript")

    proc_transcripts = []
    for t in transcripts:
        try:
            utrs = [(u['start'], u['end']) for u in t['UTR']]
        except KeyError:
            utrs = []

        exons = []
        for e in t['Exon']:
            # Sanity check for exons on different chrs than query - this should
            # not happen but I will check
            if e['seq_region_name'] != t['seq_region_name']:
                raise ValueError(
                    "exon chr_name != query transcript: {0} != {1}".format(
                        e['seq_region_name'], t['seq_region_name']
                    )
                )
            exon_start = e['start']
            exon_end = e['end']

            utr_region = None
            for idx in range(len(utrs)):
                if su.overlap_1d((exon_start, exon_end), utrs[idx]) > 0:
                    utr_region = utrs.pop(idx)
                    break
            exons.append(
                features.Exon(
                    e['seq_region_name'], exon_start, end_pos=exon_end,
                    utr_region=utr_region, strand=e['strand'], label=e['id']
                )
            )

        try:
            label = t['display_name']
        except KeyError:
            label = None

        proc_transcripts.append(
            features.ExonTranscript(
                exons, trans_id=t['id'], strand=t['strand'], label=label,
                biotype=t['biotype']
            )
        )
    if canonical is True:
        proc_transcripts[0].trans_id = gene_data['id']
        proc_transcripts[0].label = gene_name
    return proc_transcripts
