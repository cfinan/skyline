"""Linker, Blank and Ruler tracks.
"""
from skyline import axes, styles, features
from matplotlib.path import Path
from matplotlib import patches
from matplotlib.ticker import FuncFormatter
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class LinkCoords(features._BaseFeatureCoords):
    """A representation of linker coordinates and styles.

    Parameters
    ----------
    link_from : (`tuple` or `list`)
        The start coordinates of the link. Each tuple should be of length
        three with ``[0]`` being the start and
        ``[1]`` being the end position.
    link_to : (`tuple` or `list`)
        The end coordinates of the link. Each tuple should be of length
        three with ``[0]`` being the start and
        ``[1]`` being the end position.
    *args
        Any arguments to be passed to the parent class.
    *kwargs
        Any keyword arguments to be passed to the parent class.

    Notes
    -----
    Currently the links must be within the same chromosome, so it does not
    really have any impact on the coordinate calculations and is more of a
    placeholder.
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, link_from, link_to, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.link_from = link_from
        self.link_to = link_to

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_args(self):
        """Return all the arguments and keyword arguments for creating a copy.

        Returns
        -------
        args : `list`
            Arguments that can be used to make a copy.
        kwargs : `dict`
            Kaeyword arguments that can be used to make a copy.
        """
        super_args, super_kwargs = super().get_args()

        return (
            super_args + [self.link_from, self.link_to],
            super_kwargs
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def link_from_start(self):
        """The start positon of the from link (`int`)
        """
        return self.link_from.start_pos

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def link_from_end(self):
        """The end positon of the from link (`int`)
        """
        return self.link_from.end_pos

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def link_to_start(self):
        """The start positon of the to link (`int`)
        """
        return self.link_to.start_pos

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def link_to_end(self):
        """The end positon of the to link (`int`)
        """
        return self.link_to.end_pos

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def name(self):
        """The link name (`str`)
        """
        return self._name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def from_link(self):
        """Alias for ``link_from`` (`tuple` or `int`)
        """
        return self.link_from

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def to_link(self):
        """Alias for ``link_to`` (`tuple` or `int`)
        """
        return self.link_to


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class BlankTrack(axes.BaseGenomicAxes):
    """A blank empty track.

    Parameters
    ----------
    figure : `matplotlib.Figure` or `skyline.GenomicFigure`
        The figure that the Axes will be associated with.
    track : `matplotlib.gridspec.SubplotSpec`
        The track grid coordinates that the Axes should be added to. These are
        obtained from a `matplotlib.GridSpec` subset.
    coords : `skyline.coords.ChrCoords`
        An object inheriting from `skyline.coords.ChrCoords`
    *args
        Arguments to `skyline.axes.BaseGenomicAxes` and `matplotlib.Axes`.
    **kwargs
        Keyword arguments to `skyline.axes.BaseGenomicAxes` and
        `matplotlib.Axes`.

    Notes
    -----
    The idea here is that you can use this to set the coordinates system and
    then use it as an ``matplotlib.Axes``. It is essentially the same as
    ``skyline.axes.BaseGenomicAxes`` but may get more functionality later.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, *args,
                 **kwargs):
        super().__init__(figure, track, coords, *args, **kwargs)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class LowerBlankTrack(axes.BaseGenomicAxes):
    """A blank empty track with a low z-order.

    Parameters
    ----------
    figure : `matplotlib.Figure` or `skyline.GenomicFigure`
        The figure that the Axes will be associated with.
    track : `matplotlib.gridspec.SubplotSpec`
        The track grid coordinates that the Axes should be added to. These are
        obtained from a `matplotlib.GridSpec` subset.
    coords : `skyline.coords.ChrCoords`
        An object inheriting from `skyline.coords.ChrCoords`
    *args
        Arguments to `skyline.axes.BaseGenomicAxes` and `matplotlib.Axes`.
    **kwargs
        Keyword arguments to `skyline.axes.BaseGenomicAxes` and
        `matplotlib.Axes`.

    Notes
    -----
    Please note this implementation does not seem to work as intended. I might
    remove it if I can't figure it out. Please use
    ``skyline.linkers.BlankTrack`` instead and add to the figure before any
    other tracks.

    See also
    --------
    skyline.linkers.BlankTrack
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, *args, zorder=-100000,
                 **kwargs):
        super().__init__(figure, track, coords, *args, **kwargs)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class UpperBlankTrack(axes.BaseGenomicAxes):
    """A blank empty track with a high z-order.

    Parameters
    ----------
    figure : `matplotlib.Figure` or `skyline.GenomicFigure`
        The figure that the Axes will be associated with.
    track : `matplotlib.gridspec.SubplotSpec`
        The track grid coordinates that the Axes should be added to. These are
        obtained from a `matplotlib.GridSpec` subset.
    coords : `skyline.coords.ChrCoords`
        An object inheriting from `skyline.coords.ChrCoords`
    *args
        Arguments to `skyline.axes.BaseGenomicAxes` and `matplotlib.Axes`.
    **kwargs
        Keyword arguments to `skyline.axes.BaseGenomicAxes` and
        `matplotlib.Axes`.

    Notes
    -----
    Please note this implementation does not seem to work as intended. I might
    remove it if I can't figure it out. Please use
    ``skyline.linkers.BlankTrack`` instead and add to the figure after any
    other tracks.

    See also
    --------
    skyline.linkers.BlankTrack
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, *args, zorder=100000,
                 **kwargs):
        super().__init__(figure, track, coords, *args, **kwargs)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TopRulerTrack(axes.BaseGenomicAxes):
    """An axis that just acts as a genomic coordinate xaxis, with the tick
    labels placed above the Axis.

    Parameters
    ----------
    figure : `matplotlib.Figure` or `skyline.GenomicFigure`
        The figure that the Axes will be associated with.
    track : `matplotlib.gridspec.SubplotSpec`
        The track grid coordinates that the Axes should be added to. These are
        obtained from a `matplotlib.GridSpec` subset.
    coords : `skyline.coords.ChrCoords`
        An object inheriting from `skyline.coords.ChrCoords`
    *args
        Arguments to `skyline.axes.BaseGenomicAxes` and `matplotlib.Axes`.
    coord_units : `tuple` of (`int`, `str`), optional, default: (`1E6`, `Mbp`)
        The plot units, The denominator is at ``[0]`` i.e
        ``base pairs/coord_units[0]`` and the unit text is as ``[1]``.
    plot_unit_text_label : `bool`, optional, default: `True`
        If set to ``False``, then do not plot any unit text label.
    unit_text_style : `dict`, optional, default: `NoneType`
        The styling for the unit text label. These should be appropriate for a
        call to ``matplotlib.Axes.xaxis.set_label_text``.
    **kwargs
        Keyword arguments to `skyline.axes.BaseGenomicAxes` and
        `matplotlib.Axes`.

    Notes
    -----
    It is sometimes more convenient to have this in a separate Axes if the
    other plotting Axes have joiners between them.

    See also
    --------
    matplotlib.Axes.xaxis.set_label_text
    """
    UNIT_TEXT_COORDS = (0.5, 2.5)
    """The default placement of the coordinate unit label. (`float`, `float`)
    """
    OFFSET_TEXT_STYLE = axes.BaseGenomicAxes.STYLE_CLASS(
            ha='right', va='top', fontsize=5, fontweight='bold'
        )
    """The default style for the of the coordinate unit label. (`dict`)
    """
    OFFSET_TEXT_KEY = "OFFSET_TEXT"
    """The name of the coordinate unit label in the class style dict (`str`)
    """
    STYLE = axes.BaseGenomicAxes.STYLE.copy()
    """The class style dict this is 'inherited' from the base class (`dict`)
    """
    # Update styling parameters for the ruler unit text label
    STYLE[OFFSET_TEXT_KEY] = OFFSET_TEXT_STYLE

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, *args,
                 coord_units=(1E6, 'Mbp'), plot_unit_text_label=True,
                 unit_text_style=None, unit_text_coords=UNIT_TEXT_COORDS,
                 **kwargs):
        self._sl_offset_text_coords = unit_text_coords
        self._sl_offset_text_kwargs = self.get_skyline_style_kwargs(
            unit_text_style, self.OFFSET_TEXT_STYLE
        )
        self._sl_plot_unit_text_label = plot_unit_text_label
        self._sl_units = coord_units
        super().__init__(figure, track, coords, *args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_skyline_plot_area(self):
        """Setup the skyline Axes plotting area. This sets attributes that are
        common to many of the Axes that inherit from `skyline.BaseGenomicAxes`
        """
        super().set_skyline_plot_area()
        self.xaxis.set_visible(True)
        self.xaxis.set_visible(True)
        self.spines['top'].set_visible(True)
        self.xaxis.get_offset_text().set_visible(False)

        try:
            # We use the unit exponent to set a formatter function
            self.xaxis.set_major_formatter(
                get_converter(exponent=self._sl_units[0])
            )
        except TypeError:
            # To handle earlier versions of matplotlib
            self.xaxis.set_major_formatter(
                FuncFormatter(get_converter(exponent=self._sl_units[0]))
            )
        self.xaxis.tick_top()
        self.xaxis.set_label_position('top')

        if self._sl_plot_unit_text_label is True:
            self.xaxis.set_label_text(
                self._sl_units[1], **self._sl_offset_text_kwargs
            )
            self.xaxis.set_label_coords(
                *self._sl_offset_text_coords, transform=self.transAxes
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class BottomRulerTrack(axes.BaseGenomicAxes):
    """An axis that just acts as a genomic coordinate xaxis, with the tick
    labels placed below the Axis.

    Parameters
    ----------
    figure : `matplotlib.Figure` or `skyline.GenomicFigure`
        The figure that the Axes will be associated with.
    track : `matplotlib.gridspec.SubplotSpec`
        The track grid coordinates that the Axes should be added to. These are
        obtained from a `matplotlib.GridSpec` subset.
    coords : `skyline.coords.ChrCoords`
        An object inheriting from `skyline.coords.ChrCoords`
    *args
        Arguments to `skyline.axes.BaseGenomicAxes` and `matplotlib.Axes`.
    coord_units : `tuple` of (`int`, `str`), optional, default: (`1E6`, `Mbp`)
        The plot units, The denominator is at ``[0]`` i.e
        ``base pairs/coord_units[0]`` and the unit text is as ``[1]``.
    plot_unit_text_label : `bool`, optional, default: `True`
        If set to ``False``, then do not plot any unit text label.
    unit_text_style : `dict`, optional, default: `NoneType`
        The styling for the unit text label. These should be appropriate for a
        call to ``matplotlib.Axes.xaxis.set_label_text``.
    **kwargs
        Keyword arguments to `skyline.axes.BaseGenomicAxes` and
        `matplotlib.Axes`.

    Notes
    -----
    It is sometimes more convenient to have this in a separate Axes if the
    other plotting Axes have joiners between them.

    See also
    --------
    skyline.linkers.BlankTrack
    matplotlib.Axes.xaxis.set_label_text
    """
    UNIT_TEXT_COORDS = (0.5, 2.5)
    """The default placement of the coordinate unit label. (`float`, `float`)
    """
    OFFSET_TEXT_STYLE = axes.BaseGenomicAxes.STYLE_CLASS(
            ha='right', va='top', fontsize=5, fontweight='bold'
        )
    """The default style for the of the coordinate unit label. (`dict`)
    """
    OFFSET_TEXT_KEY = "OFFSET_TEXT"
    """The name of the coordinate unit label in the class style dict (`str`)
    """
    STYLE = axes.BaseGenomicAxes.STYLE.copy()
    """The class style dict this is 'inherited' from the base class (`dict`)
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, *args,
                 coord_units=(1E6, 'Mbp'), plot_unit_text_label=True,
                 unit_text_style=None, unit_text_coords=UNIT_TEXT_COORDS,
                 **kwargs):
        self._sl_offset_text_coords = unit_text_coords
        self._sl_offset_text_kwargs = self.get_skyline_style_kwargs(
            unit_text_style, self.OFFSET_TEXT_STYLE
        )
        self._sl_plot_unit_text_label = plot_unit_text_label
        self._sl_units = coord_units
        super().__init__(figure, track, coords, *args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_skyline_plot_area(self):
        """Setup the skyline Axes plotting area. This sets attributes that are
        common to many of the Axes that inherit from `skyline.BaseGenomicAxes`
        """
        super().set_skyline_plot_area()
        self.xaxis.set_visible(True)

        try:
            # We use the unit exponent to set a formatter function
            self.xaxis.set_major_formatter(
                get_converter(exponent=self._sl_units[0])
            )
        except TypeError:
            # To handle earlier versions of matplotlib
            self.xaxis.set_major_formatter(
                FuncFormatter(get_converter(exponent=self._sl_units[0]))
            )

        if self._sl_plot_unit_text_label is True:
            self.xaxis.set_label_text(
                self._sl_units[1], **self._sl_offset_text_kwargs
            )
            self.xaxis.set_label_coords(
                *self._sl_offset_text_coords, transform=self.transAxes
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class CenterRulerTrack(axes.BaseGenomicAxes):
    """An axis that just acts as a genomic coordinate xaxis, with the tick
    labels above and below the Axis and the tick labels in between.

    Parameters
    ----------
    figure : `matplotlib.Figure` or `skyline.GenomicFigure`
        The figure that the Axes will be associated with.
    track : `matplotlib.gridspec.SubplotSpec`
        The track grid coordinates that the Axes should be added to. These are
        obtained from a `matplotlib.GridSpec` subset.
    coords : `skyline.coords.ChrCoords`
        An object inheriting from `skyline.coords.ChrCoords`
    *args
        Arguments to `skyline.axes.BaseGenomicAxes` and `matplotlib.Axes`.
    coord_units : `tuple` of (`int`, `str`), optional, default: (`1E6`, `Mbp`)
        The plot units, The denominator is at ``[0]`` i.e
        ``base pairs/coord_units[0]`` and the unit text is as ``[1]``.
    plot_unit_text_label : `bool`, optional, default: `True`
        If set to ``False``, then do not plot any unit text label.
    unit_text_style : `dict`, optional, default: `NoneType`
        The styling for the unit text label. These should be appropriate for a
        call to ``matplotlib.Axes.xaxis.set_label_text``.
    **kwargs
        Keyword arguments to `skyline.axes.BaseGenomicAxes` and
        `matplotlib.Axes`.

    Notes
    -----
    It is sometimes more convenient to have this in a separate Axes if the
    other plotting Axes have joiners between them.

    See also
    --------
    skyline.linkers.BlankTrack
    matplotlib.Axes.xaxis.set_label_text
    """
    UNIT_TEXT_COORDS = (1, 0.5)
    """The default placement of the coordinate unit label. (`float`, `float`)
    """
    OFFSET_TEXT_STYLE = axes.BaseGenomicAxes.STYLE_CLASS(
            ha='right', va='top', fontsize=5, fontweight='bold'
        )
    """The default style for the of the coordinate unit label. (`dict`)
    """
    OFFSET_TEXT_KEY = "OFFSET_TEXT"
    """The name of the coordinate unit label in the class style dict (`str`)
    """
    STYLE = axes.BaseGenomicAxes.STYLE.copy()
    """The class style dict this is 'inherited' from the base class (`dict`)
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, *args,
                 coord_units=(1E6, 'Mbp'), plot_unit_text_label=True,
                 unit_text_style=None, unit_text_coords=UNIT_TEXT_COORDS,
                 **kwargs):
        self._sl_offset_text_coords = unit_text_coords
        self._sl_offset_text_kwargs = self.get_skyline_style_kwargs(
            unit_text_style, self.OFFSET_TEXT_STYLE
        )
        self._sl_plot_unit_text_label = plot_unit_text_label
        self._sl_units = coord_units
        super().__init__(figure, track, coords, *args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_skyline_plot_area(self):
        """Setup the skyline Axes plotting area. This sets attributes that are
        common to many of the Axes that inherit from `skyline.BaseGenomicAxes`
        """
        super().set_skyline_plot_area()
        self.set_ylim(0, 1)
        self.xaxis.set_visible(True)
        self.yaxis.set_visible(False)
        self.spines['right'].set_visible(False)
        self.spines['top'].set_visible(True)
        self.spines['left'].set_visible(False)
        self.spines['bottom'].set_visible(True)
        middle = self.secondary_xaxis(0.5)
        middle.xaxis.set_tick_params(top=False, bottom=False)
        middle.spines['bottom'].set_visible(False)
        middle.spines['top'].set_visible(False)
        middle.spines['left'].set_visible(False)
        middle.spines['right'].set_visible(False)

        for tick in middle.xaxis.get_major_ticks():
            tick.set_pad(-9)

        try:
            # We use the unit exponent to set a formatter function
            middle.xaxis.set_major_formatter(
                get_converter(exponent=self._sl_units[0])
            )
        except TypeError:
            # To handle earlier versions of matplotlib
            middle.xaxis.set_major_formatter(
                FuncFormatter(get_converter(exponent=self._sl_units[0]))
            )
        top = self.secondary_xaxis(1)
        top.xaxis.set_tick_params(direction='in')
        top.xaxis.set_ticklabels([])
        self.xaxis.set_ticklabels([])
        self.xaxis.set_tick_params(direction='in')

        if self._sl_plot_unit_text_label is True:
            self.xaxis.set_label_text(
                self._sl_units[1], **self._sl_offset_text_kwargs
            )
            self.xaxis.set_label_coords(
                *self._sl_offset_text_coords, transform=self.transAxes
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_converter(exponent=1E06):
    """Get a converter function with the exponent set correctly.

    Parameters
    ----------
    exponent : `int`, optional, defaut: `1E06`
        The exponent of the returned converter function.

    Returns
    -------
    converter_function : `func`
        This embeds the exponent as a closure.
    """
    def converter(x, pos):
        return x / exponent
    return converter


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def bp_converter(x):
    """Convert the base pairs to the minimal integer base pair position.

    Parameters
    ----------
    x : `int`
        The position to convert in base pairs

    Returns
    -------
    min_int_pos : `int`
        The minimal integer position.
    units : `str`
        The units of the minimal integer position i.e. kbp, Mbp
    """
    for i in ['bp', 'kbp', 'Mbp', 'Gbp']:
        test_pos = x / 1000
        if test_pos < 1:
            return x
        x = test_pos
    raise ValueError("are you sure these are base pairs?")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class LineLinkerTrack(axes.BaseGenomicAxes):
    """A track that links two coordinate locations and draws a vertical line
    between them.

    Parameters
    ----------
    figure : `matplotlib.Figure` or `skyline.GenomicFigure`
        The figure that the Axes will be associated with.
    track : `matplotlib.gridspec.SubplotSpec`
        The track grid coordinates that the Axes should be added to. These are
        obtained from a `matplotlib.GridSpec` subset.
    coords : `skyline.coords.ChrCoords`
        An object inheriting from `skyline.coords.ChrCoords`
    links : `list` of `skyline.tracks.linkers.LinkCoords`
        The positions of the features that will be linked.
    *args
        Arguments to `skyline.axes.BaseGenomicAxes` and `matplotlib.Axes`.
    from_calc : `str`, optional, default: `center`
        As this is a line linker this decides how to handle the span of the
        start/end coordinates of the from link, can either be from the start
        position (start), end position (end) of the center position (center)
        (the default).
    to_calc : `str`, optional, default: `center`
        As this is a line linker this decides how to handle the span of the
        start/end coordinates of the to link, can either be from the start
        position (start), end position (end) of the center position (center)
        (the default).
    linker_style : `dict`, optional, default: `NoneType``
        The default linker style if specific styles are not given with the
        ``link_coords``. `NoneType`, will use the class default style. User
        supplied `dict` are merged into (and override) default styles. Any
        arguments in the dict should be appropriate for
        ``matplotlib.Axes.axline``.
    **kwargs
        Keyword arguments to `skyline.axes.BaseGenomicAxes` and
        `matplotlib.Axes`.
    """
    _CENTER = 'center'
    """keyword for center position coords (`str`)
    """
    _START = 'start'
    """keyword for using start position coords (`str`)
    """
    _END = 'end'
    """keyword for using end position coords (`str`)
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, links, *args, from_calc='center',
                 to_calc='center', linker_style=None, **kwargs):
        if not all(isinstance(i, LinkCoords) for i in links):
            raise TypeError("links should be LinkerCoords objects")

        self._links = links
        self._sky_link_style = self.get_skyline_style_kwargs(
            linker_style, styles.LINE_LINK_STYLE.copy()
        )

        self._from_coord_calc = self._get_use_coords(from_calc)
        self._to_coord_calc = self._get_use_coords(to_calc)

        super().__init__(figure, track, coords, *args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def _get_use_coords(cls, coords_calc):
        """Determine the function to use to calculate the position of the link
        line.

        Parameters
        ----------
        coords_calc : `str`
            Should be either `start`, `end` or `center`.

        Raises
        ------
        ValueError
            if ``coords_calc``is not  `start`, `end` or `center`.
        """
        if coords_calc == cls._CENTER:
            return LinkCoords.center_coord
        elif coords_calc == cls._START:
            return LinkCoords.start_coord
        elif coords_calc == cls._END:
            return LinkCoords.end_coord
        else:
            raise ValueError(
                "unknown coordinate calculation value: '{0}'".format(
                    coords_calc
                )
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def plot_skyline(self, *args, **kwargs):
        """Add all the components to the Axes.

        Parameters
        ----------
        *args
            Ignored
        **kwargs
            Ignored
        """
        self._links.sort(key=lambda x: x.from_link.plot_start_pos)
        for i in self._links:
            # Use provided style to override defaults.
            style = {**self._sky_link_style, **i.style}
            self.axline(
                (self._from_coord_calc(
                    i.from_link.plot_start_pos, i.from_link.plot_end_pos
                ), 1),
                (self._from_coord_calc(
                    i.to_link.plot_start_pos, i.to_link.plot_end_pos
                ), 0),
                **style
            )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class _BaseBlockLinkerTrack(axes.BaseGenomicAxes):
    """The base linker that sets everything up.

    Parameters
    ----------
    figure : `matplotlib.Figure` or `skyline.GenomicFigure`
        The figure that the Axes will be associated with.
    track : `matplotlib.gridspec.SubplotSpec`
        The track grid coordinates that the Axes should be added to. These are
        obtained from a `matplotlib.GridSpec` subset.
    coords : `skyline.coords.ChrCoords`
        An object inheriting from `skyline.coords.ChrCoords`
    link_coords : `list` of `tuple` of (`skyline.features.FeatureCoords`, `skyline.features.FeatureCoords`)
        The coordinate regions of the span each tuple is
        (from coords, to coords). The features must inherit from
        ``skyline.features.FeatureCoords``. If any styling parameters are
        required for a span they must be given to the first feature in the
        tuple. If not the default styling parameters are used. Any styles given
        should be stored under the key ``BlockLinkerTrack.LINK_BLOCK_KEY`` and
        must have keyword arguments applicable for
        `matplotlib.patches.PathPatch`.
    *args
        Arguments to `skyline.axes.BaseGenomicAxes`.
    linker_style : `dict`, optional, default: `NoneType``
        The default linker style if specific styles are not given with the
        ``link_coords``. `NoneType`, will use the class default style.
    **kwargs
        Keyword arguments to `skyline.axes.BaseGenomicAxes` and
        `matplotlib.Axes`.
    """
    LINK_BLOCK_STYLE = axes.BaseGenomicAxes.STYLE_CLASS(
        facecolor='#DBDBDB40',
        edgecolor='#DBDBDB40',
        linewidth=0.5
    )
    """The default style for linker blocks. The ``dict`` keys should be
    appropriate for ``matplotlib.patches.PathPatch`` (`dict`).
    """
    LINK_BLOCK_KEY = 'LINK_BLOCK_STYLE'
    """The key name for default link block style in the main style dictionary.
    Do not change this value unless you know what you are doing (`str`)
    """
    STYLE = axes.BaseGenomicAxes.STYLE.copy()
    """All the default styles used by the
    ``skyline.tracks._BaseBlockLinkerTrack``. The keys should be
    ``LINK_BLOCK_STYLE``. Please remember this is a class
    variable and changes here will be used in all classes. If using as a
    template, to avoid this place make a deep copy (`dict`)
    """
    # Update the defaults for the main style dictionary
    STYLE[LINK_BLOCK_KEY] = LINK_BLOCK_STYLE

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, link_coords, *args,
                 linker_style=None, **kwargs):
        self.sl_link_coords = link_coords

        # Set the instance defaults for gene patch style and gene label style
        self.sl_linker_style = self.get_skyline_style_kwargs(
            linker_style, self.LINK_BLOCK_STYLE
        )

        super().__init__(
            figure, track, coords, *args, **kwargs
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_skyline_link_coords(self):
        """Get the linker coordinate positions.
        """
        return self.sl_link_coords

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def plot_skyline(self, *args, **kwargs):
        """Add artists and everything for the plot to the axes.

        Parameters
        ----------
        *args
            Any positional arguments
        **kwargs
            Any keyword arguments
        """
        bottom, top = 0, 1
        verts, codes = [], []

        for tc, bc in self.sl_link_coords:
            try:
                style = self.get_skyline_style_kwargs(
                    self.sl_linker_style, tc.styles[self.LINK_BLOCK_KEY]
                )
            except KeyError:
                style = self.sl_linker_style

            verts = [
                (bc.start_pos, bottom), (tc.start_pos, top),
                (tc.end_pos, top), (bc.end_pos, bottom), (0., 0.)
                ]
            codes = [
                Path.MOVETO, Path.LINETO, Path.LINETO, Path.LINETO,
                Path.CLOSEPOLY
            ]
            self.add_patch(patches.PathPatch(
                Path(verts, codes), **style
            ))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class BlockLinkerTrack(_BaseBlockLinkerTrack):
    """A linker that links spans of coordinates to other spans of coordinates.

    Parameters
    ----------
    figure : `matplotlib.Figure` or `skyline.GenomicFigure`
        The figure that the Axes will be associated with.
    track : `matplotlib.gridspec.SubplotSpec`
        The track grid coordinates that the Axes should be added to. These are
        obtained from a `matplotlib.GridSpec` subset.
    coords : `skyline.coords.ChrCoords`
        An object inheriting from `skyline.coords.ChrCoords`
    link_coords : `list` of `tuple` of (`skyline.features.FeatureCoords`, `skyline.features.FeatureCoords`)
        The coordinate regions of the span each tuple is
        (from coords, to coords). The features must inherit from
        ``skyline.features.FeatureCoords``. If any styling parameters are
        required for a span they must be given to the first feature in the
        tuple. If not the default styling parameters are used. Any styles given
        should be stored under the key ``BlockLinkerTrack.LINK_BLOCK_KEY`` and
        must have keyword arguments applicable for
        `matplotlib.patches.PathPatch`.
    *args
        Arguments to `skyline.axes.BaseGenomicAxes`.
    linker_style : `dict`, optional, default: `NoneType``
        The default linker style if specific styles are not given with the
        ``link_coords``. ``NoneType``, will use the class default style.
    **kwargs
        Keyword arguments to `skyline.axes.BaseGenomicAxes` and
        `matplotlib.Axes`.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, link_coords, *args,
                 linker_style=None, **kwargs):
        super().__init__(figure, track, coords, *args, **kwargs)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PosToUniformLinkerTrack(_BaseBlockLinkerTrack):
    """Link genomic coordinates to uniformaly spaced coordinates based either
    on the start position, centre position or the end position.

    Parameters
    ----------
    figure : `matplotlib.Figure` or `skyline.GenomicFigure`
        The figure that the Axes will be associated with.
    track : `matplotlib.gridspec.SubplotSpec`
        The track grid coordinates that the Axes should be added to. These are
        obtained from a `matplotlib.GridSpec` subset.
    coords : `skyline.coords.ChrCoords`
        An object inheriting from `skyline.coords.ChrCoords`
    link_coords : `list` of `skyline.features.FeatureCoords`
        The positions of the features that will be linked to a uniform
        representation.  If any styling parameters are required for a link
        they must be be stored under the key
        ``BlockLinkerTrack.LINK_BLOCK_KEY`` and have keyword arguments
        applicable for `matplotlib.patches.PathPatch`.
    *args
        Arguments to `skyline.axes.BaseGenomicAxes`.
    direction : `str`, optional, default: `down`
        The direction of the link. Should be either ``down`` - linking from the
        feature to the uniform position or ``up`` - linking from the uniform
        position to the feature.
    uniform_order : `str`, optional, default: `center`
        Control the ordering of the uniform links. They can be either,
        ``center`` - the features with the lowest center position will be to
        the left, ``start`` - the features with the lowest start position will
        be to the left or ``end`` - the features with the lowest end position
        will be to the left.
    linker_style : `dict`, optional, default: `NoneType``
        The default linker style if specific styles are not given with the
        ``link_coords``. `NoneType`, will use the class default style.
    **kwargs
        Keyword arguments to `matplotlib.Axes`.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, link_coords, *args,
                 direction='down', uniform_order='center', linker_style=None,
                 **kwargs):
        self._sl_direction = direction
        self._sl_uniform_order = uniform_order
        self._sl_links = link_coords

        link_coords = list(zip(*self.get_uniform_features(coords)))
        # pp.pprint(link_coords)
        if self._sl_direction == 'up':
            link_coords = [(j, i) for i, j in link_coords]
        elif self._sl_direction != 'down':
            raise ValueError(f"unknown direction: {self._direction}")

        super().__init__(
            figure, track, coords, link_coords, *args,
            linker_style=linker_style, **kwargs
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_uniform_features(self, coords):
        """Get uniform features that represent the feature coordinates
        """
        genome_feat = self._sl_links
        if self._sl_uniform_order == 'start':
            genome_feat = sorted(genome_feat, key=lambda x: x.start_pos)
        elif self._sl_uniform_order == 'end':
            genome_feat = sorted(genome_feat, key=lambda x: x.end_pos)
        elif self._sl_uniform_order == 'center':
            genome_feat = sorted(genome_feat, key=lambda x: x.center_pos)
        else:
            ValueError(f"unknown uniform_order: {self._sl_uniform_order}")

        uniform_features = []

        xlim_start = coords.get_coords(coords.chr_name, coords.start)
        xlim_end = coords.get_coords(coords.chr_name, coords.end)
        xlim_range = xlim_end - xlim_start
        grid_unit = xlim_range/(len(genome_feat) + 1)
        prev_start = xlim_start
        for idx, i in enumerate(genome_feat):
            cur_start = prev_start + grid_unit
            uniform_features.append(
                features.FeatureCoords(
                    i.chr_name, cur_start, end_pos=cur_start,
                    strand=i.strand, coords_mapper=i.coords_mapper,
                    styles=i.styles
                )
            )
            prev_start = cur_start
        return genome_feat, uniform_features

    # # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # def check_skyline_link_coords(self):
    #     """Process and error check the coordinates. This hijacks the base class
    #     version as the uniform linker only has one set of coordinates given to
    #     it.
    #     """
    #     if self._uniform_order == 'start':
    #         self.sl_link_coords.sort(key=lambda x: x[0])
    #     elif self._uniform_order == 'end':
    #         self.sl_link_coords.sort(key=lambda x: x[1])
    #     elif self._uniform_order == 'center':
    #         self.sl_link_coords.sort(
    #             key=lambda x: x[0] + ((x[1] - x[0]) / 2)
    #         )
    #     else:
    #         ValueError(
    #             "unknown uniform_order: {0}".format(self._uniform_order)
    #         )

    #     processed_coords = []

    #     xlim_start, xlim_end = self.get_xlim()
    #     xlim_range = xlim_end - xlim_start
    #     grid_unit = xlim_range/(len(self.sl_link_coords) + 1)
    #     prev_start = xlim_start
    #     for idx, i in enumerate(self.sl_link_coords):
    #         try:
    #             start, end, name, style = i
    #         except ValueError:
    #             style = self.sl_linker_style
    #             try:
    #                 start, end, name = i
    #             except ValueError:
    #                 start, end = i
    #                 name = idx
    #         cur_start = prev_start + grid_unit
    #         processed_coords.append(
    #             ((start, end), (cur_start, cur_start), name, style)
    #         )
    #         prev_start = cur_start

    #     if self._direction == 'up':
    #         processed_coords = [
    #             (i[1], i[0], i[2], i[3]) for i in processed_coords
    #         ]
    #     elif self._direction != 'down':
    #         raise ValueError(
    #             "unknown direction: {0}".format(self._direction)
    #         )

    #     # Update the processed coordinates
    #     self.sl_link_coords = processed_coords
