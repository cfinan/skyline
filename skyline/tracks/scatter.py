"""Scatter plot Axes
"""
from skyline import axes, constants as con, utils, styles
from matplotlib.patches import Patch
from matplotlib.ticker import FuncFormatter
from matplotlib import text
import numpy as np
import pandas as pd
import itertools
# import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class ChrScatter(axes.BaseGenomicAxes):
    """A scatter has a y-axis that is sub-chromosome based (but does not have
    to be)

    Parameters
    ----------
    figure : `skyline.GenomicFigure`
        The figure on which to apply the plot
    tracks : `matplotlib.GridSpec`
        A gridspec object to arrange the tracks
    coords : `skyline.coords.ChrCoords`, optional, default: `NoneType`
        A coordinates object to use in the plot region. If not supplied then
        one is created from the lead variant position and the ``flank`` length.
    data : `pandas.DataFrame`
        A region slice of GWAS data must have chr_name, start_pos,
        effect_allele, other_allele and p-value columns.
    value_column : `str`
        The column name of the pvalue column in the ``data``.
    *args
        Any arguments to be passed to `skyline.axes.BaseGenomicAxes`
    coord_units : `tuple`, optional, default: `(1E6, 'Mbp')`
        The units for the coordinates. This should be an exponent at ``[0]``
        i.e. 1E06 to convert the base pair coordinates to mega base pairs,
        and the unit string at ``[1]`` i.e. Mbp. The unit string will be plotted
        inline with the x-axis as the default (see ``offset_text_coords``)
    unit_text_coords : `tuple`, optional, default: `(0, -0.06)`
        The x,y placement coordinates of the unit string in ``coord_units``.
        The default will place it in the bottom left of the plot. These are not
        `matplotlib.axis.offsetText` but rather it is the positions of the
        xaxis label operating as offset text.
    unit_text_style : `dict` or `NoneType`, optional, default: `NoneType`
        Keyword arguments to `matplotlib.text.Text`, that are used to format
        ``coord_units[1]``. If ``NoneType`` then this defaults to:
        ``skyline.scatter.ChrScatter.DEFAULT_OFFSET_TEXT_STYLE()``
    **kwargs
        Any keyword arguments to be passed to `skyline.axes.BaseGenomicAxes`
    """
    MIN_YLIM = 0
    UNIT_TEXT_COORDS = (0, -0.06)

    OFFSET_TEXT_STYLE = axes.BaseGenomicAxes.STYLE_CLASS(
            ha='right', va='top', fontsize=5, fontweight='bold'
        )
    """The style for the units offset text used on the x-axis. These are
    keyword arguments to `Axes.xaxis.set_label_text` (`dict`).
    """
    OFFSET_TEXT_KEY = "OFFSET_TEXT"
    STYLE = axes.BaseGenomicAxes.STYLE.copy()
    STYLE[OFFSET_TEXT_KEY] = OFFSET_TEXT_STYLE

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, data, value_column, *args,
                 coord_units=(1E6, 'Mbp'), plot_unit_text_label=True,
                 unit_text_coords=UNIT_TEXT_COORDS, unit_text_style=None,
                 **kwargs):
        # Set the ylimits according to the data (if needed)
        kwargs = axes.BaseGenomicAxes.set_skyline_kwargs_ylim(
            kwargs, data, value_column, min_value=self.__class__.MIN_YLIM,
            max_buffer=0.1
        )

        # Store and process the data
        self._skyline_data = data
        self._skyline_value_column = value_column
        self._skyline_units = coord_units
        self._skyline_offset_text_coords = unit_text_coords
        self._skyline_offset_text_kwargs = self.get_skyline_style_kwargs(
            unit_text_style, self.OFFSET_TEXT_STYLE
        )
        self._sl_plot_unit_text_label = plot_unit_text_label

        super().__init__(figure, track, coords, *args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_skyline_plot_area(self):
        """Setup the skyline Axes plotting area. This sets attributes that are
        common to many of the Axes that inherit from `skyline.BaseGenomicAxes`
        """
        super().set_skyline_plot_area()

        # Turn on the y-axis
        self.yaxis.set_visible(True)
        self.spines['left'].set_visible(True)
        self.xaxis.set_visible(True)
        self.spines['bottom'].set_visible(True)
        self.xaxis.get_offset_text().set_visible(False)

        try:
            # We use the unit exponent to set a formatter function
            self.xaxis.set_major_formatter(
                get_converter(exponent=self._skyline_units[0])
            )
        except TypeError:
            # To handle earlier versions of matplotlib
            self.xaxis.set_major_formatter(
                FuncFormatter(get_converter(exponent=self._skyline_units[0]))
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def process_skyline_data(self):
        """Pre-process any supplied data pre-processing steps.

        Notes
        -----
        This is called upon initialisation of the object and performs sorting
        of the data on value column. This ensures that the highest value points
        are plotted last.
        """
        self._skyline_data.sort_values(
            self._skyline_value_column, inplace=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def plot_skyline(self, *args, **kwargs):
        """Perform the skyline plot. Only call this if object was initialised
        with ``do_plot=False``.

        Parameters
        ----------
        *args
            Positional arguments (ignored)
        **kwargs
            Keyword arguments (ignored)
        """
        self.sl_scatter = []
        for i in self._skyline_coords.get_active_chr_name():
            x = self.get_coords_scatter_x(i)
            y = self.get_coords_scatter_y(i)
            kwargs = self.get_scatter_kwargs(i)
            self.sl_scatter.append(self.scatter(x, y, **kwargs))

        if self._sl_plot_unit_text_label is True:
            self.xaxis.set_label_text(
                self._skyline_units[1], **self._skyline_offset_text_kwargs
            )
            self.xaxis.set_label_coords(
                *self._skyline_offset_text_coords, transform=self.transAxes
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_coords_scatter_x(self, chr_name):
        """Get all the data x coordinates for the genomic scatter plot.

        Parameters
        ----------
        chr_name : `str`
            The chromosome to get the x coordinates for.

        Returns
        -------
        x_coords : `pandas.Series`
            The x-coordinates adjusted by the coordinates object.
        """
        # The x coordinates are adjusted for plotting x coordinates
        # determined by the coordinates object
        return (self._skyline_data.loc[
            self._skyline_data[con.CHR_NAME] == chr_name, con.START_POS
        ] + self._skyline_coords.get_coords(chr_name, 1))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_coords_scatter_y(self, chr_name):
        """Get all the data y coordinates for the genomic scatter plot.

        Parameters
        ----------
        chr_name : `str`
            The chromosome to get the x coordinates for.

        Returns
        -------
        y_coords : `pandas.Series`
            The y-coordinates adjusted by the coordinates object.
        """
        return (self._skyline_data.loc[
            self._skyline_data[con.CHR_NAME] == chr_name,
            self._skyline_value_column
        ])

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_scatter_kwargs(self, chr_name):
        """Get the kwargs for `matplotlib.Axes.scatter` based on the style that
        has been given to the `skyline.tracks.GenomicScatter` object.

        Parameters
        ----------
        chr_name : `str`
            The chromosome to get the scatter keyword arguments for.
        """
        return dict()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class LocusViewScatter(ChrScatter):
    """A scatter is designed to have a y-axis that is sub-chromosome based (but
    does not have to be) and plots LD between a lead variant and is neighbours.

    Parameters
    ----------
    figure : `skyline.GenomicFigure`
        The figure on which to apply the plot
    tracks : `matplotlib.SubPlotSpec`
        The subplot to place the scatter plot.
    coords : `skyline.coords.ChrCoords`
        A coordinates object to use in the plot region.
    data : `pandas.DataFrame`
        A region slice of GWAS data must have ``chr_name``, ``start_pos``,
        ``effect_allele``, ``other_allele`` and ``pvalue`` columns.
    value_column : `str`
        The column name of the ``pvalue`` column in the ``data``.
    lead_variant : `skyline.features.Variant`
        The variant feature object representing the lead variant.
    ld_data : `list` of `tuple` or `pandas.DataFrame`
        LD data to use to colour the points in the plot. If an emply list then
        all the points will be coloured with "no LD". If it is a `list` of
        `tuple` then each tuple is a pair of variants with an r2 measure
        (float). Each variant in the pair is a tuple with: (chr_name, start_pos
        (<ALLELES>), variation ID). If it is a `pandas.DataFrame`, then it
        should be a square matrix with both rows and columns being ``uni_ids``
        - that is ``<chr_name>_<start_pos>_<allele1>_<allele2>`` where
        ``allele1`` and ``allele2`` are the ref/alt in sort order.
    *args
        Any arguments to be passed to `skyline.tracks.scatter.ChrScatter`.
    ld_breaks : `list` of `tuple`, optional, default: `NoneType`
        The LD break points and their plot colours. Each tuple should have the
        break point (r2 between 0-1 (float)) at ``[0]`` and the colour at
        ``[1]``. Note that the upper limit of 1.0 should not be included in
        this but the lower limit of 0 can be. If ``NoneType``, then the
        defaults in ``skyline.tracks.scatter.LocusViewScatter.LD_BREAKS`` are
        used.
    marker_style : `dict` or `NoneType`, optional, default: `NoneType`
        The default style parameters for the plotting marker on the scatter
        plot. `NoneType` defaults to the class defaults. The default colour
        (for variants with no LD) The ``c`` attribute should be used to define
        the default colour and not the ``color`` attribute. Note that, the
        colour map for the LD values is handled internally.
    lead_variant_style : `dict`, optional, default: `NoneType`
        The default style parameters for the lead variant plotting marker on
        the scatter plot. `NoneType` defaults to the
        ``skyline.tracks.scatter.LocusViewScatter.LEAD_VARIANT_STYLE``.
    legend_patch : `dict`, optional, default: `NoneType`
        Keyword arguments to the rectangle patches used in the legend. These
        should be general arguments used in all the patches. If you want fine
        grained control of the legend you should set ``plot_legend=False`` and
        handle the call to ``skyline.tracks.scatter.LocusViewScatter.legend``.
        ``NoneType`` uses the class defaults:
        ``skyline.tracks.scatter.LocusViewScatter.LEGEND_PATCH_STYLE``.
    plot_legend : `bool`, optional, default: `False`
        Should the legend be added to the plot. Turn off if you want the handle
        the legend manually.
    plot_lead_variant_label : `bool`, optional, default: `True`
        Should the lead variant label be plotted.
    lead_variant_label_style : `dict`, optional, default: `NoneType`
        The styling for the lead variant label. ``NoneType`` will use the
        defaults:
        ``skyline.tracks.scatter.LocusViewScatter.LEAD_VARIANT_LABEL_STYLE``
    legend_kwargs : `dict`, optional, default: `NoneType`
        Arguments to the `skyline.tracks.scatter.LocusViewScatter.legend` call.
        It is possible to completely override the default arguments with this,
        including the legend handles. If it is ``NoneType``, then this will
        default to: ``skyline.tracks.scatter.LocusViewScatter.LEGEND_ARGS``.
    phenotype : `str`, optional, default: `NoneType`
        The phenotype label to plot alongside the lead variant label.
        ``NoneType`` is the same as an empty string.
    **kwargs
        Any keyword arguments to be passed to
        `skyline.tracks.scatter.ChrScatter`.
    """
    MIN_YLIM = 0
    """The default minimal ylimit (`float`)
    """
    LEAD_LD_COL = 'lead_ld'
    """
    """
    _NO_LD_VALUE = -1
    """A lower bound for the LD break point CMAP (`int`)
    """
    MLOG_PVALUE_TEXT = r'$-\log_{{10}}(P)$'
    """The text for the negative log10 p-value
    """
    MARKER_STYLE = ChrScatter.STYLE_CLASS(
        c='#DBDBDB',
        edgecolor='black',
        linewidth=0.5,
        s=20,
        alpha=0.6
    )
    """The default marker style for the class. These are keyword arguments to
    `Axes.scatter`. The default (NO LD) style should be set as ``c`` and not
    ``color``. The ``cmap`` should not be set as it is handled internally.
    """
    LEAD_VARIANT_STYLE = ChrScatter.STYLE_CLASS(
        # color='purple',
        c='purple',
        edgecolor='black',
        linewidth=0.5,
        s=20
    )
    """The default marker style for the lead variant. These are keyword
    arguments to `Axes.scatter`.
    """
    LEAD_VARIANT_LABEL_STYLE = ChrScatter.STYLE_CLASS(
        x=0, y=1, allele_trunc=5, ha='left', va='bottom', fontsize=6,
        fontweight='bold', clip_on=False
    )
    """The default arguments for the lead variant label. These are keyword
    arguments to `matplotlib.text.Text`. In addition an additional key can be
    given (``allele_trunc``) that determines the length of the alleles to place
    in the label. If they are greater then this value then they are truncates
    with an elipsis ... placed instead. The default value for this is 5. By
    default the lead variant label is set on the top left of the plot. This can
    be altered with x/y however, be aware that by default, this label is
    plotted on Axes coordinates and not data coordinates. The text of the label
    can also be altered away from the defaults.
    """
    LEGEND_PATCH_STYLE = ChrScatter.STYLE_CLASS(
        edgecolor='black',
        linewidth=0.5,
        alpha=0.6
    )
    """The patch style for elements that are common in the legend such as
    edgecolor and linewidth
    """
    LEGEND_ARGS = ChrScatter.STYLE_CLASS(
        title="$r^2$",
        fontsize="xx-small",
        title_fontsize="small",
        frameon=False
    )
    """The default arguments to the call `LocusViewScatter.legend`. These are
    keyword arguments to `Axes.legend`.
    """
    LD_BREAKS = [
        (0.8, '#FF0604'), (0.6, '#FDA502'), (0.4, '#02FF03'),
        (0.2, '#84CCF7'), (0, '#00007E')
    ]
    """The default LD break points used for the class
    """

    MARKER_STYLE_KEY = 'MARKER_STYLE'
    """The name of the marker style key in the default ``STYLES`` class
    attribute (`str`)
    """
    LEAD_VARIANT_STYLE_KEY = 'LEAD_VARIANT_STYLE'
    """The name of the lead variant style key in the default ``STYLES`` class
    attribute (`str`)
    """
    LEAD_VARIANT_LABEL_STYLE_KEY = 'LEAD_VARIANT_LABEL_STYLE'
    """The name of the lead variant label style key in the default
    ``STYLES`` class attribute (`str`)
    """
    LEGEND_PATCH_STYLE_KEY = 'LEGEND_PATCH'
    """The name of the legend patch style key in the default ``STYLES`` class
    attribute (`str`)
    """
    LEGEND_ARGS_KEY = 'LEGEND_ARGS'
    """The name of the legend argument key in the default ``STYLES`` class
    attribute (`str`)
    """
    LD_BREAKS_KEY = 'LD_BREAKS'
    """The name of the LD breaks key in the default ``STYLES`` class
    attribute (`str`)
    """
    STYLE = ChrScatter.STYLE.copy()
    """All of the default styles for the ``LocusViewScatter``.
    """
    STYLE[MARKER_STYLE_KEY] = MARKER_STYLE
    STYLE[LEAD_VARIANT_STYLE_KEY] = LEAD_VARIANT_STYLE
    STYLE[LEAD_VARIANT_LABEL_STYLE_KEY] = LEAD_VARIANT_LABEL_STYLE
    STYLE[LEGEND_PATCH_STYLE_KEY] = LEGEND_PATCH_STYLE
    STYLE[LEGEND_ARGS_KEY] = LEGEND_ARGS
    STYLE[LD_BREAKS_KEY] = LD_BREAKS

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, data, value_column, lead_variant,
                 ld_matrix, *args, ld_breaks=None, marker_style=None,
                 lead_variant_style=None, legend_patch=None, plot_legend=True,
                 plot_lead_variant_label=True, lead_variant_label_style=None,
                 legend_kwargs=None, phenotype=None, **kwargs):
        # TODO: This is a stop gap until I can change internally, I will
        # extract the VariantFeature info into a tuple
        self._sl_lead_variant = (
            lead_variant.chr_name, lead_variant.start_pos,
            lead_variant.ref_allele, lead_variant.alt_allele,
            lead_variant.label
        )
        self._sl_lead_variant_uni_id = lead_variant.uni_id

        # Here we are merging any user defined styles with the defaults
        self._sl_ld_breaks = ld_breaks or self.LD_BREAKS.copy()
        self._sl_marker_style = self.get_skyline_style_kwargs(
            marker_style, self.MARKER_STYLE
        )
        self._sl_lead_variant_style = self.get_skyline_style_kwargs(
            lead_variant_style, self.LEAD_VARIANT_STYLE
        )
        self._sl_legend_patch = self.get_skyline_style_kwargs(
            legend_patch, self.LEGEND_PATCH_STYLE
        )
        self._sl_legend_args = self.get_skyline_style_kwargs(
            legend_kwargs, self.LEGEND_ARGS
        )
        self._sl_lead_variant_label_style = self.get_skyline_style_kwargs(
            lead_variant_label_style, self.LEAD_VARIANT_LABEL_STYLE
        )

        # Store the no LD value before we process the cmap
        self._sl_no_ld_color = self._sl_marker_style['c']

        # Store booleans for switching off plotting of some labels
        self._sl_plot_legend = plot_legend
        self._sl_plot_lead_label = plot_lead_variant_label
        self._sl_phenotype = phenotype

        # This adds the colours for the significant hits
        utils.add_listed_cmap(
            self._sl_marker_style, self._NO_LD_VALUE, 1, self._sl_ld_breaks
        )

        # Format the LD data for merging into that region GWAS data
        ld_values = self._process_ld_data(
            self._sl_lead_variant, ld_matrix
        )

        # left join is used to keep, those variants with no LD data
        data = pd.merge(
            data, ld_values,
            how='left',
            left_index=True,
            right_index=True
        )

        # Now set any values that do not have any LD data to the no LD value
        # (-1)
        missing_ld = pd.isnull(data[self.LEAD_LD_COL])
        data.loc[missing_ld, self.LEAD_LD_COL] = self._NO_LD_VALUE

        super().__init__(figure, track, coords, data, value_column,
                         *args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def make_uni_id(chr_name, start_pos, *alleles):
        """Create a universal ID uni_id for a variant.

        Parameters
        ----------
        chr_name : `str`
            The chomosome name
        start_pos : `int`
            The start position
        *alleles
            The alleles for the variant

        Returns
        -------
        uni_id : `str`
            The universal identifier for the variant. That is:
            <chr_name>_<start_pos>_<allele1>_<allele2> where allele1 and
            allele2 are the ref/alt in sort order.
        """
        return "{0}_{1}_{2}_{3}".format(
            chr_name, start_pos, *(sorted(alleles))
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def _process_ld_data(cls, lead_variant, ld_values):
        """Process the LD values into a standard format.

        Parameters
        ----------
        lead_variant : `tuple`
            The lead variant for the plot. The tuple should have the following
            structure. ``[0]`` chr_name, ``[1]`` start position ``[2]``
            reference allele ``[3]`` alternate allele ``[4]`` (optional)
            variant ID.
        ld_values : `list` of `tuple` or `pandas.DataFrame`
            If it is a `list` of `tuple` then each tuple is a pair of variants
            with an r2 measure (float). Each variant in the pair is a tuple
            with: (chr_name, start_pos (<ALLELES>), variation ID). If it is a
            `pandas.DataFrame`, then it should be a square matrix with both
            rows and columns being ``uni_ids`` - that is
            <chr_name>_<start_pos>_<allele1>_<allele2> where allele1 and
            allele2 are the ref/alt in sort order.

        Returns
        -------
        ld_values : `pandas.Series`
            The LD values (r2 floats) with the indexes being ``uni_ids``

        Raises
        ------
        TypeError
            If ``ld_values`` is not one of the recognised formats.

        Notes
        -----
        The LD values can either be supplied as a square `pandas.DataFrame`
        with ``uni_id`` indexes and columns. Or a `list` of `tuples` with each
        tuple being a pair of variants with an r2 measure (float). Each variant
        in the pair is a tuple with: (chr_name, start_pos (<ALLELES>),
        variation ID)
        """
        lead_uni_id = cls.make_uni_id(
            lead_variant[0], lead_variant[1], lead_variant[2], lead_variant[3]
        )
        # If it is a data frame that is square then we assume it is an LD
        # matrix
        if isinstance(ld_values, pd.DataFrame) and \
           ld_values.shape[0] == ld_values.shape[1]:
            ld_values = ld_values[lead_uni_id]
            ld_values.name = 'lead_ld'
            return ld_values

        # Otherwise we check it is a list of pairwise LD with the lead variant
        # and convert to a series
        if isinstance(ld_values, list):
            uni_ids = []
            rsquare = []
            for source, ld, rsq in ld_values:
                for i in itertools.combinations(ld[2], 2):
                    uni_ids.append(cls.make_uni_id(ld[0], ld[1], *i))
                    rsquare.append(rsq)
            ld_values = pd.DataFrame(
                dict(uni_id=uni_ids, lead_ld=rsquare)
            ).set_index('uni_id')
            # ld_values.name = 'lead_ld'
            return ld_values

        raise TypeError(
            "LD values should be a square data frame or list of tuples"
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_skyline_plot_area(self):
        """Setup the skyline Axes plotting area. This sets attributes that are
        common to many of the Axes that inherit from `skyline.BaseGenomicAxes`
        """
        super().set_skyline_plot_area()
        self.yaxis.set_label_text(self.MLOG_PVALUE_TEXT)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def plot_skyline(self, *args, **kwargs):
        """Initialise the plot, i.e. add all the artists and labels.
        """
        # This will handle the general Axes.scatter calls
        super().plot_skyline(*args, **kwargs)

        # Now overplot the lead variant
        # TODO: Change this so lead variant is removed and plotted once
        x = self._skyline_data.loc[self._sl_lead_variant_uni_id, con.START_POS]
        y = self._skyline_data.loc[
            self._sl_lead_variant_uni_id, self._skyline_value_column
        ]
        self.sl_scatter.append(self.scatter(
            x, y, **self._sl_lead_variant_style
        ))

        if self._sl_plot_legend is True:
            self.set_skyline_legend()

        if self._sl_plot_lead_label is True:
            self.set_lead_variant_label()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_skyline_legend(self):
        """Get the keyword parameters that are needed to pass to a legend call.
        """
        try:
            self._sl_legend_args['handles']
        except KeyError:
            # The user has not supplied any handles so we set up our own
            legend_handles = []
            for i in self._sl_ld_breaks:
                patch = self._sl_legend_patch.copy()
                patch['facecolor'] = i[1]
                patch['label'] = '$\geq {0}$'.format(i[0])
                legend_handles.append(Patch(**patch))

            lead_variant_patch = {
                **self._sl_legend_patch, **dict(label='lead variant')
            }

            for i in ['c', 'color']:
                try:
                    lead_variant_patch['facecolor'] = \
                        self._sl_lead_variant_style[i]
                except KeyError:
                    pass

            legend_handles.extend([
                Patch(
                    **{
                        **self._sl_legend_patch,
                        **dict(
                            facecolor=self._sl_no_ld_color,
                            label='no LD'
                        )
                    }
                ),
                Patch(**lead_variant_patch)
            ])
            self._sl_legend_args['handles'] = legend_handles
        return self._sl_legend_args

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_skyline_legend(self):
        """Set up the legend for the plot. This is called from ``plot_skyline``
        so does not have to be called by the user. Here mainly for overriding.
        """
        self.legend(**self.get_skyline_legend())

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_lead_variant_label(self):
        """Setup and add a lead variant label to the plot.

        Notes
        -----
        by default this will be of the format:
        <PHENOTYPE>: <CHR_NAME>:<START_POS> <EFFECT_ALLELE>/<OTHER_ALLELE>

        If the phenotype is ``NoneType``, then it will be:
        <CHR_NAME>:<START_POS> <EFFECT_ALLELE>/<OTHER_ALLELE>
        """
        effect_allele = self._sl_lead_variant[2]
        other_allele = self._sl_lead_variant[3]

        # Get any allele truncation parameters if not default to five
        allele_trunc = self._sl_lead_variant_label_style.pop(
            'allele_trunc', 5
        )
        if len(effect_allele) > allele_trunc:
            effect_allele = effect_allele[:3] + '...'
        if len(other_allele) > allele_trunc:
            other_allele = other_allele[:3] + '...'

        lead_variant_text = ""
        if self._sl_phenotype is not None:
            lead_variant_text = "{0}: ".format(self._sl_phenotype)

        lead_variant_text = "{4}{0}:{1} {2}/{3}".format(
            self._sl_lead_variant[0], self._sl_lead_variant[1],
            effect_allele, other_allele, lead_variant_text
        )

        # Add some of the object level defaults unless they have already been
        # defined
        if 'transform' not in self._sl_lead_variant_label_style:
            self._sl_lead_variant_label_style['transform'] = self.transAxes
        if 'text' not in self._sl_lead_variant_label_style:
            self._sl_lead_variant_label_style['text'] = lead_variant_text

        t = text.Text(**self._sl_lead_variant_label_style)

        # self.set_clip_on(False)
        self.add_artist(t)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_scatter_kwargs(self, chr_name):
        """Get the kwargs for `matplotlib.Axes.scatter` based on the style that
        has been given to the `skyline.tracks.GenomicScatter` object.

        Parameters
        ----------
        chr_name : `str`
            The chromosome to get the scatter keyword arguments for.
        """
        # nstyles = len(chromosome_style)
        style_obj = self._sl_marker_style

        if not isinstance(style_obj, dict):
            style_obj = style_obj.get_scatter_kwargs()

        if style_obj['cmap'] is not None:
            # print(self._skyline_data)
            style_obj['c'] = self._skyline_data.loc[
                self._skyline_data[con.CHR_NAME] == chr_name,
                'lead_ld'
            ].copy()
        return style_obj


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class GenomicScatter(axes.BaseGenomicAxes):
    """A genomic scatter has a x-axis that is usually chromosome based.

    Parameters
    ----------
    figure : `skyline.GenomicFigure`
        The figure on which to apply the ``GenomicScatter`` axes.
    track : `matplotlib.gridspec.SubplotSpec`
        The track to add the scatter plot to.
    coords : `skyline.coords.GenomewideCoords`
        A coordinates object to use in the plot region. This handles the
        transformation between plot chr:pos coordinates and genomic plotting
        coordinates.
    data : `pandas.DataFrame`
        GWAS data must have chromosome name, start position and -log10 p-value
        columns.
    value_column : `str`
        The name of the column in ``data`` with the -log10 transformed pvalue
        data in it.
    *args
        Any arguments passed through to the parent class.
    chromosome_style : `NoneType` `list` of `str`, optional, default: `NoneType`
        Colours of each chromosome used in the scatter plot. They should be in
        some format that is recognised by `matplotlib`. It the length of the
        list is less than the number of chromosomes that are being plotted than
        the colours are recycled. If it is `NoneType`, then the default
        chromosome styles are used.
    sig_limit_color_cut : `NoneType` or `list` of `tuple`, optional, default: `NoneType`
        The breakpoints of the listed color map that will define -log10(pvalue)
        cutoff values, above which the points will be coloured differently to
        the ``chromosome_style``. Each tuple should have:
        ``(<cutoff_data_value>, <color>)``. Setting to ``NoneType``, will turn
        off significance colours.
    **kwargs
        Keyword arguments that are passed through to the parent class.
    """
    MIN_YLIM = 0
    """The minimal y-limit value for the genomic scatter plot (`int`)
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, data, value_column, *args,
                 chromosome_style=None, sig_limit_color_cut=None,
                 **kwargs):
        # Set the ylimits according to the data (if needed)
        kwargs = axes.BaseGenomicAxes.set_skyline_kwargs_ylim(
            kwargs, data, value_column, min_value=self.__class__.MIN_YLIM
        )
        # Make sure the chromosome colors is a list
        if chromosome_style is not None and not \
           isinstance(chromosome_style, (list, tuple)):
            raise TypeError("chromosome colors should be a list-like object")
        self._chromosome_style = chromosome_style or \
            [i.copy() for i in [styles.MANHATTAN_ODD_CHR, styles.MANHATTAN_EVEN_CHR]]
        if sig_limit_color_cut is not None:
            max_data = max(data[value_column])
            self._chromosome_style = [
                utils.add_listed_cmap(
                    i, 0, max_data, sig_limit_color_cut
                ) for i in self._chromosome_style
            ]

        # Store and process the data
        self._skyline_data = data
        self._skyline_value_column = value_column
        super().__init__(figure, track, coords, *args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_skyline_plot_area(self):
        """Setup the skyline Axes plotting area. This sets attributes that are
        common to many of the Axes that inherit from `skyline.BaseGenomicAxes`
        """
        super().set_skyline_plot_area()
        # Turn on the y-axis
        self.yaxis.set_visible(True)
        self.spines['left'].set_visible(True)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def process_skyline_data(self):
        """Pre-process any supplied data pre-processing steps.

        Notes
        -----
        This is called upon initialisation of the object and performs sorting
        of the data on value column. This ensures that the highest value points
        are plotted last.
        """
        self._skyline_data.sort_values(
            self._skyline_value_column, inplace=True
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def plot_skyline(self, *args, **kwargs):
        """Initialise the plot, i.e. add all the artists and labels.
        """
        for i in self._skyline_coords.get_active_chr_name():
            x = self.get_genomic_scatter_x(i)
            y = self.get_genomic_scatter_y(i)
            kwargs = self.get_genomic_scatter_kwargs(i)
            # pp.pprint(kwargs)
            self.scatter(x, y, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_genomic_scatter_x(self, chr_name):
        """Get all the data x coordinates for the genomic scatter plot.

        Parameters
        ----------
        chr_name : `str`
            The chromosome to get the x coordinates for.

        Returns
        -------
        x_coords : `pandas.Series`
            The x-coordinates adjusted by the coordinates object.
        """
        # The x coordinates are adjusted for plotting x coordinates
        # determined by the coordinates object
        return self._skyline_data.loc[
            self._skyline_data[con.CHR_NAME] == chr_name, con.START_POS
        ] + self._skyline_coords.get_coords(chr_name, 1)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_genomic_scatter_y(self, chr_name):
        """Get all the data y coordinates for the genomic scatter plot.

        Parameters
        ----------
        chr_name : `str`
            The chromosome to get the x coordinates for.

        Returns
        -------
        y_coords : `pandas.Series`
            The y-coordinates adjusted by the coordinates object.
        """
        return self._skyline_data.loc[
            self._skyline_data[con.CHR_NAME] == chr_name,
            self._skyline_value_column
        ]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_genomic_scatter_kwargs(self, chr_name):
        """Get the kwargs for `matplotlib.Axes.scatter` based on the style that
        has been given to the `skyline.tracks.GenomicScatter` object.

        Parameters
        ----------
        chr_name : `str`
            The chromosome to get the scatter keyword arguments for.
        """
        # Now set the chromosome colours, if the chromosome_style are not
        # set then they are taken from the style object.
        chromosome_style = self._chromosome_style
        if self._chromosome_style is None:
            chromosome_style = self._skyline_style.scatter_track_colors

        # The chromosome index. Note this is not the chromosome name it is
        # the rank plotting order (0-based), is chromosome 1 might be 0, X
        # might be 23. Although the actual number will depend in the
        # chromosomes being plotted, if you are only plotting chromosomes 10
        # and 11, then 10 will be 0 and 11 will be 1.
        chr_idx = self._skyline_coords.get_chr_name_index(chr_name)

        nstyles = len(chromosome_style)
        style_obj = chromosome_style[chr_idx % nstyles]

        if not isinstance(style_obj, dict):
            style_obj = style_obj.get_scatter_kwargs()

        try:
            if style_obj['cmap'] is not None:
                style_obj['c'] = self._skyline_data.loc[
                    self._skyline_data[con.CHR_NAME] == chr_name,
                    self._skyline_value_column
                ]
        except KeyError:
            # There is no cmap
            pass

        return style_obj


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class GenomicSkyviewScatter(GenomicScatter):
    """Generate a skyview scatter plot. Imagine looking down at a scatter plot.

    Parameters
    ----------
    data_label : `NoneType` or (`str` or `tuple`), optional, default: `NoneType`
        The y-axis label, but as there is not really a y-axis as such this is
        called a data label. Current the label can only be located on the left
        of the plot. If it is ``NoneType`` then no label is drawn. If it is a
        ``str``, then the default styling is used (or whatever is passed to the
        ``style`` parameter). If it is a tuple it should be of length 2 with
        the label text at ``[0]`` and the styling dictionary (keyword arguments
        to `Axes.set_ylabel`) at ``[1]``.

    Notes
    -----
    This Axes has the ylimits set to ``(0, 1)`` internally and will override
    whatever the user passes. This can of course be changed after creation but
    it is not recommended.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, jitter_breaks=[], jitter_min=0, jitter_max=1,
                 jitter_center=0.5, data_label=None, **kwargs):
        self._skyline_jitter_breaks = jitter_breaks
        self._skyline_jitter_center = jitter_center
        self._skyline_jitter_max = jitter_min
        self._skyline_jitter_min = jitter_max
        self._skyline_data_label = data_label
        super().__init__(*args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_skyline_plot_area(self):
        """Setup the skyline Axes plotting area. This sets attributes that are
        common to many of the Axes that inherit from `skyline.BaseGenomicAxes`
        """
        super().set_skyline_plot_area()
        self.set_ylim(0, 1)

        if self._skyline_data_label is not None:
            self.set_yticks([])
            self.spines['left'].set_visible(False)
            self.yaxis.set_ticklabels([])
        else:
            # Turn off the y-axis
            self.yaxis.set_visible(False)
            self.spines['left'].set_visible(False)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def plot_skyline(self, *args, **kwargs):
        """Now actually add the Artists to the Axes.

        Parameters
        ----------
        *args
            Any positional arguments to plot_skyline (currently there are none)
        **kwargs
            Any keyword arguments to plot_skyline (currently there are none)
        """
        # We use the superclass to perform the scatter plot and then determine
        # if we need to add a label to the plot
        super().plot_skyline()

        if self._skyline_data_label is not None:
            label_text = self._skyline_data_label
            style_obj = self._skyline_style.scatter_track_label

            if isinstance(self._skyline_data_label, (list, tuple)):
                label_text, style_obj = self._skyline_data_label

            if not isinstance(style_obj, dict):
                style_obj = style_obj.get_text_kwargs()

            self.set_ylabel(label_text, **style_obj)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_genomic_scatter_y(self, chr_name):
        """Get all the data y coordinates for the genomic scatter plot.

        Parameters
        ----------
        chr_name : `str`
            The chromosome to get the x coordinates for.

        Returns
        -------
        y_coords : `pandas.Series`
            The y-coordinates adjusted by the coordinates object.
        """
        value_data = self._skyline_data.loc[
            self._skyline_data[con.CHR_NAME] == chr_name,
            self._skyline_value_column
        ]

        # I will initialise to zero
        y = np.zeros(len(value_data))

        breakpoints = []
        if isinstance(self._skyline_jitter_breaks, int):
            data_max = self._skyline_data[self._skyline_value_column].max()
            data_min = self._skyline_data[self._skyline_value_column].min()

            nbounds = self._skyline_jitter_breaks
            jitter_inc = \
                (self._skyline_jitter_max - self._skyline_jitter_min)/nbounds
            data_inc = (data_max - data_min)/nbounds

            jitter_break = jitter_inc
            data_break = value_data.min()
            for i in range(nbounds):
                breakpoints.append((data_break, jitter_break))
                data_break += data_inc
                jitter_break += jitter_inc
        if isinstance(self._skyline_jitter_breaks, list):
            self._skyline_jitter_breaks.sort(key=lambda x: x[0])
            breakpoints = self._skyline_jitter_breaks

        for idx, breaks in enumerate(breakpoints):
            data_break, jitter_break = breaks

            try:
                upper_break = breakpoints[idx+1][0]
                matches = \
                    (value_data >= data_break) & (value_data < upper_break)
            except IndexError:
                matches = (value_data >= data_break)

            bound = jitter_break/2
            y[matches] = np.random.uniform(
                self._skyline_jitter_center - bound,
                self._skyline_jitter_center + bound,
                len(y[matches])
            )
        return y

    # # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # def add_roi(self, rois):
    #     """
    #     """
    #     data = self._data
    #     for i in rois:
    #         ic = i.copy()
    #         chr_name = str(ic.pop('chr_name'))
    #         pos = ic.pop('pos')
    #         locus_width = ic.pop('locus_width')
    #         start_pos = max(0, pos - locus_width)
    #         end_pos = min(
    #             self._skyline_coords.chr_lengths[chr_name]['length'],
    #             pos + locus_width
    #         )
    #         region_lookup = (
    #             (data['chr_name'] == chr_name) &
    #             (data['start_pos'] >= start_pos) &
    #             (data['start_pos'] <= end_pos)
    #         )

    #         x = data.loc[region_lookup, 'start_pos'] + \
    #             self._skyline_coords.get_coords(chr_name, 1)
    #         y = data.loc[region_lookup, 'pvalue']

    #         self.scatter(x, y, **ic)

    # # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # def set_marker_styles(self, markers):
    #     """
    #     """
    #     if markers is None:
    #         self._marker_styles = {}
    #     elif isinstance(markers, list):
    #         self._marker_styles = {}
    #         for idx, i in enumerate(self._skyline_coords.get_active_chr_name()):
    #             style = markers[idx % len(markers)]
    #             self._marker_styles[i] = style
    #     elif isinstance(markers, dict):
    #         self._marker_styles = markers
    #     else:
    #         raise TypeError("unknown marker style argument")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_converter(exponent=1E06):
    def converter(x, pos):
        return x / exponent
    return converter


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def bp_converter(x, pos):
    """
    """
    for i in ['bp', 'kbp', 'Mbp', 'Gbp']:
        test_pos = x / 1000
        if test_pos < 1:
            # return "{0} {1}".format(x, i)
            return x#"{0} {1}".format(x, i)
        x = test_pos
    raise ValueError("are you sure these are base pairs?")
