"""Plotting gene/transcript region tracks
"""
from skyline import (
    axes,
    utils
)
from skyline.patches import genes as gp
from matplotlib import text, patches
import warnings
# import pprint as pp


# I have made the constants private to enforce the idea that if changed they
# will screw stuff up but I might change this to make them public, not sure yet

# The indexes of the various attributes in the list that hold the gene
# information
_CHR_NAME = 0
_COORDS = 1
_STRAND = 2
_LABEL = 3
_BIOTYPE = 4
_STYLE = 5
_START_POS = 6
_END_POS = 7
_TRACK = 8
_PLOT_FONT_SIZE = 9
_PLOT_START = 10
_PLOT_END = 11
_UNIT_FONT_SIZE = 12
_UNIT_LABEL_START = 13
_UNIT_LABEL_END = 14
_UNIT_LABEL_WIDTH = 15
_UNIT_LABEL_HEIGHT = 16
_LABEL_TEXT_OBJ = 17
_DISPLAY_LABEL = 18

# Constants for strand information
_FORWARD_STRAND = 1
_REVERSE_STRAND = -1
_STRANDS = [_FORWARD_STRAND, _REVERSE_STRAND]
_FORWARD_STRAND_HA = 'left'
_REVERSE_STRAND_HA = 'right'

# Constants for the labels
_LABEL_VA = 'bottom'
_UNIT_FONTSIZE_VALUE = 1


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class GeneTrack(axes.BaseGenomicAxes):
    """A track that will display genes along a stretch of the genome where each
    gene will be displaying in a track such that it will not overlap any other
    genes.

    Parameters
    ----------
    figure : `matplotlib.Figure` or `skyline.GenomicFigure`
        The figure that the Axes will be associated with.
    track : `matplotlib.gridspec.SubplotSpec`
        The track grid coordinates that the Axes should be added to. These are
        obtained from a `matplotlib.GridSpec` subset.
    coord_mapper : `skyline.coords.ChrCoords`
        An object inheriting from `skyline.coords.ChrCoords`
    gene_features : `list` of [`skyline.features.SimpleTranscript` or
    `skyline.features.ExonTranscript`]
        Gene coordinates to plot. These can have styling information for a
        specific gene within them. So for the gene patch it would be:
        ``skyline.features.SimpleTranscript.style[GeneTrack.GENE_PATCH_KEY] = dict()``
        for gene trace styling use ``GeneTrack.GENE_TRACE_KEY`` and for gene
        label styling use: ``GeneTrack.GENE_LABEL_KEY``.
    *args
        Positional arguments to `skyline.axes.BaseGenomicAxes`
    gene_row_pad : `float`, optional, default: 0.1
        The amount of padding to add to the top and bottom of each track that
        is created
    gene_length_pad : `int`, optional, default: 0
        The amount of padding in base pairs to add to the gene flacks when
        determining track placement. This has the effect of shifting genes
        that are very close to each other onto separate tracks and can be used
        to avoid crowding of genes on the track.
    gene_label_height : `float`, optional, default: `0.5`
        The proportion of the gene row taken up by the gene label. Set this to
        ``0`` to remove labels altogether. Note currently there is a bug where
        if this is set to 0 then no genes are plotted.
    gene_label_pad : `float`, optional, default: `0.01`
        The proportion of the gene row to be used as space between the gene
        patch and the gene label.
    gene_patch : `skyline.patches.genes.BlockGene`
        The style of the gene to use in the track, must be a
        `skyline.patches.genes.BlockGene` or a subclass of it.
    track_bottom_pad : `float`, optional, default: 0.1
        The amount of padding to the bottom of the track. This is not padding
        to individual gene rows but rather the main track containing all the
        genes. For padding to individual gene rows see ``gene_row_pad``.
    track_top_pad : `float`, optional, default: 0.1
        The amount of padding to the top of the track. This is not padding to
        individual gene rows but rather the main track containing all the
        genes. For padding to individual gene rows see ``gene_row_pad``.
    gene_label_style : `dict`, optional, default: `NoneType`
        A style object to use for formatting of the gene labels or
        ``NoneType`` to use the default style object. You can also add styles
        to individual gene features.
    gene_patch_style : `dict` or `NoneType`
        A style object to use for formatting of the gene patches or
        ``NoneType`` to use the default style object. You can also add styles
        to individual gene features.
    gene_row_style : `list` of `dict`, optional, default: `NoneType`
        Colours for the backgrounds of the individual gene rows. The colours
        are recycled if number of gene rows is > than the length of
        ``gene_row_colors``. Each dict should contain keyword arguments for
        a `matplotlib.Axes.axhspan`
    plot_strand : `int` or `NoneType`, optional, default: ``NoneType``
        Only plot genes specific to a strand. If provided should be 1 for
        forward strand and -1 for reverse strand. ``NoneType`` means both
        strands.
    add_label_dir : `bool`, optional, default: `True`
        Add a directional symbol to the gene label, this is either a ``>`` for
        genes on the forward strand or a ``<`` for genes on the reverse strand.
    min_gene_rows : `int`, optional, default: `1`
        The minimum number of gene rows to use in the track. Rows with no genes
        will be left empty. This can be used to standardise the size of the
        genes in cases where multiple gene tracks are used.
    gene_patch_pointbreaks : `list` of `int`, optional, default: `[1000, 10000]`
        The base pair cutoffs that are used to place a directional arrow head
        on a gene patch for gene patches that support it (currently only
        ``skyline.patches.genes.BlockGene``). If the patch does not support
        arrow breakpoints, then this will be ignored.
    **kwargs
        Keyword arguments to `skyline.axes.BaseGenomicAxes`
    """
    GENE_TRACE_STYLE = axes.BaseGenomicAxes.STYLE_CLASS(
        facecolor='#DBDBDB40',
        edgecolor='#DBDBDB40',
        linewidth=0.5
    )
    """The default style for gene trace, this is a block of color extending
    from the gene to the edge of the Axes, either upwards or downwards, the
    dict keys should be appropriate keyword arguments for TODO: (`dict`).
    """
    GENE_ROW_STYLE = [
        axes.BaseGenomicAxes.STYLE_CLASS(edgecolor='none', facecolor='none')
    ]
    """The default style for alternating gene row background colours, these are
    color bands that sit behind the genes, the provided styles are recycled,
    the dict keys should be appropriate keyword arguments for TODO:
    (`list` of `dict`).
    """
    GENE_PATCH_STYLE = axes.BaseGenomicAxes.STYLE_CLASS(
        facecolor="#6666B1",
        edgecolor="#6666B1",
        linewidth=0.3
    )
    """The default style for the gene patch. Each gene is effectively a
    plotting symbol on the gene track the dict keys should be appropriate
    keyword arguments for ``matplotlib.patches.PathPatch`` (`dict`).
    """
    GENE_LABEL_STYLE = axes.BaseGenomicAxes.STYLE_CLASS(
        fontstyle='italic', va='bottom'
    )
    """The default style for gene labels. The ``dict`` keys should be
    appropriate for TODO: (`dict`)
    """
    GENE_LABEL_KEY = 'GENE_LABEL_STYLE'
    """The key name for gene label style in the main style dictionary. Do not
    change this value unless you know what you are doing (`str`)
    """
    GENE_ROW_KEY = 'GENE_ROW_STYLE'
    """The key name for gene row style in the main style dictionary. Do not
    change this value unless you know what you are doing (`str`)
    """
    GENE_TRACE_KEY = 'GENE_TRACE_STYLE'
    """The key name for gene trace style in the main style dictionary. Do not
    change this value unless you know what you are doing (`str`)
    """
    GENE_PATCH_KEY = 'GENE_PATCH_STYLE'
    """The key name for gene patch style in the main style dictionary. Do not
    change this value unless you know what you are doing (`str`)
    """
    STYLE = axes.BaseGenomicAxes.STYLE.copy()
    """All the default styles used by the ``skyline.tracks.GeneTrack``. The
    keys should be ``GENE_LABEL_STYLE``, ``GENE_ROW_STYLE``,
    ``GENE_TRACE_STYLE``, ``GENE_PATCH_STYLE``. Please remember this is a class
    variable and changes here will be used in all classes. If using as a
    template, to avoid this place make a deep copy (`dict`)
    """
    # Update the defaults for the main style dictionary
    STYLE[GENE_ROW_KEY] = GENE_ROW_STYLE
    STYLE[GENE_TRACE_KEY] = GENE_TRACE_STYLE
    STYLE[GENE_PATCH_KEY] = GENE_PATCH_STYLE
    STYLE[GENE_LABEL_KEY] = GENE_LABEL_STYLE

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coord_mapper, gene_features, *args,
                 gene_row_pad=0.1, gene_length_pad=0, gene_label_height=0.5,
                 gene_label_pad=0.01, gene_patch=gp.BlockGene,
                 track_bottom_pad=0.1, track_top_pad=0.1, gene_label_style=None,
                 gene_patch_style=None, gene_row_style=None,
                 plot_strand=None, add_label_dir=True, min_gene_rows=1,
                 gene_patch_pointbreaks=[1000, 10000],
                 **kwargs):
        # TODO: Fix bug where no genes are plotted with label_height of 0
        super().__init__(figure, track, coord_mapper, *args, **kwargs)
        self._sl_gene_features = gene_features
        self._sl_gene_patch = gene_patch
        self._sl_gene_row_pad = gene_row_pad
        self._sl_gene_length_pad = gene_length_pad
        self._sl_gene_row_colors = gene_row_style or self.GENE_ROW_STYLE
        self._sl_gene_patch_pointbreaks = gene_patch_pointbreaks
        self._sl_track_bottom_pad = track_bottom_pad
        self._sl_track_top_pad = track_top_pad
        self._sl_label_height = gene_label_height
        self._sl_gene_label_pad = gene_label_pad
        self._sl_fig_renderer = self.figure.canvas.get_renderer()
        self._sl_fig_dpi = self.figure.dpi  # Might not be needed not used
        self._sl_add_label_dir = add_label_dir
        self._sl_min_gene_rows = min_gene_rows

        # This is the font size that the gene label text is initialised at
        # before it is adjusted to fit the required space
        self._sl_base_font_size = 40

        # Set the instance defaults for gene patch style and gene label style
        self._sl_gene_patch_style = self.get_skyline_style_kwargs(
            gene_patch_style, self.GENE_PATCH_STYLE
        )
        self._sl_gene_label_style = self.get_skyline_style_kwargs(
            gene_label_style, self.GENE_LABEL_STYLE
        )

        # Initialise the number of gene rows and the label size scaling factor
        # both will be adjusted when the genes are placed  into their
        # respective rows
        self._sl_n_gene_rows = 1
        self._sl_scale_by = 1
        self._sl_gene_patch_height = 0
        self._sl_gene_patch_bottom = 0
        self._sl_gene_patch_top = 1

        # The default strands that will be plotted
        self._sl_plot_strand = _STRANDS

        if self._sl_min_gene_rows < 1:
            raise ValueError("min gene rows must be >= 1")

        # Validate and initialise the plot strand argument
        self._sl_set_plot_strand(plot_strand)

        # Make sure the gene patch is what we expect it to be
        if not issubclass(self._sl_gene_patch, gp.BlockGene):
            raise TypeError(
                "gene patch must be a subclass of skyline.patches.genes."
                "BlockGene"
            )

        # Sanity check some of the arguments
        self._sl_check_pad_arg(self._sl_gene_row_pad, "gene_row_pad")
        self._sl_check_pad_arg(self._sl_gene_length_pad, "gene_pad_length")
        self._sl_check_pad_arg(self._sl_label_height, "label_height")

        # Initialise the y-limit to 1-2, this will be updated based on min rows
        # and the assigned gene tracks.
        self.set_ylim(1, 2)

        # Initialise sizes, so we work out the gene patch height. This will
        # also throw an error if the label height/padding has been set so large
        # that there is no space for genes
        self._sl_calc_gene_patch_height()

        # # Process the coordinates into a list and calculate the genomic
        # # coordinates based on the gene structure coordinates.
        # self._sl_init_gene_coords()

        # Perform some initial processing on the genes that have been passed
        # to calculate the initial start/end locations in plotting coordinates
        self._sl_init_gene_plot_coords()

        # Now deal with the labels
        if self._sl_label_height > 0:
            # This will determine the font size and number of tracks based on
            # label length and gene length/overlaps
            self._optimise_gene_rows()
            # self._set_gene_tracks()
        else:
            self._unit_scale_by = 1
            # Otherwise we set the placement based on the gene length and what
            # ever padding we have
            # Determine the gene placement on the tracks
            self._set_gene_rows()

            # Finally make sure that the y-limits
            self._set_y_limits()
        # # pp.pprint(self._sl_gene_features)

        # # Used to derive an offset to align the genes to the top of the track
        # # when min_rows > gene_rows
        # self._set_blank_rows()

        # Now perform the plot
        self._plot_track_background()
        self._plot_genes()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __str__(self):
        """Pretty printing, for some reason __repr__ does not work?
        """
        for_print = []
        atts = ['_sl_label_height', '_sl_gene_row_pad',
                '_sl_gene_patch_height', '_sl_gene_patch_bottom',
                '_sl_gene_patch_top']
        for i in atts:
            display_label = i[4:]
            for_print.append(
                "{0}={1}".format(display_label, round(getattr(self, i), 2))
            )

        # The numbers for forward and reverse strand genes
        nforward = len([i for i in self._sl_gene_features if i.strand == 1])
        nreverse = len([i for i in self._sl_gene_features if i.strand == -1])
        for_print.append(f"forward_strand={nforward}")
        for_print.append(f"reverse_strand={nreverse}")

        return "<{0}({1})>".format(
            self.__class__.__name__, ", ".join(for_print)
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_skyline_plot_area(self):
        """Setup the skyline Axes plotting area. This sets attributes that are
        common to many of the Axes that inherit from `skyline.BaseGenomicAxes`
        """
        super().set_skyline_plot_area()

        # By default, gene tracks will have the x-axis on
        self.xaxis.set_visible(True)
        self.spines['bottom'].set_visible(True)

        # self.yaxis.set_visible(True)
        # self.spines['top'].set_visible(True)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def print_gene(self, gf):
        """Print all the gene attributes for a gene feature, mainly used for
        debugging.
        """
        gene_data = [
            f"gene_display_label=gf.gene_display_label",
            f"row_no={gf.row_no}",
            f"font_size={gf.font_size}",
            f"strand={gf.strand}",
            f"virtual_start_pos={gf.virtual_start_pos}",
            f"virtual_end_pos={gf.virtual_end_pos}",
            f"horizontal_align=gf.horizontal_align",
            f"label_start={gf.label_start}",
            f"label_auto_size={gf.label_auto_size}",
            f"gene_label_artist={gf.gene_label_artist}",
            f"rendered_label_height={gf.rendered_label_height}",
            f"rendered_label_width={gf.rendered_label_width}",
            f"rendered_label_start_pos={gf.rendered_label_start_pos}",
            f"rendered_label_end_pos={gf.rendered_label_end_pos}"
        ]
        return "\n".join(gene_data)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def print_debug(self):
        """Print all the gene attributes for all gene features, mainly used for
        debugging.
        """
        print("*******************")
        print(f"ylim={self.get_ylim()}")
        print(f"min_gene_rows={self._sl_min_gene_rows}")
        print(f"n_gene_rows={self._sl_n_gene_rows}")
        print(f"scale_by={self._sl_scale_by}")
        for i in self._sl_gene_features:
            print("------------------")
            print(self.print_gene(i))
            print("------------------")
        print("*******************")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def n_gene_rows(self):
        """The number of gene rows within the overall track.
        """
        return max(self._sl_min_gene_rows, self._sl_n_gene_rows)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _sl_set_plot_strand(self, plot_strand):
        """Validate and initialise the ``plot_strand`` argument.

        Parameters
        ----------
        plot_strand : `int` or `NoneType`
            The strands of the genes that you want to plot in the gene track.
            ``NoneType`` means both strands and is set internally to
            ``[1, -1]``. Otherwise it should be either ``-1`` for the reverse
            strand or ``1`` for the forward strand.

        Raises
        ------
        ValueError
            If plot_strand is not ``1``, ``-1`` or ``NoneType``
        """
        if plot_strand is None:
            self._sl_plot_strand = _STRANDS
        else:
            if plot_strand not in _STRANDS:
                raise ValueError("plot strand must be 1 or -1 or NoneType")
            self._sl_plot_strand = [plot_strand]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def _sl_check_pad_arg(cls, pad_arg, arg_type):
        """Ensure the pad argument is greater than or equal to 0

        Parameters
        ----------
        pad_arg : `int`
            The padding argument to be checked
        arg_type : `str`
            An argument type to be used in any error messages that might be
            generated

        Raises
        ------
        ValueError
            If the ``pad_arg`` is < 0
        """
        if pad_arg < 0:
            raise ValueError("{0} must be >= 0".format(arg_type))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _sl_calc_gene_patch_height(self):
        """Calculate the gene patch height by accounting for the width of the
        label and the gene track padding.

        This also calculates the bottom and top of the gene batch coordinates
        (relative to the track it is on).

        Raises
        ------
        ValueError
            If the gene track height is <= 0
        """
        # The height allotted to the actual gene representation is what ever
        # is leftover after removing the top/bottom padding and the label area
        self._sl_gene_patch_height = (
            1 - (self._sl_gene_row_pad * 2) - self._sl_label_height -
            self._sl_gene_label_pad
        )
        # print(self._sl_gene_patch_height)
        if self._sl_gene_patch_height <= 0:
            raise ValueError(
                "gene patch has no space: {0}".format(
                    self._sl_gene_patch_height
                )
            )

        # Calculate the relative coordinates for the bottom and top of the
        # gene patch
        self._sl_gene_patch_bottom = (
            self._sl_gene_row_pad + self._sl_label_height +
            self._sl_gene_label_pad
        )
        self._sl_gene_patch_top = \
            self._sl_gene_patch_bottom + self._sl_gene_patch_height

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _sl_init_gene_plot_coords(self):
        """Initialise the plotting coordinates and track for each gene. This
        takes the input and internally represents as a list, where the elements
        are updated during the plotting process.
        """
        # Initialise the number of tracks to 1
        self._sl_n_gene_rows = 1

        # Scale by will hold the scaling factor to scale the unit label size up
        # to the unit track size, this can then be normalised to the number of
        # tracks
        self._sl_scale_by = None

        to_plot = []
        for gf in self._sl_gene_features:
            if gf.strand not in self._sl_plot_strand:
                continue

            # Add the coordinates mapper to the gene feature
            gf.add_coords_mapper(self._skyline_coords)

            to_plot.append(gf)
            # Initialise to the first track
            gf.row_no = 1
            gf.font_size = self._sl_base_font_size

            # Now we define the plot start and plot end, these coordinates are
            # used to determine the space taken up by the gene, any labels
            # and any padding around the gene on the plot.
            gf.virtual_start_pos = gf.plot_start_pos - self._sl_gene_length_pad
            gf.virtual_end_pos = gf.plot_end_pos + self._sl_gene_length_pad

            # Set the gene display label - this will be the label with
            # potentially a < or > if we are adding label directions
            gf.gene_display_label = None

            # The horizontal alignment of the gene label text will depend on
            # the strand of the gene
            gf.horizontal_align = None

            # The start position of the label will end up either being aligned
            # to the gene start or the gene end
            gf.label_start = None

            # If the use has supplied a fontsize in the gene label style then
            # this will turn off any auto sizing
            gf.label_auto_size = False

            # Will hold the Text() object with containing the gene label
            gf.gene_label_artist = None

            # The rendered sizes are those calculated from the label bounding
            # box based on the font size of 1 (in the case of auto sizing)
            # The heights of the label
            gf.rendered_label_height = 0

            # The start and end position of the label. Note that this is
            # initialised to the start and end position of the gene to cover
            # the scenario where lebelsize == 0, this will be adjusted if that
            # is not the case
            gf.rendered_label_width = gf.plot_end_pos - gf.plot_start_pos
            gf.rendered_label_start_pos = gf.plot_start_pos
            gf.rendered_label_end_pos = gf.plot_end_pos

            # If we are plotting labels then we have to create them so we can
            # size them to get the various scaling factors
            if self._sl_label_height > 0:
                gf.gene_display_label = gf.label

                # Set the label and horizontal alignment depending on the
                # strand of the gene
                if gf.strand == _FORWARD_STRAND:
                    gf.horizontal_align = _FORWARD_STRAND_HA
                    gf.label_start = gf.plot_start_pos

                    if self._sl_add_label_dir is True:
                        gf.gene_display_label = f"{gf.label} >"
                elif gf.strand == _REVERSE_STRAND:
                    gf.horizontal_align = _REVERSE_STRAND_HA
                    gf.label_start = gf.plot_end_pos

                    if self._sl_add_label_dir is True:
                        gf.gene_display_label = f"< {gf.label}"

                try:
                    gene_label_style = self.get_skyline_style_kwargs(
                        gf.styles[self.GENE_LABEL_KEY],
                        self._sl_gene_label_style
                    )
                except KeyError:
                    # Not defined so go to the instance default
                    gene_label_style = self._sl_gene_label_style.copy()

                # Has the user defined a font size, if so then, we will still
                # do the scaling etc but will not use it. I will eventually
                # change this
                gf.label_auto_size = False
                if 'fontsize' not in gene_label_style:
                    gf.label_auto_size = True
                    gene_label_style['fontsize'] = gf.font_size

                # Create a text artists and add it to the axes
                gf.gene_label_artist = text.Text(
                    gf.label_start, gf.row_no + self._sl_gene_row_pad,
                    gf.gene_display_label, ha=gf.horizontal_align,
                    **gene_label_style
                )
                self.add_artist(gf.gene_label_artist)

                # Now we get the size in data coordinates of the unit label
                # and calculate it's height - this is used to determine the
                # scaling factor
                box = gf.gene_label_artist.get_window_extent(
                    renderer=self._sl_fig_renderer
                )
                box = self.transData.inverted().transform(box)
                gf.rendered_label_height = box[1, 1] - box[0, 1]
                try:
                    # Calculate the scaling factor based on what the label
                    # height is (in plot y-axis units) and the actual label
                    # height
                    self._sl_scale_by = max(
                        self._sl_scale_by,
                        (self._sl_label_height / gf.rendered_label_height)
                    )
                except TypeError:
                    # Initialisation, scale_by is NoneType but we check to make
                    # sure
                    if self._sl_scale_by is not None:
                        raise
                    self._sl_scale_by = \
                        self._sl_label_height / gf.rendered_label_height
                # Now update the rendered start/end coordinates
                gf.rendered_label_start_pos = box[0, 0]
                gf.rendered_label_end_pos = box[1, 0]
                gf.rendered_label_width = box[1, 0] - box[0, 0]

        # Update the genes
        self._sl_gene_features = to_plot

        # Set the scaling factor by which the unit fontsize will be scaled
        self._unit_scale_by = self._sl_scale_by
        self._sort_genes()

        if len(to_plot) == 0:
            warnings.warn("no genes to plot")
            self._sl_scale_by = 1
            self._unit_scale_by = self._sl_scale_by

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _sort_genes(self):
        """Sort the genes so that the leftmost gene is at ``[0]`` and the
        rightmost is at ``[-1]``, if there is a tie then the longest gene is
        leftmost. Note the genes are sorted on plotting space not genomic
        coordinates (i.e. inclusive of their label size)
        """
        # First sort on length
        self._sl_gene_features.sort(key=lambda x: len(x), reverse=True)

        # Then of start
        self._sl_gene_features.sort(key=lambda x: x.plot_start_pos)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _optimise_gene_rows(self):
        """A simple optimisation step that aims to balance the label size with
        the number of tracks.
        """
        # This is to make sure that we do not optimise back to a value we have
        # already found and end up in an infinite loop. So if we end up to
        # somewhere where we have already been then that is optimised
        attempted_n_rows = set()
        while self._sl_n_gene_rows not in attempted_n_rows:
            attempted_n_rows.add(self._sl_n_gene_rows)

            # Now we know how many tracks we have, then we will set the
            # y-limits
            self._set_y_limits()

            # Adjust the PLOT_START/PLOT_END and PLOT_FONTSIZE for the current
            # number of tracks
            self._scale_gene_attributes()

            # Set all the tracks in the genes to 1 (note that the n_tracks
            # variable is not reset by this) as it is needed by the label
            # scaler below
            self._reset_rows()

            # Determine the gene placement on the tracks
            self._set_gene_rows()

        # Finally make sure that the y-limits
        self._set_y_limits()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _set_y_limits(self):
        """The y-limits are set to the number of tracks with 1 being the first
        track and N being the last track. The track bottom padding is
        subtracted off the 1 and top padding is added to N.
        """
        # # The number of tracks in the y range may be more than are occupied
        # # by genes
        # n_rows = max(self._sl_min_gene_rows, self._sl_n_gene_rows)

        ymin = 1 - self._sl_track_bottom_pad
        ymax = self.n_gene_rows + 1 + self._sl_track_top_pad
        # ymax = self.n_gene_rows + self._sl_track_top_pad

        # Set the Axes y limit
        self.set_ylim(ymin, ymax)

        y_range = ymax - ymin

        # When we calculate font sizes etc, we do it for a single row and then
        # we adjust for the number of rows that we have
        self._sl_scale_by = self._unit_scale_by / y_range

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _scale_gene_attributes(self):
        """Adjust the plot start/end according to the label size and number of
        tracks.
        """
        # Loop through all the genes that will be placed
        for gf in self._sl_gene_features:
            # Set the scaled font size that we will plot at
            gf.font_size = self._sl_scale_by * self._sl_base_font_size


            # These represent the minimal start and end position of each gene
            # They will be adjusted to take into account the size with the
            # label (if it is bigger than the gene length)
            virtual_start = gf.plot_start_pos - self._sl_gene_length_pad
            virtual_end = gf.plot_end_pos + self._sl_gene_length_pad
            gf.virtual_start_pos = gf.plot_start_pos - self._sl_gene_length_pad
            gf.virtual_end_pos = gf.plot_end_pos + self._sl_gene_length_pad

            # The direction we expand the gene in is dependent on strand. Genes
            # on the positive strand will expand rightwards (i.e. their end
            # position will be modified). Genes on the reverse strand will
            # expand leftwards, i.e. their start position will be modified.
            # Note, the plot position is not the actual position that will be
            # plotted but is used to determine if the gene+label size will
            # overlap the next gene in the sort order. (see _set_gene_tracks)
            scaled_label_width = gf.rendered_label_width * self._sl_scale_by

            # for forward strand we extend any oversize labels to the right
            if gf.strand == _FORWARD_STRAND:
                gf.virtual_start_pos = virtual_start
                gf.virtual_end_pos = max(
                    virtual_end,
                    virtual_start + scaled_label_width
                )
            else:
                gf.virtual_start_pos = min(
                    virtual_start,
                    virtual_end - scaled_label_width
                )
                gf.virtual_end_pos = virtual_end

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _reset_rows(self):
        """Reset all the track numbers of the genes and the number of tracks
        to 1
        """
        self._sl_n_gene_rows = 1

        for gf in self._sl_gene_features:
            gf.row_no = 1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _set_gene_rows(self):
        """Organise the genes into tracks. This is based on their total plot
        positioning including any label sizing.
        """
        self._sort_genes()

        # Now loop through all the other genes
        for idx in range(1, len(self._sl_gene_features)):
            gf = self._sl_gene_features[idx]

            # If the current gene can be assigned to an existing track then
            # this will be set to an integer track value. However, if it is
            # still NoneType after checking everything then that indicates that
            # we need a new track
            assigned_track = None

            # If we have a collision with a gene in a track it is added to the
            # excluded tracks set, so we will not evaluate genes with that
            # track again (for the current gene). This may not be needed as
            # everything is sorted
            excluded_tracks = set()

            # Compare the current gene to all the previous genes. The basic
            # idea is that we want to put the gene in the lowest track possible
            for prev_idx in reversed(range(0, idx)):
                # Code clarity
                prev_gf = self._sl_gene_features[prev_idx]

                # Have already excluded the row
                if prev_gf.row_no in excluded_tracks:
                    continue

                # Do the genes+labels overlap
                if utils.overlap_1d(
                        (gf.virtual_start_pos, gf.virtual_end_pos),
                        (prev_gf.virtual_start_pos, prev_gf.virtual_end_pos)
                ) == 0:
                    try:
                        # No overlap with a previous gene
                        assigned_track = min(assigned_track, prev_gf.row_no)
                    except TypeError:
                        # No assigned track yet so initialise
                        assigned_track = prev_gf.row_no

                    # If we have minimised the track value then we do not need
                    # to check any more
                    if assigned_track == 1:
                        break
                else:
                    # There is an overlap, so we will exclude this track from
                    # future comparisons
                    excluded_tracks.add(prev_gf.row_no)

            # This will happen if the track overlaps all the previous tracks
            if assigned_track is None:
                self._sl_n_gene_rows += 1
                assigned_track = self._sl_n_gene_rows

            gf.row_no = assigned_track

            # Update the number of gene rows as we go through
            self._sl_n_gene_rows = max(self._sl_n_gene_rows, assigned_track)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_gene_traces(self, trace_style=None, direction="down"):
        """Add traces to all the genes in the plot
        """
        trace_style = trace_style or dict()

        trace_style = self.get_skyline_style_kwargs(
            self.STYLE[self.GENE_TRACE_KEY], trace_style,
        )
        trace_order = sorted(
            self._sl_gene_features,
            key=lambda x: x.plot_start_pos - x.plot_end_pos,
            reverse=True
        )
        # pp.pprint(trace_order)
        for i in trace_order:
            try:
                gene_trace = self.get_skyline_style_kwargs(
                    trace_style, i.styles[self.GENE_TRACE_KEY]
                )
            except KeyError:
                gene_trace = trace_style

            self.add_gene_trace(
                i,
                trace_style=gene_trace,
                direction=direction
            )
            # break

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_gene_trace(self, gene_feature, trace_style=None, direction="down"):
        """Add a trace for a gene.
        """
        # print(gene_feature)
        # print(gene_feature.row_no)
        trace_style = trace_style or dict()

        trace_style = self.get_skyline_style_kwargs(
            self.STYLE[self.GENE_TRACE_KEY], trace_style,
        )

        try:
            gf = self._sl_gene_features[
                self._sl_gene_features.index(gene_feature)
            ]
        except ValueError:
            raise ValueError(
                f"can't find gene feature in gene track: {gf.label}"
            )

        # height = self._sl_n_gene_rows - gf.row_no + 2 + len(self._blank_rows)
        height = self.n_gene_rows - gf.row_no + 2 + self._sl_gene_row_pad
        width = gf.plot_end_pos - gf.plot_start_pos
        rect = patches.Rectangle(
            (gf.plot_start_pos, 0), width, height,
            zorder=-1, **trace_style
        )
        self.add_patch(rect)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _plot_genes(self):
        """Plot the gene patches on the tracks.
        """
        nrows = self.n_gene_rows + 1

        for gf in self._sl_gene_features:
            # This just inverts the tracks so that track one becomes n_tracks
            # and n_tracks becomes track 1 we also adjust for any blank rows
            # so that all the genes are top aligned
            plot_track = nrows - gf.row_no

            try:
                patch_style = self.get_skyline_style_kwargs(
                    gf.styles[self.GENE_PATCH_KEY],
                    self._sl_gene_patch_style
                )
            except KeyError:
                patch_style = self._sl_gene_patch_style

            # Now plot the gene patch, note that we are using the actual
            # coordinates for this, not coordinates + label size
            # gene_patch_bottom is at the top of the space needed for the label
            # gene_patch_top extends to the top of the gene row
            gene_patch = self._sl_gene_patch(
                gf,  # [(gf.plot_start_pos, gf.plot_end_pos, True)],
                bottom=plot_track + self._sl_gene_patch_bottom,
                top=plot_track + self._sl_gene_patch_top,
                gene_name=gf.gene_display_label,
                style=patch_style,
                point_breaks=self._sl_gene_patch_pointbreaks
            )
            self.add_patch(gene_patch)

            # If we are plotting labels then, they should all ready be added to
            # the Axes and it is just a matter of setting their final location
            # and fontsize
            if self._sl_label_height > 0:
                # print("Plot Font: {0}".format(coords[_PLOT_FONT_SIZE]))
                # TODO: I have to figure out why the font size is twice what
                #  it should be, so I have put in a fudge factor here
                gf.gene_label_artist.set_fontsize(gf.font_size)
                #     coords[_PLOT_FONT_SIZE] / 2
                # )

                gf.gene_label_artist.set_position(
                    (
                        # We use the x coordinate that was used in label
                        # also the horizontal alignment that was set
                        # (i.e. not changed)
                        gf.gene_label_artist.get_position()[0],
                        # The label bottom will be at the plot track just
                        # after the padding
                        plot_track + self._sl_gene_row_pad
                    )
                )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _plot_track_background(self):
        """Plot the background colours (if we need to).
        """
        for i in range(self.n_gene_rows):
            color = self._sl_gene_row_colors[i % len(self._sl_gene_row_colors)]
            # TODO: Shall I store these span artists as I may need them for
            #  some z-order alterations?
            self.axhspan(i+1, i+2, **color)
