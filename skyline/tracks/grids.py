"""A grid matrix style track
"""
from skyline import axes, utils, align
from matplotlib.path import Path
from matplotlib import patches
import numpy as np
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class BooleanGrid(axes.BaseGenomicAxes):
    """Create a boolean grid for outputting yes/no status for various attributes
    of genomic features.

    Parameters
    ----------
    figure : `matplotlib.Figure` or `skyline.GenomicFigure`
        The figure that the Axes will be associated with.
    track : `matplotlib.gridspec.SubplotSpec`
        The track grid coordinates that the Axes should be added to. These are
        obtained from a `matplotlib.GridSpec` subset.
    coords : `skyline.coords.ChrCoords`
        An object inheriting from `skyline.coords.ChrCoords`
    grid : `pandas.DataFrame`
        The `True`/`False` status for attributes (defined in rows) of the
        various genomic features (defined in the columns). The attribute and
        feature names are taken directly from the row/column names
        respectively.
    *args
        Arguments to `skyline.axes.BaseGenomicAxes`.
    grid_style : `dict`, optional, default: `NoneType``
        The default grid line style for the matrix, if `NoneType` then the
        class default style is used. The attributes of the dict should be
        appropriate for `matplotlib.Axes.axvline` and
        `matplotlib.Axes.axvline`.
    border_style : `dict`, optional, default: `NoneType``
        The default border style for the matrix, if `NoneType` then the
        class default style is used. The attributes of the dict should be
        approproprite for `matplotlib.patches.Rectangle`. The border is plotted
        after the grid-lines but the z-order argument should still work.
    true_style : `dict`, optional, default: `NoneType`
        The marker attributes (appropriate to `matplotlib.Axes.scatter`) for
        `True` values. If `NoneType` then the class default style is used.
    false_style : `dict`, optional, default: `NoneType`
        The marker attributes (appropriate to `matplotlib.Axes.scatter`) for
        `False` values. If `NoneType` then the class default style is used.
    **kwargs
        Keyword arguments to `matplotlib.Axes`.
    """
    _DEFAULT_GRID_STYLE = dict(
        # Light grey with 75% alpha
        color='#DBDBDB40',
        linewidth=0.5
    )
    _DEFAULT_BORDER_STYLE = dict(
        # Light grey with 75% alpha
        edgecolor='grey',
        linewidth=1,
        facecolor='none'
    )
    _DEFAULT_TRUE_STYLE = dict(
        # Light grey with 75% alpha
        marker='o',
        c='red',
        linewidth=0.5,
        edgecolor='black'
    )
    _DEFAULT_FALSE_STYLE = dict(
        # Light grey with 0% alpha
        marker='o',
        c='#ECECEC',
        linewidth=0.5,
        edgecolor='#D3D3D3'
    )
    _DEFAULT_XLABEL_STYLE = dict(
        rotation=90
    )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, grid, *args,
                 grid_style=None, border_style=None, true_style=None,
                 false_style=None, **kwargs):
        # store the data for the grid
        self._sl_grid = grid
        self._sl_grid_style = grid_style or self.__class__._DEFAULT_GRID_STYLE
        self._sl_border_style = grid_style or \
            self.__class__._DEFAULT_BORDER_STYLE

        # Set and or update the true or false style, will will use what the
        # user has supplied to overide the defaults
        true_style = true_style or dict()
        self._sl_true_style = \
            {**self.__class__._DEFAULT_TRUE_STYLE, **true_style}

        false_style = false_style or dict()
        self._sl_false_style = \
            {**self.__class__._DEFAULT_FALSE_STYLE, **false_style}

        # Extract the do_plot argument if the user has passed it, we will act
        # on this after calling the super class
        do_plot = kwargs.pop('do_plot', True)

        super().__init__(
            figure, track, coords, *args, do_plot=False, **kwargs
        )

        if do_plot is True:
            self.plot_skyline()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def DEFAULT_TRUE_STYLE(cls):
        """Get the default styling for the true entries in the grid
        """
        return cls._DEFAULT_TRUE_STYLE.copy()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def DEFAULT_FALSE_STYLE(cls):
        """Get the default styling for the false entries in the grid
        """
        return cls._DEFAULT_FALSE_STYLE.copy()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_skyline_plot_area(self):
        """Setup the skyline Axes plotting area. This sets attributes that are
        common to many of the Axes that inherit from `skyline.BaseGenomicAxes`
        """
        super().set_skyline_plot_area()

        # Set the y-limits to the number of attributes in the boolean matrix
        self.set_ylim(0, self._sl_grid.shape[0] + 1)

        # self.set_yticks([])
        self.spines['left'].set_visible(False)
        # self.yaxis.set_ticklabels([])
        self.set_xticks([])
        self.spines['bottom'].set_visible(False)
        self.xaxis.set_ticklabels([])

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _set_xlim_params(self):
        """Set the x-limit parameters for the grid
        """
        # Determine the grid unit (the separation distance between grid points)
        self._sl_xlim_start, self._sl_xlim_end = self.get_xlim()
        self._sl_xlim_range = self._sl_xlim_end - self._sl_xlim_start
        self._sl_grid_unit = self._sl_xlim_range/(self._sl_grid.shape[1] + 1)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def plot_skyline(self, *args, **kwargs):
        """Perform the skyline plot. Only call this if object was initialised
        with ``do_plot=False``.

        Parameters
        ----------
        *args
            Positional arguments (ignored)
        **kwargs
            Keyword arguments (ignored)
        """
        self._set_xlim_params()

        # Plot the vertical grid lines
        prev_start = self._sl_xlim_start
        for i in range(self._sl_grid.shape[1]):
            cur_start = prev_start + self._sl_grid_unit
            self.axvline(cur_start, **self._sl_grid_style)
            prev_start = cur_start

        # plot the horizontal grid lines
        for i in range(self._sl_grid.shape[0] + 1):
            self.axhline(i, **self._sl_grid_style)

        # Now the border
        border = patches.Rectangle(
            (self._sl_xlim_start, 0), self._sl_xlim_range,
            self._sl_grid.shape[0] + 1,
            **self._sl_border_style
        )
        self.add_patch(border)

        # Now add the points to the plot
        x, y = self._get_sl_grid_coords(True)
        self.scatter(x, y, **self._sl_true_style)
        x, y = self._get_sl_grid_coords(False)
        self.scatter(x, y, **self._sl_false_style)

        self.yaxis.set_visible(True)
        self.yaxis.set_ticks(np.arange(self._sl_grid.shape[0]) + 1)
        self.yaxis.set_ticklabels(
            reversed(self._sl_grid.index.to_list()), fontsize='xx-small'
        )
        # self.set_yticks(range(self._sl_grid.shape[0]))
        # self.set_yticklabels(self._sl_grid.index.to_list())

        # print(x)
        #
        x = self._sl_xlim_start + (np.arange(1, self._sl_grid.shape[1] + 1) * self._sl_grid_unit)
        self.xaxis.set_visible(True)
        self.xaxis.set_ticks(x)
        self.xaxis.set_ticklabels(
            self._sl_grid.columns.to_list(), rotation=90, fontstyle='italic', fontsize='xx-small'
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_sl_grid_coords(self, is_true):
        """Get the  scatter plot x,y coordinates for the values in the matrix
        that are either True or false
        """
        coords = np.array(np.where(self._sl_grid == int(is_true)))
        y = np.abs((coords[0, :] + 1) - self._sl_grid.shape[0]) + 1
        x = self._sl_xlim_start + ((coords[1, :] + 1) * self._sl_grid_unit)
        return x, y
