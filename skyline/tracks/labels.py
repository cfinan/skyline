"""Classes for handling labelling
"""
from matplotlib import text
from skyline import (
    axes,
    align,
    coords,
    styles,
    features,
    colors as skycol
)
from itertools import cycle
import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Labels(axes.BaseGenomicAxes):
    """A track that contains plotting labels

    This will try to position them as close to their requested genomic
    coordinates as possible without them overlapping.

    Parameters
    ----------
    figure : `matplotlib.Figure` or `skyline.GenomicFigure`
        The figure that the Axes will be associated with.
    track : `matplotlib.gridspec.SubplotSpec`
        The track grid coordinates that the Axes should be added to. These are
        obtained from a `matplotlib.GridSpec` subset.
    coords : `skyline.coords.ChrCoords`
        An object inheriting from `skyline.coords.ChrCoords`
    *args
        Other positional arguments to `matplotlib.axes.Axes`
    labels : `list` of `skyline.features.FeatureCoords`
        Objects inheriting from `skyline.features.FeatureCoords` that will form
        the basis of the labels. The actual label text is dictated by the.
        `skyline.features.FeatureCoords.display_label` property. To change the
        style of individual label components you should add a style with the
        name ``LABEL_TEXT`` and the style parameters should be appropriate for
        `matplotlib.text.Text`, these are merged with any default styling
        parameters.
    label_style : `dict`, optional, default: `NoneType`
        A style to use for all the labels, this overrides the default style,
        but will also be overridden by any individual label styling parameters.
        The the style parameters should be appropriate for
        `matplotlib.text.Text`
    label_y_pos : `float`, optional, default: `0.07`
        A y-offset for the label placement on the Axes, should be between 0-1
        inclusive.
    **kwargs
        Any keyword arguments for `matplotlib.axes.Axes`

    Notes
    -----
    This will store the actual text label artists inside the supplied labels.
    The artists will be stored under a key that has the
    ``<class name>.LABEL_TEXT.<HEX MEM REFERENCE>``.

    See also
    --------
    skyline.labels.Labels.LABEL_TRACK_STYLE
    skyline.align.Align1D
    skyline.features.FeatureCoords
    matplotlib.text.Text
    """
    LABEL_TRACK_STYLE = axes.BaseGenomicAxes.STYLE_CLASS(
        rotation=90,
        fontstyle='italic',
        color=skycol.MANHATTAN_GREY_ODD,
        horizontalalignment='center',
        fontsize=5.0
    )
    """A default style for the text that occurs in a label track. The dict
    keys should be appropriate for ``matplotlib.text.Text`` (`dict`)
    """

    LABEL_TRACK_STYLE_KEY = 'LABEL_TRACK_STYLE'
    """The name of the marker style key in the default ``STYLES`` class
    attribute (`str`)
    """
    STYLE = axes.BaseGenomicAxes.STYLE_CLASS()
    STYLE[LABEL_TRACK_STYLE_KEY] = LABEL_TRACK_STYLE

    # _DEFAULT_KWARGS = dict(
    #     ylim=(0, 1)
    # )
    LABEL_TEXT = "LABEL_TEXT"
    """The individual label text key, use this to add specific label
    formatting parameters (`str`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, labels, label_style=None,
                 label_y_pos=0.07, *args, **kwargs):
        if not all(
                issubclass(i.__class__, features.FeatureCoords) for i in labels
        ):
            raise TypeError(
                "labels should be subclass of features.FeatureCoords objects"
            )

        # A text label key ID
        self._skyl_label_id = "{0}.{1}.{2}".format(
            self.__class__.__name__,
            self.LABEL_TEXT,
            hex(id(self))
        )

        # The feature coordinate objects that will dictate the labels
        self._skyl_labels = labels
        self.sky_label_y_pos = label_y_pos

        # The plot positions of the labels
        self._positions = []

        # Merge any default label styles with anything the user has supplied
        self._skyl_label_style = self.get_skyline_style_kwargs(
            label_style, self.LABEL_TRACK_STYLE
        )

        # Add the coordinates object to the labels features
        kwargs['add_coords_to'] = self._skyl_labels

        # Call the superclass to set things up
        super().__init__(figure, track, coords, *args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def label_plot_coords(self):
        """Return the requested start/end positions and the actual
        start/end positions in plotting coordinates (so no chromosomes)
        (`tuple` of `tuples` of `int`).
        """
        plot_labels = []
        for i, j in self._positions:
            plot_pos = j.copy()
            diff = plot_pos.end_pos - plot_pos.start_pos
            plot_coords = \
                plot_pos.artists[self._skyl_label_id].get_position()[0]
            plot_pos.set_pos(
                plot_coords, plot_coords + diff
            )
            plot_pos.delete_coords_mapper()
            plot_pos.add_coords_mapper(coords.PassthroughCoords())
            plot_labels.append((plot_pos, i))
        return plot_labels

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _sky_create_artists(self):
        """Initialise the matplotlib.Text label Artists and associate them
        with the coordinates.
        """
        for idx, i in enumerate(self._skyl_labels):
            try:
                style = {
                    **self._skyl_label_style,
                    **i.styles[self.LABEL_TEXT]
                }
            except KeyError:
                style = self._skyl_label_style.copy()

            plot_center = i.center_coord(i.plot_start_pos, i.plot_end_pos)

            label_text = text.Text(
                x=plot_center, y=self.sky_label_y_pos, text=i.label,
                **style
            )
            i.requested_pos = plot_center
            i.delta_pos = 0
            i.priority = idx
            i.add_artist(self._skyl_label_id, label_text)
            self.add_artist(label_text)
            self._positions.append((i, i.copy()))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _plot_labels(self):
        """Add the labels to the Axes.
        """
        for i in self._label_text:
            self.add_artist(i)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def plot_skyline(self, *args, **kwargs):
        """Initialise the plotting (called from base class)
        """
        self._sky_create_artists()
        align.Align1D(self, self._skyl_labels,
                      artist_name=self._skyl_label_id)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ChrLabels(axes.BaseGenomicAxes):
    """A specialist chromosome label axes.

    This is not an Axis object but rather an Axes with text labels placed
    according to the coords object.

    Parameters
    ----------
    figure : `matplotlib.Figure` or `skyline.GenomicFigure`
        The figure that the Axes will be associated with.
    track : `matplotlib.gridspec.SubplotSpec`
        The track grid coordinates that the Axes should be added to. These are
        obtained from a `matplotlib.GridSpec` subset.
    coords : `skyline.coords.ChrCoords`
        An object inheriting from `skyline.coords.ChrCoords`
    *args
        Other positional arguments to `matplotlib.axes.Axes`
    rows : `int`, optional, default: 1
        The number of rows to split the labels over. This will stagger the
        labels and can be useful when space is limited and you want a decent
        fontsize.
    label_style : `NoneType` or `dict` or (`list` of `dict`), default: `NoneType`
        Formatting arguments for the labels, these are applied directly to the
        `matplotlib.text.Text` object. If it is `NoneType` then the default
        styling is used. If it is a `list` of `dict` it is styling that is
        applied cyclically to individual labels. If it is a `dict`, then the
        keys should be ``chr_name`` and the values should be a `dict` of
        styling parameters. In the event of a `dict`, the styling parameters
        for the other chromosomes not present in the `dict` are taken from the
        default style.
    verticalalignment : `str`, optional, default: `center`
        The alignment of the chromosome labels on the track, either ``top``,
        ``center`` or ``bottom``. This is the alignment of the block of labels
        not individual text components.
    **kwargs
        Other keyword arguments to `matplotlib.axes.Axes`

    Notes
    -----
    This Axes has a ylim of (0, 1). The labels are plotted according to the
    genomic position in the x axis and the Axes coordinates on the y-axis.
    """

    _DEFAULT_TEST_TEXT = "%$|WMOR2"
    """Some test text to use to re-size the height of text to fit the Axes.
    (`str`)
    """
    CHR_LABEL_STYLE = axes.BaseGenomicAxes.STYLE_CLASS(
        rotation=0,
        fontsize=4,
        ha='center'
    )
    """The default style the chromosome labels. The ``dict`` keys should be
    appropriate for ``matplotlib.Text`` (`dict`)
    """
    CHR_LABEL_KEY = 'chr_label_style'
    """The key name for chromsome label style in the main style dictionary.
    Do not change this value unless you know what you are doing (`str`)
    """
    STYLE = axes.BaseGenomicAxes.STYLE.copy()
    """All the default styles used by the ``skyline.tracks.GeneTrack``. The
    keys should be ``GENE_LABEL_STYLE``, ``GENE_ROW_STYLE``,
    ``GENE_TRACE_STYLE``, ``GENE_PATCH_STYLE``. Please remember this is a class
    variable and changes here will be used in all classes. If using as a
    template, to avoid this place make a deep copy (`dict`)
    """
    # Update the defaults for the main style dictionary
    STYLE[CHR_LABEL_KEY] = CHR_LABEL_STYLE

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coords, *args, rows=1,
                 chr_label_style=None, verticalalignment='center', **kwargs):
        # Make sure that rows is defined properly
        self._skyline_rows = max(1, rows)
        self._skyline_chromosome_format = self.get_skyline_style_kwargs(
            chr_label_style, self.CHR_LABEL_STYLE
        )
        self._skyline_verticalalignment = verticalalignment

        if isinstance(self._skyline_chromosome_format, dict):
            self._skyline_chromosome_format = [self._skyline_chromosome_format]
        super().__init__(figure, track, coords, *args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_row_vertical_coords(self):
        """Get the row coordinates of text rows such that if there are multiple
        rows the text will not overlap and will be aligned as a block according
        to what the user wants.

        Returns
        -------
        row_starts : `list` of `float`
            The start positions of the text in each row.
        text_alignment : `str`
            The alignment of the text box label, either ``center``, ``top``,
            ``bottom``
        """
        # get the widths and height of some test text. In this instance we only
        # need the maximum text height
        min_width, min_height, max_width, max_height = \
            self.get_chr_axis_text_size()

        # I am increasing the text height by a small margin just to make sure
        # there is some separation.
        # TODO: I may have to review adding a constant to the max height.
        #  text gap parameter?
        max_height += 0.07

        # Will store the start positions of the text in each row
        row_starts = []

        # These variables will be updated when we evaluate the alignment
        va = None
        row_start = None
        offset = 1

        if self._skyline_verticalalignment == "center":
            # Align to the center for single row management
            va = "center"

            # For a single row this will evaluate to 0.5
            row_start = 0.5 + (max_height * (self._skyline_rows - 1) / 2)
        elif self._skyline_verticalalignment == "bottom":
            va = "bottom"
            row_start = 0 + (max_height * (self._skyline_rows - 1))
        elif self._skyline_verticalalignment == "top":
            va = "top"
            row_start = 1
        else:
            raise ValueError(
                "unknown vertical alignment: '{0}'".format(
                    self._skyline_verticalalignment
                )
            )

        # Now put together the offsets for the number of rows we want
        row_starts = [row_start]
        for i in range(self._skyline_rows - 1):
            row_starts.append(row_starts[-1] - (max_height*offset))

        # If we are alignment to the bottom make sure the first chromosome is
        # the lowest, otherwise for the center and top it is the highest
        # (in vertical orientation)
        if self._skyline_verticalalignment == "bottom":
            return reversed(row_starts), va

        return row_starts, va

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_chr_axis_text_size(self):
        """This gets the maximum text size for the test text based on the
        chromosome styling attributes given to the Axes.

        Parameters
        ----------
        text : `NoneType` or `str`, optional, default: `NoneType`
            The text to size, if not provided then the default text is used
            which is currently a the string ``%$|WMOR``.

        Returns
        -------
        max_width : `float`
            The max_width of the text based on all the styling attributes
            used in the labelling Axes.
        max_height : `float`
            The max height of the text based on all the styling attributes
            used in the labelling Axes.

        Raises
        ------
        AttributeError
            If the canvas has no ``get_renderer()`` method. If this is the
            case please make sure you set your back end correctly. i.e.
            ``fig.set_canvas(backend_agg.FigureCanvas(figure=fig))`` for the
            file based back end.
        """
        return self.__class__.size_skyline_text(
            self, self._skyline_chromosome_format[0],
            self._skyline_chromosome_format,
            transform=self.transAxes,
            text=self._DEFAULT_TEST_TEXT,
            renderer=self.get_figure().canvas.get_renderer(),
            fontweight='bold'
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def plot_skyline(self, *args, **kwargs):
        """Now actually add the Artists to the Axes.

        Parameters
        ----------
        *args
            Any positional arguments to plot_skyline (currently there are none)
        **kwargs
            Any keyword arguments to plot_skyline (currently there are none)
        """
        row_starts, vert_align = self.get_row_vertical_coords()
        row_starts = cycle(row_starts)

        for chr_name, chr_style in \
            axes.BaseGenomicAxes.yield_skyline_chromosome_format(
                self._skyline_chromosome_format[0],
                self._skyline_coords,
                self._skyline_chromosome_format):
            center = self._skyline_coords.get_center(chr_name)

            chr_style['va'] = vert_align
            try:
                # Remove any parameters that the user might have specified
                del chr_style['verticalalignment']
            except KeyError:
                pass

            self.text(
                center,
                next(row_starts),
                chr_name,
                **chr_style
            )
