"""Patches for genes/transcripts
"""
from skyline import axes
from matplotlib import patches
from matplotlib.path import Path
# from matplotlib import textpath
# import numpy as np
# import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BlockGene(patches.PathPatch):
    """A simple representation of a gene. Only plots according to the
    boundaries of the gene, i.e. first exon start, last exon end.

    Parameters
    ----------
    gene : `skyline.features.SimpleTranscript`
        A transcript feature object can also be
        `skyline.features.ExonTranscript` but more generally, as long as the
        feature has an exon attribute that provides a list of features with
        ``start_pos``/``end_pos`` then it should work.
    *args
        Any arguments passed on to ``matplotlib.patches.PathPatch``.
    bottom : `float`, optional, default: `0`
        The bottom (y) coordinate for the gene/transcript.
    top : `float`, optional, default: `1`
        The top (y) coordinate for the gene/transcript.
    gene_name : `str`, optional, default: `NoneType`
        The name of the gene. This is only needed for reference and is not used
        in any plotting. The default will be a sequential class counter
        concatenated with ``GENE``.
    style : `dict`, optional, default: `NoneType`
        The style for the patch, the keys/values should be appropriate for
        ``matplotlib.patches.PathPatch``. The default will be the style defined
        in the ``GENE_PATCH_STYLE`` class variable.
    point_breaks : `list` of `int`
        Cut points to define a directional point onto the BlockGene patch. So
        if the gene is greater than these then different gradient of points
        will be applied to the patch.
    **kwargs
        Any keyword arguments applied to the ``matplotlib.patches.PathPatch``.

    Notes
    -----
    The block gene represents genes as rectangles with points on them, where
    the points face rightwards if the gene is on the forward strand or
    leftwards if the gene is on the reverse strand. If the gene is too small
    (i.e. the length is less than the left of the point section) then the no
    point is added and the gene is represented by a single rectangle.

    See also
    --------
    matplotlib.patches.PathPatch
    skyline.features.SimpleTranscript
    skyline.features.ExonTranscript
    """
    _GENE_NO = 1
    """Incremented each time a BlockGene patch (or subclass) is instantiated
    and is used in the generation of the default label.
    """
    GENE_PATCH_STYLE = axes.BaseGenomicAxes.STYLE_CLASS(
        facecolor="#6666B1",
        edgecolor="#6666B1",
        linewidth=0.3
    )
    """The default style for the gene patch. Each gene is effectively a
    plotting symbol on the gene track the dict keys should be appropriate
    keyword arguments for ``matplotlib.patches.PathPatch`` (`dict`).
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, gene, *args, bottom=0, top=1,
                 gene_name=None, style=None, point_breaks=[1000],
                 **kwargs):
        self._skyline_coords = gene
        self._skyline_bottom = bottom
        self._skyline_top = top
        self._skyline_strand = gene.strand

        # Set the default gene name if none has been supplied, this is not used
        # for any labelling but just for reference
        self._skyline_gene_name = gene_name or self._get_default_name()

        # Set the default name
        self._skyline_style = axes.BaseGenomicAxes.get_skyline_style_kwargs(
            style, self.GENE_PATCH_STYLE
        )
        self._skyline_point_breaks = point_breaks

        self._skyline_point_length = 0
        self._skyline_start = None
        self._skyline_end = None

        self._set_gene_bounds()
        self._set_gene_point_length()
        kwargs = self.set_skyline_style_args(kwargs)
        super().__init__(self.get_gene_path(), *args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_skyline_style_args(self, kwargs):
        """Merge any keyword args with any style arguments with style arguments
        taking priority over keyword arguments
        """
        return {**kwargs, **self._skyline_style}

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_default_name(self):
        """Get the default gene name. This is an incremental count of a class
        level counter.

        Returns
        -------
        gene_name : `str`
            A label to attach to the object.
        """
        gene_name = "GENE{0}".format(self.__class__._GENE_NO)
        self.__class__._GENE_NO += 1
        return gene_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _set_gene_bounds(self):
        """Set the boundaries of the gene based on the exon coordinates that
        were passed.
        """
        self._skyline_start = self._skyline_coords.plot_start_pos
        self._skyline_end = self._skyline_coords.plot_end_pos

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _set_gene_point_length(self):
        """Set the point length for the gene based on the gene length and the
        point break points.
        """
        self._skyline_point_breaks.sort(reverse=True)
        length = self._skyline_end - self._skyline_start
        for break_point in self._skyline_point_breaks:
            if length >= break_point:
                self._skyline_point_length = break_point
                break

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_gene_path(self):
        """Get a Path object representing the gene.

        Returns
        -------
        path : `matplotlib.path.Path`
            A path representing the outline of the gene.
        """
        if self._skyline_point_length == 0:
            # fall back to a rectangle
            return self._get_rect_path()
        else:
            return self._get_gene_path()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_gene_path(self):
        """Produce a pointed path for the gene, this is strand aware.

        Returns
        -------
        path : `matplotlib.path.Path`
            A path representing the outline of the gene.
        """
        start = self._skyline_start
        end = self._skyline_end
        bottom = self._skyline_bottom
        top = self._skyline_top
        point_start = self._skyline_end - self._skyline_point_length
        point_middle = self._skyline_bottom + abs(
            self._skyline_bottom - self._skyline_top
        ) / 2

        # There must be a more intelligent way of doing this
        if self._skyline_strand == -1:
            start = self._skyline_end
            end = self._skyline_start
            point_start = self._skyline_start + self._skyline_point_length

        verts = [
            (start, bottom),  # left, bottom
            (start, top),  # left, top
            (point_start, top),  # right, top
            (end, point_middle),  # right, top
            (point_start, bottom),  # right, bottom
            (0., 0.),  # ignored
        ]
        codes = [
            Path.MOVETO,
            Path.LINETO,
            Path.LINETO,
            Path.LINETO,
            Path.LINETO,
            Path.CLOSEPOLY,
        ]
        return Path(verts, codes)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _get_rect_path(self):
        """Get a rectangle representation of a gene.

        Returns
        -------
        path : `matplotlib.path.Path`
            A path representing the outline of the gene.
        """
        verts = [
            (self._skyline_start, self._skyline_bottom),  # left, bottom
            (self._skyline_start, self._skyline_top),  # left, top
            (self._skyline_end, self._skyline_top),  # right, top
            (self._skyline_end, self._skyline_bottom),  # right, bottom
            (0., 0.),  # ignored
        ]

        codes = [
            Path.MOVETO,
            Path.LINETO,
            Path.LINETO,
            Path.LINETO,
            Path.CLOSEPOLY,
        ]
        return Path(verts, codes)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ExonGene(BlockGene):
    """A more complex representation of a gene/transcript. Will use blocks to
    represent exons with connecting lines to represent introns.

    Parameters
    ----------
    *args
        Any arguments passed on to ``skyline.patches.genes.BlockGene``.
    **kwargs
        Any keyword arguments applied to the
        ``skyline.patches.genes.BlockGene``.

    See also
    --------
    matplotlib.patches.PathPatch
    skyline.features.SimpleTranscript
    skyline.features.ExonTranscript
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_gene_path(self):
        """Get a Path object representing the gene

        Returns
        -------
        path : `matplotlib.path.Path`
            A path representing the outline of the gene.
        """
        start = self._skyline_start
        end = self._skyline_end
        bottom = self._skyline_bottom
        top = self._skyline_top
        patch_center = top + (bottom - top)/2
        verts = []
        codes = []
        exons = self._skyline_coords.exons
        for i in range(len(exons)):
            e1 = exons[i]
            start, end = e1.start_pos, e1.end_pos
            verts.extend(
                [
                    (start, bottom),  # left, bottom
                    (start, top),  # left, top
                    (end, top),  # right, top
                    (end, bottom),
                    (0., 0.)  # ignored
                ]
            )
            codes.extend(
                [
                    Path.MOVETO,
                    Path.LINETO,
                    Path.LINETO,
                    Path.LINETO,
                    Path.CLOSEPOLY
                ]
            )
            try:
                e2 = exons[i+1]
                # next_start, next_end = e2.start_pos, e2.end_pos
                next_start = e2.start_pos
                # next_start, next_end, next_coding = self._skyline_coords[i+1]
                verts.extend(
                    [
                        (end, patch_center),
                        (next_start, patch_center)
                    ]
                )
                codes.extend(
                    [
                        Path.MOVETO,
                        Path.LINETO
                    ]
                )
            except IndexError:
                pass
        return Path(verts, codes)
