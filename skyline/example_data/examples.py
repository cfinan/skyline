"""Provides centralised access to example data sets that can be used in tests
and also in example code and/or jupyter notebooks.

The `skyline.example_data.examples` module is very simple, this is
not really designed for editing via end users but they should call the two
public methods functions, `skyline.example_data.examples.get_data()`,
`skyline.example_data.examples.help()`.

Notes
-----
Data can be "added" either through functions that generate the data on the fly
or via functions that load the data from a static file located in the
``example_data`` directory. The data files being added  should be as small as
possible (i.e. kilobyte/megabyte range). The dataset functions should be
decorated with the ``@dataset`` decorator, so the example module knows about
them. If the function is loading a dataset from a file in the package, it
should look for the path in ``_ROOT_DATASETS_DIR``.

Examples
--------

Registering a function as a dataset providing function:

>>> @dataset
>>> def dummy_data(*args, **kwargs):
>>>     \"\"\"A dummy dataset function that returns a small list.
>>>
>>>     Returns
>>>     -------
>>>     data : `list`
>>>         A list of length 3 with ``['A', 'B', 'C']``
>>>
>>>     Notes
>>>     -----
>>>     This function is called ``dummy_data`` and has been decorated with a
>>>     ``@dataset`` decorator which makes it available with the
>>>     `example_data.get_data(<NAME>)` function and also
>>>     `example_data.help(<NAME>)` functions.
>>>     \"\"\"
>>>     return ['A', 'B', 'C']

The dataset can then be used as follows:

>>> from skyline.example_data import examples
>>> examples.get_data('dummy_data')
>>> ['A', 'B', 'C']

A dataset function that loads a dataset from file, these functions should load
 from the ``_ROOT_DATASETS_DIR``:

>>> @dataset
>>> def dummy_load_data(*args, **kwargs):
>>>     \"\"\"A dummy dataset function that loads a string from a file.
>>>
>>>     Returns
>>>     -------
>>>     str_data : `str`
>>>         A string of data loaded from an example data file.
>>>
>>>     Notes
>>>     -----
>>>     This function is called ``dummy_data`` and has been decorated with a
>>>     ``@dataset`` decorator which makes it available with the
>>>     `example_data.get_data(<NAME>)` function and also
>>>     `example_data.help(<NAME>)` functions. The path to this dataset is
>>>     built from ``_ROOT_DATASETS_DIR``.
>>>     \"\"\"
>>>     load_path = os.path.join(_ROOT_DATASETS_DIR, "string_data.txt")
>>>     with open(load_path) as data_file:
>>>         return data_file.read().strip()

The dataset can then be used as follows:

>>> from skyline.example_data import examples
>>> examples.get_data('dummy_load_data')
>>> 'an example data string'
"""
import os
import re
import wget
import pandas as pd


# The name of the example datasets directory
_EXAMPLE_DATASETS = "example_datasets"
"""The example dataset directory name (`str`)
"""

_ROOT_DATASETS_DIR = os.path.join(os.path.dirname(__file__), _EXAMPLE_DATASETS)
"""The root path to the dataset files that are available (`str`)
"""

_DATASETS = dict()
"""This will hold the registered dataset functions (`dict`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def dataset(func):
    """Register a dataset generating function. This function should be used as
    a decorator.

    Parameters
    ----------
    func : `function`
        The function to register as a dataset. It is registered as under the
        function name.

    Returns
    -------
    func : `function`
        The function that has been registered.

    Raises
    ------
    KeyError
        If a function of the same name has already been registered.

    Notes
    -----
    The dataset function should accept ``*args`` and ``**kwargs`` and should be
    decorated with the ``@dataset`` decorator.

    Examples
    --------
    Create a dataset function that returns a dictionary.

    >>> @dataset
    >>> def get_dict(*args, **kwargs):
    >>>     \"\"\"A dictionary to test or use as an example.
    >>>
    >>>     Returns
    >>>     -------
    >>>     test_dict : `dict`
    >>>         A small dictionary of string keys and numeric values
    >>>     \"\"\"
    >>>     return {'A': 1, 'B': 2, 'C': 3}
    >>>

    The dataset can then be used as follows:

    >>> from skyline.example_data import examples
    >>> examples.get_data('get_dict')
    >>> {'A': 1, 'B': 2, 'C': 3}

    """
    try:
        _DATASETS[func.__name__]
        raise KeyError("function already registered")
    except KeyError:
        pass

    _DATASETS[func.__name__] = func
    return func


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_data(name, *args, **kwargs):
    """Central point to get the datasets.

    Parameters
    ----------
    name : `str`
        A name for the dataset that should correspond to a registered
        dataset function.
    *args
        Arguments to the data generating functions
    **kwargs
        Keyword arguments to the data generating functions

    Returns
    -------
    dataset : `Any`
        The requested datasets
    """
    try:
        return _DATASETS[name](*args, **kwargs)
    except KeyError as e:
        raise KeyError("dataset not available: {0}".format(name)) from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def list_datasets():
    """List all the registered datasets.

    Returns
    -------
    datasets : `list` of `tuple`
        The registered datasets. Element [0] for each tuple is the dataset name
        and element [1] is a short description captured from the docstring.
    """
    datasets = []
    for d in _DATASETS.keys():
        desc = re.sub(
            r'(Parameters|Returns).*$', '', _DATASETS[d].__doc__.replace(
                '\n', ' '
            )
        ).strip()
        datasets.append((d, desc))
    return datasets


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def help(name):
    """Central point to get help for the datasets.

    Parameters
    ----------
    name : `str`
        A name for the dataset that should correspond to a unique key in the
        DATASETS module level dictionary.

    Returns
    -------
    help : `str`
        The docstring for the function
    """
    docs = ["Dataset: {0}\n{1}\n\n".format(name, "-" * (len(name) + 9))]
    try:
        docs.extend(
            ["{0}\n".format(re.sub(r"^\s{4}", "", i))
             for i in _DATASETS[name].__doc__.split("\n")]
        )
        return "".join(docs)
    except KeyError as e:
        raise KeyError("dataset not available: {0}".format(name)) from e


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def test_gwas(*args, **kwargs):
    """A test gwas file, if not present it will be downloaded.

    Returns
    -------
    gwas_path : `str`
        A path to the test GWAS
    """
    url = (
        'http://csg.sph.umich.edu/abecasis/public/lipids2013/'
        'jointGwasMc_LDL.txt.gz'
    )
    gwas_path = os.path.join(_ROOT_DATASETS_DIR, "test_gwas.txt.gz")
    try:
        open(gwas_path).close()
    except FileNotFoundError:
        wget.download(url, gwas_path)
    return gwas_path


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def open_test_gwas(*args, **kwargs):
    """Open test gwas file, if not present it will be downloaded.

    Returns
    -------
    gwas_data : `pandas.DataFrame`
        Test GWAS data.
    """
    test_gwas_path = test_gwas()
    data = pd.read_csv(test_gwas_path, delimiter="\t")
    data[['chr_name', 'start_pos']] = \
        data['SNP_hg19'].str.split(':', expand=True)
    data['start_pos'] = data['start_pos'].astype(int)
    data['chr_name'] = data['chr_name'].replace(r'^chr', '', regex=True)
    return data


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def test_labels(*args, **kwargs):
    """Some test gene labels to go with the downscaled_gwas

    Returns
    -------
    test_labels : `pandas.DataFrame`
        Test labels
    """
    labels_path = os.path.join(_ROOT_DATASETS_DIR, "test_nearest_genes.txt.gz")
    return pd.read_csv(
        labels_path, compression='gzip', delimiter="\t",
        dtype={'chr_name': str, 'gene_chr_name': str}
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def reference_lengths(*args, **kwargs):
    """Chromosome lengths from human reference genomes. This is actually used by
    the `skyline.coords` module.

    Returns
    -------
    reference_lengths : `str`
        A path to a config ``.ini`` file with reference chromosome lengths
    """
    return os.path.join(_ROOT_DATASETS_DIR, "reference_lengths.txt")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def downscaled_gwas(*args, **kwargs):
    """A test gwas file, that has been downscaled and is useful for example plots.

    Returns
    -------
    downscale_gwas : `pd.DataFrame`
        Downscaled GWAS data
    """
    return pd.read_csv(
        os.path.join(_ROOT_DATASETS_DIR, "downscale_gwas.txt.gz"),
        delimiter="\t", dtype={'chr_name': str}
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def paired_gwas_paths(*args, **kwargs):
    """File paths to hypothetical GWAS data representing six traits stratified
    by sex.

    Returns
    -------
    paired_gwas_paths : `list` of `str`
        Paths to downscaled hypothetical GWAS data
    """
    files = [
        'trait1_females.down.txt.gz',
        'trait1_males.down.txt.gz',
        'trait2_females.down.txt.gz',
        'trait2_males.down.txt.gz',
        'trait3_females.down.txt.gz',
        'trait3_males.down.txt.gz',
        'trait4_females.down.txt.gz',
        'trait4_males.down.txt.gz',
        'trait5_females.down.txt.gz',
        'trait5_males.down.txt.gz',
        'trait6_females.down.txt.gz',
        'trait6_males.down.txt.gz'
    ]
    return [
        os.path.join(_ROOT_DATASETS_DIR, "paired_data/{0}".format(i))
        for i in files
    ]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def paired_gwas_labels(*args, **kwargs):
    """Trait labels for the paired GWAS data.

    Returns
    -------
    paired_gwas_labels : `list` of `str`
        Labels for the hypothetical paired GWAS data
    """
    labels = []
    for i in paired_gwas_paths():
        matches = re.search(r'(trait)(\d+)_(females|males)', i)
        labels.append(
            "{0} {1} - {2}".format(
                matches.group(1).capitalize(),
                matches.group(2),
                matches.group(3).capitalize()
            )
        )
    return labels


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def paired_gwas(*args, **kwargs):
    """``pandas.DataFrames`` of hypothetical GWAS data representing six traits
    stratified by sex.

    Returns
    -------
    downscale_gwas : `pd.DataFrame`
        Downscaled GWAS data
    """
    gwas_files = paired_gwas_paths()

    gwas_data = []
    for i in gwas_files:
        data = pd.read_csv(i, delimiter="\t", dtype={'chr_name': str})
        data.loc[data['chr_name'] == '23', 'chr_name'] = 'X'
        gwas_data.append(
            data
        )
    return gwas_data


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@dataset
def ldl_locus_view(*args, **kwargs):
    """``pandas.DataFrame`` of LDL gwas data for chromosome 1 from GLGC.

    Returns
    -------
    ldl_gwas : `pd.DataFrame`
        LDL GWAS data for chromosome 1.
    """
    lz_file = os.path.join(
        _ROOT_DATASETS_DIR, "locus_view", "locus_view_ldl_chr1.txt.gz"
    )
    return pd.read_csv(lz_file, delimiter="\t", dtype=dict(chr_name=str))
