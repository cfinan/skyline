"""Base Axes class.
"""
from matplotlib import axes, rcParams
import matplotlib.transforms as transforms
from skyline import (
    utils,
    styles,
    coords
)
from itertools import cycle
import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BaseGenomicAxes(axes.Axes):
    """The base Axes class. Do not use directly.

    Parameters
    ----------
    figure : `matplotlib.Figure` or `skyline.GenomicFigure`
        The figure that the Axes will be associated with.
    track : `matplotlib.gridspec.SubplotSpec`
        The track grid coordinates that the Axes should be added to. These are
        obtained from a `matplotlib.GridSpec` subset.
    coord_mapper : `skyline.coords.PassthroughCoords`
        An object inheriting from `skyline.coords.PassthroughCoords`
    xpad_left : `int` or `float`, optional, default: `0`
        Padding to add to the left bound xaxis limit. If it is a float it is
        treated as a proportion, if it is an integer it is treated as
        basepairs.
    xpad_right : `int` or `float`, optional, default: `0`
        Padding to add to the right bound xaxis limit. If it is a float it is
        treated as a proportion, if it is an integer it is treated as
        basepairs.
    style : `Any`, optional, default: `NoneType`
        Any objects that carrying styling information used in the Axes.
    do_plot : `bool`, optional, default: `True`
        Should the ``BaseGenomicAxes.plot_skyline`` method be called when the
        Axes is initialised. This gives sub-classes the option of delaying the
        building of the Axes components and calling
        ``BaseGenomicAxes.plot_skyline`` directly.
    add_coords_to : `list` of `skyline.coords.FeatureCoords`
        Features relevant to the Axes that need to have a coordinate mapper
        associated with them for correct plotting.
    error_on_existing_coords_mapper : `bool`, optional, default: `False`
        If any of the `skyline.coords.FeatureCoords` objects in
        ``add_coords_to`` already has a coordinate object associated with it,
        then raise an ``AttributeError`` (if ``True``), otherwise ignore it
        (if ``False``).
    *args
        Arguments to ``matplotlib.Axes``.
    **kwargs
        Keyword arguments to ``matplotlib.Axes``.

    Notes
    -----
    This implements some common features of Axes that are used to implement the
    various skyline Axes used to plot tracks. It inherits from
    `matplotlib.Axes`.
    """
    _AXES_COUNT = 0
    _DEFAULT_TEST_TEXT = "%$|WMOR"
    _DEFAULT_KWARGS = dict()

    _STYLE_CLASS_DICT = dict
    """Default styling parameters will be given as dictionary objects.
    """
    _STYLE_CLASS_SKYLINE = styles.SkylineStyle
    """Default styling parameters will be given as
    ``skyline.styles.SkylineStyle`` objects. These behave like dicts but also
    have attributes and run time inheritance from parent
    ``skyline.styles.SkylineStyle`` objects.
    """
    STYLE_CLASS = _STYLE_CLASS_DICT
    """The class to use for return default styles.
    """
    STYLE = STYLE_CLASS()
    """The default style that will be populated by subclasses
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, figure, track, coord_mapper, xpad_left=0, xpad_right=0,
                 style=None, do_plot=True, add_coords_to=None,
                 error_on_existing_coords_mapper=False, *args, **kwargs):
        # Overwrite any default kwargs with the user specified ones
        kwargs = {**self.__class__._DEFAULT_KWARGS, **kwargs}

        # Check the class of the coordinate feature is ok
        if not issubclass(coord_mapper.__class__, coords.PassthroughCoords):
            raise TypeError(
                "coordinates object should be a subclass of "
                "skyline.coords.PassthroughCoords"
            )

        if add_coords_to is not None:
            self.add_coords_to_features(
                add_coords_to, coord_mapper,
                error_on_existing_coords_mapper=error_on_existing_coords_mapper
            )

        self.set_skyline_label(kwargs)

        # Perform any required data pre-processing
        self.process_skyline_data()

        # We get the bounding box for the grid spec coordinates and pass them
        # directly to the super class
        super().__init__(
            figure, track.get_position(figure), *args, **kwargs
        )
        self._skyline_coords = coord_mapper

        # If the style is not set then we default to the null style
        self._skyline_style = style or styles.DEFAULT_STYLE

        # Sets the axes xlimit padding and will also set the xlimit
        self.set_skyline_xpad(xpad_left, xpad_right)

        # Set up the plotting area
        self.set_skyline_plot_area()

        if do_plot is True:
            self.plot_skyline(*args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def add_coords_to_features(features, coords_mapper,
                               error_on_existing_coords_mapper=False):
        """Add the coordinates mapper to the features.

        Parameters
        ----------
        features : `list` of `skyline.coords.FeatureCoords`
            A feature coordinates object to add the mapper to.
        coords_mapper : `skyline.coords.PassthroughCoords`
            An object inheriting from `skyline.coords.PassthroughCoords` to
            add to the features.
        error_on_existing_coords_mapper : `bool`, optional, default: `False`
            Raise an error if the feature already has a coordinate mapper
            associated with it.
        """
        # Need to make sure that no coords object is associated
        for i in features:
            try:
                i.add_coords_mapper(coords_mapper)
            except AttributeError as e:
                if e.args[0].startswith("coords mapper already set"):
                    # Correct error and want to raise
                    if error_on_existing_coords_mapper is True:
                        raise
                else:
                    # Not the correct error
                    raise

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def get_skyline_style_kwargs(user_style, default_style):
        """A helper method for merging user defined styles into default
        styles.

        Parameters
        ----------
        user_style : `dict` or `NoneType`
            The user supplied styling dictionary, if the user has supplied no
            styling dict then this will be NoneType and the returned style
            will be based on a copy of the defaults. If they have then the user
            defined style will be used to overide the defaults where there are
            common keys.
        default_style : `dict`
            The default styling parameters.

        Returns
        -------
        merged_style : `dict`
            Merged user/default styling keyword arguments.

        Raises
        ------
        TypeError
            If the user styles and/or the default style are not ``dict`` and if
            the user style is not ``NoneType``.
        """
        if user_style is None:
            user_style = dict()

        if not isinstance(user_style, dict) or \
           not isinstance(default_style, dict):
            raise TypeError(
                "user/default style must be a dict not '{0}|{1}'".format(
                    type(user_style), type(default_style)
                )
            )

        return {**default_style, **user_style}

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def get_skyline_text_bbox(axes, text, text_style=None, transform=None,
                              renderer=None, x=0, y=0):
        """Get the bounding box surrounding the text based on the text styling
        parameters.
        """
        renderer = renderer or axes.get_figure().canvas.get_renderer()

        # We have to plot the text on the artist to get the bounding box
        text = axes.text(x, y, text, **text_style, transform=transform)
        bb = text.get_window_extent(renderer)
        text.remove()
        return bb

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def get_skyline_fontsize_limits(styles, default_style=None):
        """Get the minimum and maximum fontsize from a list of styles.

        Parameters
        ----------
        styles : `list` of `dict`
            The styles to retrieve the fontsize from
        default_style : `NoneType` or `dict`
            The default style to use if any of the defined styles does not have
            the fontsize set. If this is `NoneType` or the default_style does
            not have the fontsize defined then we default to
            ``rcParams['font.size']``.

        Returns
        -------
        min_fontsize : (`float` or `int`)
            The minimum fontsize defined in the Axes.
        max_fontsize : (`float` or `int`)
            The maximum fontsize defined in the Axes.
        """
        fs = 'fontsize'
        try:
            # Attempt to get the default fontsize from the default style
            default_font = default_style[fs]
        except (AttributeError, KeyError):
            # It is not defined in the default style so fall back to the
            # matplotlib default
            default_font = rcParams['font.size']

        max_font = None
        min_font = None

        # Loop through all the styles that have been given
        for i in styles:
            try:
                # update min and max
                max_font = max(max_font, i[fs])
                min_font = max(min_font, i[fs])
            except KeyError:
                # fontsize not defined in the style
                try:
                    # So update using the default fontsize
                    max_font = max(max_font, default_font)
                    min_font = max(min_font, default_font)
                except TypeError:
                    # min, max not defined yet, so use the default size for
                    # min/max
                    max_font = default_font
                    min_font = default_font
            except TypeError:
                # min/max not defined yet so initialise from style fontsize
                max_font = i[fs]
                min_font = i[fs]

        return min_font, max_font

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def get_skyline_fontfamily(styles, default_style=None):
        """Get a set of fonts used in the Axes.

        Parameters
        ----------
        styles : `list` of `dict`
            The styles to retrieve the fontsize from
        default_style : `NoneType` or `dict`
            The default style to use if any of the defined styles does not have
            the fontfamily set. If this is `NoneType` or the default_style does
            not have the fontfamily defined then we default to
            ``rcParams['font.family']``.

        Returns
        -------
        font_families : `set`
            The unique fontfamily names used in the Axes.
        """
        fs = 'fontfamily'
        try:
            default_font = default_style[fs]
        except (AttributeError, KeyError):
            default_font = rcParams['font.family']

        font_families = set()
        for i in styles:
            try:
                i[fs]
            except KeyError:
                if isinstance(default_font, (list, tuple)):
                    font_families.update(default_font)
                else:
                    font_families.add(default_font)

        return font_families

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def get_skyline_chromosome_format(default_style, coords, style_args):
        """This utility method is for re-formatting a chromosome-wise style
        argument.

        Parameters
        ----------
        default_style : `dict`
            A dictionary of `matplotlib` keyword arguments to use in the event
            that no `style_args` have been defined.
        coords : `skyline.coords.ChrCoords`
            A coordinates object that inherits from `skyline.coords.ChrCoords`
        style_args : `NoneType` or `dict` or (`list` of `dict`)
            Style arguments to standardise into a list that can be used
            cyclically. If ``NoneType`` then a list is returned that has length
            1 and is comprised of the default_style. If a list or tuple then
            this is just returned. However, if it is a  dictionary then it
            is repeated in a list where the length with be the same as the
            number of active chromosomes.

        Returns
        -------
        list_style_args : `list` of `dict`
            The style arguments expanded into a list such that they can be
            looped through cyclically.

        Raises
        ------
        TypeError
            If the ``style_args`` are not `NoneType`, `list` or `dict`.

        Notes
        -----
        Several Skyline Axes use this method to allow users to specify
        individual styles to specific chromosomes, this method takes that
        information and standardises it. So can be used pre-plotting. In
        general, users will not need to worry about this as it is called
        internally from various Axes. However, it may be useful if you are
        sub-classing any Skyline Axes.

        The nature of the styles is not important, this will just ensure that
        the result is a list of required styles that can be used to format
        chromosomes.

        See also
        --------
        skyline.axes.BaseGenomicAxes.yield_skyline_chromosome_format
        """
        if style_args is None:
            return [default_style]

        if isinstance(style_args, (list, tuple)):
            return style_args

        if isinstance(style_args, dict):
            expanded_style_args = []

            for i in coords.get_active_chr_name():
                if i not in style_args:
                    expanded_style_args.append(default_style)
                else:
                    expanded_style_args.append(style_args[i])
            return expanded_style_args

        raise TypeError(
            "unknown style arguments: {0}".format(type(style_args))
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def yield_skyline_chromosome_format(default_style, coords, style_args):
        """This utility method is for yielding chromsome names and corresponing
        styles as specified by the user.

        Parameters
        ----------
        default_style : `dict`
            A dictionary of `matplotlib` keyword arguments to use in the event
            that no ``style_args`` have been defined.
        coords : `skyline.coords.ChrCoords`
            A coordinates object that inherits from `skyline.coords.ChrCoords`.
        style_args : `NoneType` or `dict` or (`list` of `dict`)
            Style arguments to standardise into a list that can be used
            cyclically.

        Yields
        ------
        chr_name : `str`
            The name of the chromosome.
        style : `dict`
            A dictionary of matplotlib kwargs that will be used to format the
            style of the chromosome represented by ``chr_name`` in some way.

        Raises
        ------
        TypeError
            If the `style_args` are not `NoneType`, `list` or `dict`.

        Notes
        -----
        Several Skyline Axes use this method to yield the chromosomes and their
        style in the correct order according to how they are specified in the
        coordinates object and the user specific styling arguments. In
        general, users will not need to worry about this as it is called
        internally from various Axes. However, it may be useful if you are
        sub-classing any Skyline Axes.

        See also
        --------
        skyline.axes.BaseGenomicAxes.get_skyline_chromosome_format
        """
        style_list = BaseGenomicAxes.get_skyline_chromosome_format(
            default_style, coords, style_args
        )
        style_cycle = cycle(style_list)

        for chr_name in coords.get_active_chr_name():
            yield chr_name, next(style_cycle)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def set_skyline_kwargs_ylim(kwargs, data, data_column, min_value=0,
                                max_buffer=0):
        """Determine if the y-limit value needs to be set from the data or if
        it has been set by the user in kwargs.

        Parameters
        ----------
        kwargs : `dict`
            Keyword arguments that have been supplied to the axes by the user.
        data : `pandas.DataFrame`
            Data that has been supplied by the user
        data_column : `str`
            The name of the data column to derive the ylimit values from.
        min_value : `int` or `float`, optional, default: 0
            The minimum value to use for the ylimit should the data not have
            values as low as this.

        Returns
        -------
        kwargs : `dict`
            The same dictionary that was supplied but with a guaranteed
            ``ylim`` argument added. The return is not strictly necessary as
            the addition happens inplace.

        Notes
        -----
        This will typically be called by a sub-class of
        `skyline.axes.BaseGenomicAxes` to set the ``ylim`` prior to calling the
        super class. If the ylim value has been set but is ``NoneType``, then
        it will be removed as this causes matplotlib to not set the ylim
        correctly from the data.
        """
        ylim_arg = 'ylim'
        max_value = data[data_column].max()
        max_value += (max_value * max_buffer)

        try:
            # Is it presnt
            ylim_val = kwargs[ylim_arg]

            # Yes, but if it is NoneType, then remove
            if ylim_val is None:
                del kwargs[ylim_arg]
        except KeyError:
            # No, set with some padding
            kwargs[ylim_arg] = (
                min(min_value, data[data_column].min()),
                max_value
            )
        return kwargs

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def use_skyline_style(cls, use):
        """Return all default styles as `skyline.styles.SkylineStyle` objects.
        If toggles off then `dict` will be used instead. Note this will effect
        all classes inheriting from this base class
        """
        if use is True:
            cls.STYLE_CLASS = cls._STYLE_CLASS_SKYLINE
        elif use is False:
            cls.STYLE_CLASS = cls._STYLE_CLASS_DICT
        else:
            raise ValueError(f"unknown use value: {use}")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def size_skyline_text(cls, axes, default_style, styles, transform=None,
                          text=None, renderer=None, fontweight='bold'):
        """This gets the maximum text size for the test text based on the
        styling attributes given to the Axes.

        Parameters
        ----------
        text : `NoneType` or `str`, optional, default: `NoneType`
            The text to size, if not provided then the default text is used
            which is currently a the string ``%$|WMOR``.

        Returns
        -------
        min_width : `float`
            The max_width of the text based on all the styling attributes
            used in the labelling Axes.
        min_height : `float`
            The max height of the text based on all the styling attributes
            used in the labelling Axes.
        max_width : `float`
            The max_width of the text based on all the styling attributes
            used in the labelling Axes.
        max_height : `float`
            The max height of the text based on all the styling attributes
            used in the labelling Axes.

        Raises
        ------
        AttributeError
            If the canvas has no ``get_renderer()`` method. If this is the
            case please make sure you set your back end correctly. i.e.
            ``fig.set_canvas(backend_agg.FigureCanvas(figure=fig))`` for the
            file based back end.
        """
        fontweight = 'bold'
        transform = transform or axes.transData
        text = text or cls._DEFAULT_TEST_TEXT

        min_font, max_font = cls.get_skyline_fontsize_limits(
            styles, default_style=default_style
        )

        font_families = cls.get_skyline_fontfamily(
            styles, default_style=default_style
        )

        min_width = None
        max_width = None
        min_height = None
        max_height = None
        for i in font_families:
            style = dict(fontfamily=i, fontsize=max_font,
                         fontweight=fontweight)
            bb = cls.get_skyline_text_bbox(
                axes, text, text_style=style, transform=transform,
                renderer=renderer, x=0, y=0
            )

            bb = transform.inverted().transform(bb)
            height = bb[1, 0] - bb[0, 0]
            width = bb[1, 1] - bb[0, 1]
            try:
                min_width = min(min_width, width)
                max_width = max(max_width, width)
                min_height = min(min_height, height)
                max_height = max(max_height, height)
            except TypeError:
                min_width = width
                max_width = width
                min_height = height
                max_height = height

        return min_width, min_height, max_width, max_height

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_skyline_label(self, kwargs):
        """Make a unique label
        """
        if 'label' not in kwargs:
            kwargs['label'] = "SkylineAxes({0})".format(
                self.__class__._AXES_COUNT
            )

        self.__class__._AXES_COUNT += 1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_skyline_xpad(self, xpad_left, xpad_right):
        """Set the left and right padding for the x limits. This will also
        adjust the `Axes.xlim`.

        Parameters
        ----------
        xpad_left : `int` or `float`
            Padding to add to the left bound xaxis limit. If it is a float it
            is treated as a proportion, if it is an integer it is treated as
            basepairs.
        xpad_right : `int` or `float`
            Padding to add to the right bound xaxis limit. If it is a float it
            is treated as a proportion, if it is an integer it is treated as
            basepairs.
        """
        self._xpad_left = utils.prop_to_bp(xpad_left, self._skyline_coords.length)
        self._xpad_right = utils.prop_to_bp(xpad_right, self._skyline_coords.length)

        # Now set the xlim
        self.set_skyline_xlim()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_skyline_xlim(self):
        """Set the axes x-limits based on the coords object and the
        ``xpad_left`` and ``xpad_right`` values.
        """
        xstart = self._skyline_coords.start - self._xpad_left
        xend = self._skyline_coords.end + self._xpad_right
        self.set_xlim(xstart, xend)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def set_skyline_plot_area(self):
        """Setup the skyline Axes plotting area. This sets attributes that are
        common to many of the Axes that inherit from `skyline.BaseGenomicAxes`
        """
        # Turn off the x and y-axis
        self.xaxis.set_visible(False)
        self.yaxis.set_visible(False)

        # Remove all the frame splines
        self.spines['right'].set_visible(False)
        self.spines['top'].set_visible(False)
        self.spines['left'].set_visible(False)
        self.spines['bottom'].set_visible(False)

        # Make the Axes background colour transparent
        self.set_facecolor('none')

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def process_skyline_data(self):
        """A method that is called to perform any data pre-processing steps.

        Notes
        -----
        This should be overridden as this baseclass version does not do
        anything and is there for the interface. This is called upon
        initialisation of the object.
        """
        pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def axvlines_from_linker(self, linkers, top_cap=False, cap_marker=None,
                             bottom_cap=False, to=True, **kwargs):
        """Add vertical lines across the entire Axes plotting area based on
        positions within a label track.

        Parameters
        ----------
        label_track : `skyline.labels.LabelTrack`
            The label track to extract positional information from
        top_cap : `bool`, optional, default; `False`
            Should the lines have top caps.
        bottom_cap : `bool`, optional, default; `False`
            Should the lines have bottom caps.
        cap_marker : `dict` or `NoneType`, optional, default: `NoneType`
            Keyword arguments to `matplotlib.Axes.scatter` except the x, y
            coordinates.
        **kwargs
            Keyword arguments to `matplotlib.Axes.axvline` controlling the
            line style

        Notes
        -----
        This is just meant to be a convenience method for adding
        `matplotlib.Axes.axvline` object to the `Axes`, you can of course do
        that directly.

        See also
        --------
        `skyline.labels.LabelTrack`
        """
        # Merge the input styles with the defaults
        kwargs = {**styles.LINE_LINK_STYLE, **kwargs}
        cap_marker = cap_marker or dict()

        xpos = []
        if to is True:
            for i in linkers:
                style = {**i.style, **kwargs}
                request_x_pos = i.link_to.plot_center_pos
                self.axvline(x=request_x_pos, **style)
                xpos.append(request_x_pos)
        elif to is False:
            for i in linkers:
                style = {**i.style, **kwargs}
                request_x_pos = i.link_from.plot_center_pos
                self.axvline(x=request_x_pos, **style)
                xpos.append(request_x_pos)

        # Get the y-limits of the plot area just in case we need to use them
        # for placement of top/bottom caps
        ylim_min, ylim_max = self.get_ylim()

        # Remove any x/y coordinates the user might have slipped in there
        for i in ['x', 'y']:
            try:
                del(cap_marker[i])
            except (TypeError, KeyError):
                pass

        # the x coords of this transformation are data, and the
        # y coord are axes
        trans = transforms.blended_transform_factory(
            self.transData, self.transAxes)

        # If we want top_caps on the lines
        if top_cap is True:
            ypos = 1
            ypos = [ypos for i in range(len(xpos))]
            self.scatter(
                xpos, ypos,
                **{**styles.TOP_CAP_STYLE, **cap_marker}
            )

        # If we want top_caps on the lines
        if bottom_cap is True:
            ypos = 0
            ypos = [ypos for i in range(len(xpos))]

            # print(self.transData.transform((0, 0)))
            # print(self.transData.transform((0, 0))[0])
            self.scatter(
                xpos, ypos, transform=trans,
                **{**styles.BOTTOM_CAP_STYLE, **cap_marker}
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def plot_skyline(self, *args, **kwargs):
        """Add artists and everything for the plot to the axes.

        Parameters
        ----------
        *args
            Any positional arguments
        **kwargs
            Any keyword arguments

        Notes
        -----
        Typically this is the primary point where "plotting" takes place. This
        is called upon initialisation if ``do_plot=True``. This base class
        version is a place holder for the interface and does not do anything.
        """
        pass
