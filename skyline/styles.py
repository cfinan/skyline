"""Skyline style handling. Skyline has it's own style handling that can be used
completely partially or be sidelined entirely in favour of the style handling
in matplotlib. This is experimental but I wanted to build something to make it
easier to re-use styling elements and (i.e. a style hierarchy) and to make it
easy to highlight certain elements of a plot.
"""
from collections.abc import Mapping, MutableMapping
from skyline import colors as skycol
from matplotlib import transforms
from matplotlib import rcParams
# import sys
import math
import pprint as pp


LINE_LINK_STYLE = dict(
    color=skycol.MANHATTAN_GREY_ODD,
    alpha=0.5,
    linewidth=1,
    linestyle='dashed'
)
"""A default style for the lines that occur in line linker tracks. The dict
keys should be appropriate for ``matplotlib.axes.Axes.axline`` (`dict`)
"""

# LABEL_TRACK_STYLE = dict(
#     rotation=90,
#     fontstyle='italic',
#     color=skycol.MANHATTAN_GREY_ODD,
#     horizontalalignment='center',
#     fontsize=5.0
# )
# """A default style for the text that occurs in a label track. The dict
# keys should be appropriate for ``matplotlib.text.Text`` (`dict`)
# """

MANHATTAN_CHR = dict(
    s=1, marker='.', edgecolor=None
)
"""A default style for points in a manhattan/miami scatter plot. The dict
keys should be appropriate for ``matplotlib.axes.Axes.scatter`` (`dict`)
"""

MANHATTAN_ODD_CHR = dict(
    **MANHATTAN_CHR,
    **dict(c=skycol.add_alpha(skycol.MANHATTAN_GREY_ODD, 0.05))
)
"""A default style for points representing an even chromosome index number in a
manhattan/miami scatter plot. The dict keys should be appropriate for
``matplotlib.axes.Axes.scatter`` (`dict`)
"""

MANHATTAN_EVEN_CHR = dict(
    **MANHATTAN_CHR,
    **dict(c=skycol.add_alpha(skycol.MANHATTAN_GREY_EVEN, 0.05))
)
"""A default style for points representing an add chromosome index number in a
manhattan/miami scatter plot. The dict keys should be appropriate for
``matplotlib.axes.Axes.scatter`` (`dict`)
"""

SIG_LIMIT = -math.log10(5E-08)
"""The canonical GWAS significance limit (`float`)
"""
CON_LIMIT = -math.log10(5E-12)
"""A conservate GWAS significance limit (`float`)
"""
LIMITS = [(CON_LIMIT, skycol.SIGNIF_PURPLE), (SIG_LIMIT, skycol.GITLAB_ORANGE)]
"""GWAS cutoff limits (`tuple` of (`float`, `str`))
"""

CHR_LABEL_STYLE = dict(
    rotation=0,
    fontsize=4,
    ha='center'
)
"""The default text style for the `skyline.tracks.ChrLabels` track. The dict
keys should be appropriate for `matplotlib.text.Text` (`dict`)
"""

CAP_STYLE = dict(
    color=skycol.MANHATTAN_GREY_ODD,
    alpha=0.5, s=1, edgecolor=None
)
"""The default CAP style used to cap drop lines that can be added to skyline
Axes (`dict`)
"""

BOTTOM_CAP_STYLE = dict(
    **CAP_STYLE, **dict(marker='^')
)
"""The default CAP style used to cap the bottom of drop lines that can be added
to skyline Axes (`dict`)
"""

TOP_CAP_STYLE = dict(
    **CAP_STYLE, **dict(marker='v')
)
"""The default CAP style used to cap the top of drop lines that can be added
to skyline Axes (`dict`)
"""


# #############################################################################
# BELOW IS SOME EXPERIMENTAL STYLE CODE WITH INHERITING DICTS I REALLY LIKE   #
# IT BUT IT IS PROBLEMATIC FOR USE WITH MATPLOTLIB AS IT IS DESIGNED TO HOLD  #
# ALL AVAILABLE OPTIONS SO I HAVE TO FILTER OUT UNNEEDED OPTIONS EACH TIME IT #
# IS PASSED TO MATPLOTLIB OBJECTS. I THINK I SHOULD HAVE A SET OF SOURCE      #
# DICTS WHERE PARAMETERS CAN BE REGISTERED                                    #
# #############################################################################
# pp.pprint(rcParams)
_SIMPLE_STYLE_ATTR = dict(
    edgecolor=rcParams["patch.facecolor"],
    # edgecolor=rcParams["scatter.edgecolors"],
    facecolor=rcParams["patch.facecolor"],
    linestyle=rcParams['lines.linestyle'],
    linewidth=rcParams["lines.linewidth"],
    capstyle=rcParams["lines.solid_capstyle"],
    joinstyle=rcParams["lines.solid_joinstyle"],
    antialiased=rcParams["patch.antialiased"],
    offset=(0, 0),
    trans_offset=transforms.IdentityTransform(),
    offset_position='screen',
    hatch=None,
    pickradius=5,
    urls=None,
    zorder=1,
    plotnoninfinite=False,
    fontstyle=None,
    labelpad=None,
    rotation=None,
    fontsize=rcParams['font.size'],
    fontstretch=rcParams['font.stretch'],
    fontvariant=rcParams['font.variant'],
    fontweight=rcParams['font.weight'],
    marker=rcParams["scatter.marker"],
    size=rcParams['lines.markersize'],
    cmap=rcParams["image.cmap"],
    alpha=None,
    norm=None,
    color=rcParams["axes.prop_cycle"],
    c=rcParams["axes.prop_cycle"],
    xycoords='data',
    textcoords=None,
    verticalalignment='baseline',
    horizontalalignment='left',
    multialignment=None,
    fontproperties=None,  # defaults to FontProperties()
    linespacing=None,
    rotation_mode=None,
    usetex=None,          # defaults to rcParams['text.usetex']
    wrap=False,
    transform_rotates_text=False,
    agg_filter=None,
    in_layout=True,
    animated=False,
    clip_box=None,
    clip_path=None,
    clip_on=True,
    fontcolor=None,
    fontfamily=rcParams["font.family"],
    contains=None,
    math_fontfamily=None,
    path_effects=None,
    picker=None,
    rasterized=False,
    sketch_params=None,
    snap=None,
    transform=None,
    url=None,
    visible=True
)
"""Some defaults for the simple style object (`dict`)

The simple style object is the base level style that is designed to hold most
of the matplotlib styling parameters within it.
"""


# RESET = '\033[0m'
# def get_color_escape(r, g, b, background=False):
#     return '\033[{};2;{};{};{}m'.format(48 if background else 38, r, g, b)

# print(get_color_escape(255, 128, 0) 
#       + get_color_escape(80, 30, 60, True)
#       + 'Fancy colors!' 
#       + RESET)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _BaseNullStyle(Mapping):
    """An immutable "null" style that should allow things to default to
    matplotlib style management.
    """
    _ATTR = {}

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self):
        self._storage = dict(**self.__class__._ATTR)
        for k, v in self._storage.items():
            setattr(self, k, v)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        data = []
        for k, v in self._storage.items():
            data.append("{0}={1}".format(k, v))
        return "<{0}({1})>".format(self.__class__.__name__, ",".join(data))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __getitem__(self, key):
        try:
            return self._storage[key]
        except KeyError:
            pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        return iter(self._storage)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __len__(self):
        return len(self._storage)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class KwargFetchMixin(object):
    """A mixin class that will add functionality for getting keyword argument
    sets for various matplotlib function parameters that are used throughout
    skyline.
    """
    SCATTER_KWARGS = [
        'marker', 'alpha'

    ]
    _COLLECTION_KWARGS = [
        'capstyle', 'joinstyle', 'norm', 'cmap', 'hatch', 'pickradius', 'urls',
        'zorder'
    ]
    _TEXT_KWARGS = [
        'agg_filter', 'alpha', 'animated', 'bbox', 'clip_box', 'clip_on',
        'clip_path', 'color', 'contains', 'fontfamily', 'fontproperties',
        'fontsize', 'fontstretch', 'fontstyle', 'fontvariant', 'fontweight',
        'gid', 'horizontalalignment', 'in_layout', 'label', 'linespacing',
        'math_fontfamily', 'multialignment', 'path_effects', 'picker',
        'position', 'rasterized', 'rotation', 'rotation_mode',
        'sketch_params', 'snap', 'text', 'transform',
        'transform_rotates_text', 'url', 'usetex', 'verticalalignment',
        'visible', 'wrap', 'zorder'
    ]
    """Keyword argument names that are directly translatable to the style
    object kwargs. These tend to be the singular ones (`list` or `str`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_scatter_kwargs(self):
        """Get a kwarg dict of keyword arguments appropriate for a called to
        `matplotlib.Axes.scatter`.

        Returns
        -------
        kwargs : `dict`
           Keyword arguments that can be passed to a `matplotlib.Axes.scatter`.
        """
        kwargs = self.get_collection_kwargs()
        for i in self.__class__.SCATTER_KWARGS:
            kwargs[i] = self[i]
        kwargs['c'] = self['color']
        kwargs['s'] = self['size']
        del kwargs['facecolors']
        del kwargs['offsets']
        del kwargs['linewidths']
        return kwargs

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_collection_kwargs(self):
        """Get a kwarg dict of keyword arguments appropriate for a collection.

        Returns
        -------
        kwargs : `dict`
            Keyword arguments that can be passed to a collection object
        """
        kwargs = dict(
            [(i, self[i]) for i in self.__class__._COLLECTION_KWARGS]
        )
        kwargs['edgecolors'] = self['edgecolor']
        kwargs['facecolors'] = self['facecolor']
        kwargs['linewidths'] = self['linewidth']
        kwargs['linestyles'] = self['linestyle']
        kwargs['antialiaseds'] = self['antialiased']
        kwargs['offsets'] = self['offset']
        return kwargs

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_text_kwargs(self):
        kwargs = dict(
            [(i, self[i]) for i in self.__class__._TEXT_KWARGS]
        )
        kwargs['backgroundcolor'] = self['color']
        kwargs['color'] = self['fontcolor']
        return kwargs


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _NullSimpleStyle(_BaseNullStyle, KwargFetchMixin):
    """A null style that should allow things to default to matplotlib style
    management.

    This is the null object for the `skyline.style.SimpleStyle`.  This is
    immutable and is designed to have the same interace as the
    `skyline.style.SimpleStyle` object but just return values that will make
    matplotlib use it's default style.
    """
    _ATTR = _SIMPLE_STYLE_ATTR


NULL_SIMPLE_STYLE = _NullSimpleStyle()
"""A module level null  style object that users can use. It only needs to be
created once (`skyline.styles._NullSimpleStyle`).
"""

_SKYLINE_STYLE_ATTR = dict(
    gene_patch=NULL_SIMPLE_STYLE,
    gene_patch_coding=NULL_SIMPLE_STYLE,
    gene_patch_noncoding=NULL_SIMPLE_STYLE,
    # gene_track_colors=[NULL_SIMPLE_STYLE],
    gene_track=NULL_SIMPLE_STYLE,
    gene_label=NULL_SIMPLE_STYLE,
    scatter_track_colors=[NULL_SIMPLE_STYLE],
    scatter_track_label=NULL_SIMPLE_STYLE,
    chraxis_track_label=NULL_SIMPLE_STYLE,
    label_track_label=NULL_SIMPLE_STYLE
)
"""The default Skyline styling arguments for a `skyline.styles.SkylineStyle`
object (`dict`)
"""
_ALL_STYLE_ATTR = {**_SIMPLE_STYLE_ATTR, **_SKYLINE_STYLE_ATTR}
"""The combined default skyline and simple skyline arguments for a
`skyline.styles.SkylineStyle` object (`dict`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _NullSkylineStyle(_BaseNullStyle):
    """A null style that should allow things to default to matplotlib style
    management
    """
    _ATTR = _ALL_STYLE_ATTR


NULL_SKYLINE_STYLE = _NullSkylineStyle()
DEFAULT_STYLE = NULL_SKYLINE_STYLE


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_property(self, key):
    """Get the property attribute from the object storage

    Parameters
    ----------
    key : `str`
        The name of the property to get

    Raises
    ------
    AttributeError
        If the property is not valid

    Notes
    -----
    This first looks directly in the storage but if the property is not set
    there then it will look in the storage of the parent. This will end up
    being recursive, if the parent does not have the property then it will look
    in it's parent etc...Eventually, it will come to the
    `skyline.styles._NullSimpleStyle` or `skyline.styles._NullSkylineStyle`
    which should contain all attributes.
    """
    try:
        return self._storage[key]
    except KeyError:
        pass

    try:
        return self._parent[key]
    except KeyError:
        pass
    raise AttributeError("unknown style attribute: {0}".format(key))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _set_property(self, key, value):
    """Set the property attribute in the object storage

    Parameters
    ----------
    key : `str`
        The name of the property to set
    value : `Any`
        The value to set the property to

    Raises
    ------
    AttributeError
        If the property is not allowed
    """
    if key in self.__class__._ATTR:
        self._storage[key] = value
        return
    raise AttributeError("unknown style attribute: {0}".format(key))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _getter(self, key):
    """A closure to provide a property method that will return the correct
    value from the storage dictionary.

    Parameters
    ----------
    key : `str`
        The attribute to get

    Returns
    -------
    getter : `func`
        A get function for the property
    """
    def _get(self):
        return _get_property(self, key)
    return _get


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _setter(self, key):
    """A closure to provide a property method that will set the correct
    value in the storage dictionary.

    Parameters
    ----------
    key : `str`
        The attribute to get

    Returns
    -------
    setter : `func`
        A set function for the property
    """
    def _set(self, value):
        return _set_property(self, key, value)
    return _set


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class _BaseStyle(MutableMapping):
    """A null style that should allow things to default to matplotlib style
    management
    """
    _ATTR = []
    _DEFAULT_PARENT = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __new__(cls, *args, parent=None, **kwargs):
        for k in cls._ATTR:
            setattr(cls, k, property(_getter(cls, k), _setter(cls, k)))
        instance = object.__new__(cls)
        return instance

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, parent=None, **kwargs):
        # Assign the parent and ensure the class type is valid
        self._parent = parent or self.__class__._DEFAULT_PARENT
        self._check_parent()

        self._storage = dict(*args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _check_parent(self):
        """Ensure the parent is a valid class type
        """
        if not issubclass(
                self._parent.__class__,
                (self.__class__, self._DEFAULT_PARENT.__class__)
        ):
            raise TypeError(
                "parent must be: {0} or {1}".format(
                    self._parent.__class__.__name__,
                    self.__class__._DEFAULT_PARENT.__class__.__name__
                )
            )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        data = []
        for k, v in self._storage.items():
            data.append("{0}={1}".format(k, v))
        data.append("parent={0}".format(self._parent))
        return "<{0}({1})>".format(self.__class__.__name__, ",".join(data))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __getitem__(self, key):
        return _get_property(self, key)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __setitem__(self, key, value):
        self._storage[key] = value

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __delitem__(self, key, value):
        del self._storage[key]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __iter__(self):
        return iter(self._storage)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __len__(self):
        return len(self._storage)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def parent(self):
        return self._parent


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SimpleStyle(_BaseStyle, KwargFetchMixin):
    _ATTR = list(_SIMPLE_STYLE_ATTR.keys())
    _DEFAULT_PARENT = _NullSimpleStyle


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SkylineStyle(_BaseStyle):
    pass


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class SkylineStyle(_BaseStyle):
#     _ATTR = list(_ALL_STYLE_ATTR.keys())
#     _DEFAULT_PARENT = _NullSkylineStyle


# # A module level default style
DEFAULT_STYLE = SkylineStyle()
# BIOTYPE_STYLES = {
#     '3prime_overlapping_ncRNA': DEFAULT_STYLE,
#     'IG_C_gene': DEFAULT_STYLE,
#     'IG_C_pseudogene': DEFAULT_STYLE,
#     'IG_D_gene': DEFAULT_STYLE,
#     'IG_D_pseudogene': DEFAULT_STYLE,
#     'IG_J_gene': DEFAULT_STYLE,
#     'IG_J_pseudogene': DEFAULT_STYLE,
#     'IG_LV_gene': DEFAULT_STYLE,
#     'IG_M_gene': DEFAULT_STYLE,
#     'IG_V_gene': DEFAULT_STYLE,
#     'IG_V_pseudogene': DEFAULT_STYLE,
#     'IG_Z_gene': DEFAULT_STYLE,
#     'IG_pseudogene': DEFAULT_STYLE,
#     'LRG': 'DEFAULT_STYLE',
#     'LRG_gene': DEFAULT_STYLE,
#     'Mt_rRNA': DEFAULT_STYLE,
#     'Mt_tRNA': DEFAULT_STYLE,
#     'Mt_tRNA_pseudogene': DEFAULT_STYLE,
#     'RNase_MRP_RNA': DEFAULT_STYLE,
#     'RNase_P_RNA': DEFAULT_STYLE,
#     'SRP_RNA': DEFAULT_STYLE,
#     'TEC': DEFAULT_STYLE,
#     'TR_C_gene': DEFAULT_STYLE,
#     'TR_D_gene': DEFAULT_STYLE,
#     'TR_J_gene': DEFAULT_STYLE,
#     'TR_J_pseudogene': DEFAULT_STYLE,
#     'TR_V_gene': DEFAULT_STYLE,
#     'TR_V_pseudogene': DEFAULT_STYLE,
#     'TR_gene': DEFAULT_STYLE,
#     'Y_RNA': DEFAULT_STYLE,
#     'ambiguous_orf': DEFAULT_STYLE,
#     'antisense': DEFAULT_STYLE,
#     'antisense_RNA': DEFAULT_STYLE,
#     'antitoxin': DEFAULT_STYLE,
#     'bidirectional_promoter_lncRNA': DEFAULT_STYLE,
#     'class_II_RNA': DEFAULT_STYLE,
#     'class_I_RNA': DEFAULT_STYLE,
#     'coding': 'DEFAULT_STYLE',
#     'disrupted_domain': DEFAULT_STYLE,
#     'guide_RNA': DEFAULT_STYLE,
#     'known_ncRNA': DEFAULT_STYLE,
#     'lincRNA': DEFAULT_STYLE,
#     'lncRNA': DEFAULT_STYLE,
#     'lnoncoding': 'DEFAULT_STYLE',
#     'macro_lncRNA': DEFAULT_STYLE,
#     'miRNA': DEFAULT_STYLE,
#     'miRNA_pseudogene': DEFAULT_STYLE,
#     'misc_RNA': DEFAULT_STYLE,
#     'misc_RNA_pseudogene': DEFAULT_STYLE,
#     'misc_non_coding': DEFAULT_STYLE,
#     'mnoncoding': 'DEFAULT_STYLE',
#     'ncRNA': DEFAULT_STYLE,
#     'ncRNA_host': DEFAULT_STYLE,
#     'no_group': 'DEFAULT_STYLE',
#     'non_coding': DEFAULT_STYLE,
#     'non_stop_decay': DEFAULT_STYLE,
#     'nonsense_mediated_decay': DEFAULT_STYLE,
#     'nontranslating_CDS': DEFAULT_STYLE,
#     'piRNA': DEFAULT_STYLE,
#     'polymorphic_pseudogene': DEFAULT_STYLE,
#     'pre_miRNA': DEFAULT_STYLE,
#     'processed_pseudogene': DEFAULT_STYLE,
#     'processed_transcript': DEFAULT_STYLE,
#     'protein_coding': DEFAULT_STYLE,
#     'pseudogene': DEFAULT_STYLE,
#     'pseudogene_with_CDS': DEFAULT_STYLE,
#     'rRNA': DEFAULT_STYLE,
#     'rRNA_pseudogene': DEFAULT_STYLE,
#     'retained_intron': DEFAULT_STYLE,
#     'ribozyme': DEFAULT_STYLE,
#     'rnaseq_putative_cds': DEFAULT_STYLE,
#     'sRNA': DEFAULT_STYLE,
#     'scRNA': DEFAULT_STYLE,
#     'scRNA_pseudogene': DEFAULT_STYLE,
#     'scaRNA': DEFAULT_STYLE,
#     'sense_intronic': DEFAULT_STYLE,
#     'sense_overlapping': DEFAULT_STYLE,
#     'snRNA': DEFAULT_STYLE,
#     'snRNA_pseudogene': DEFAULT_STYLE,
#     'snlRNA': DEFAULT_STYLE,
#     'snoRNA': DEFAULT_STYLE,
#     'snoRNA_pseudogene': DEFAULT_STYLE,
#     'snoncoding': 'DEFAULT_STYLE',
#     'tRNA': DEFAULT_STYLE,
#     'tRNA_pseudogene': DEFAULT_STYLE,
#     'telomerase_RNA': DEFAULT_STYLE,
#     'tmRNA': DEFAULT_STYLE,
#     'transcribed_processed_pseudogene': DEFAULT_STYLE,
#     'transcribed_unitary_pseudogene': DEFAULT_STYLE,
#     'transcribed_unprocessed_pseudogene': DEFAULT_STYLE,
#     'translated_processed_pseudogene': DEFAULT_STYLE,
#     'translated_unprocessed_pseudogene': DEFAULT_STYLE,
#     'transposable_element': DEFAULT_STYLE,
#     'undefined': 'DEFAULT_STYLE',
#     'unitary_pseudogene': DEFAULT_STYLE,
#     'unknown': DEFAULT_STYLE,
#     'unknown_likely_coding': DEFAULT_STYLE,
#     'unprocessed_pseudogene': DEFAULT_STYLE,
#     'vault_RNA': DEFAULT_STYLE
# }
