"""Tools for aligning objects on plots
"""
from skyline import utils
import numpy as np
import math
import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Align1D(object):
    """Align straight objects in the x (horizontal) dimension. This is mainly
    used for label tracks.

    Parameters
    ----------
    place_axes : `matplotlib.Axes`
        The axes where the aligned artists will be placed.
    artist_objs : `matplotlib.Artist`
        An object inheriting from `matplotlib.Artist`.
    nudge_prop : `float`
        The proportion of the horizontal plotting space that each obect is
        shifted when they are being aligned (in Axes coordinates).

    Notes
    -----
    This requires access to a renderer.
    """
    _LABEL_TEXT = "LABEL_TEXT"

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, place_axes, coords, nudge_prop=0.001,
                 artist_name=_LABEL_TEXT):
        self._axes = place_axes
        self._coords = coords
        self.sky_artist_name = artist_name
        self._nudge_prop = nudge_prop
        self._initialised = False
        start, end = self._axes.get_xlim()
        self._axes_length = end - start
        self._data_nudge = self._axes_length * nudge_prop

        # We get the figure and the renderer, we need a renderer to determine
        # the size of the objects in display coordinates
        self._figure = self._axes.get_figure()
        self._renderer = self._figure.canvas.get_renderer()

        # Initialise will line up (with equal spacing) all the artists and
        # determine if there is enough space to plot them without overlap
        self.initialise()

        # Now align them as best we can
        self.align()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def initialise(self):
        """Initialise the coordinates objects so they are arranges in the
        correct order
        """
        for idx, i in enumerate(self._coords):
            try:
                i.priority
            except AttributeError:
                i.priority = idx

        # Now get the total linear space taken up by the artists
        self.get_used_space()
        self._coords.sort(key=self._left_limit)
        x_place = 0.0
        available_space = 1.0
        placements = available_space/len(self._coords)
        for idx, i in enumerate(self._coords, 1):
            data_x, data_y = i.artists[self.sky_artist_name].get_position()
            new_data_x, new_data_y = self.get_data_units(x_place, 0)
            i.artists[self.sky_artist_name].set_position((new_data_x, data_y))
            available_space -= self.get_artist_size(i)
            i.delta_pos = i.requested_pos - new_data_x
            x_place += placements
        self._initialised = True

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def align(self):
        """Perform the alignment on the objects. If not initialised this will
        perform initialisation as well.
        """
        if self._initialised is False:
            self.initialise()
        iterations = 0
        stop_it = 1000000
        moves = 1
        artist_values = self.get_values()
        while moves > 0 and iterations < stop_it:
            moves = 0

            # This provides a list of the element index position of the
            # object's distance away from their ideal location. So the index
            # position of the elements that are furthest away from their
            # ideal position are at the start and those closest to their ideal
            # position are at the end. This means that the most offset objects
            # are moved first.
            delta_index = utils.index_sort(
                [abs(i['delta_pos']) for i in artist_values], reverse=True
            )
            for idx in delta_index:
                # Get the actual artist at that index (code clarity)
                artist = artist_values[idx]

                if artist['delta_pos'] == 0:
                    continue

                move_dir = int(math.copysign(1, artist['delta_pos']))

                neighbour_idx = idx + move_dir
                move_dist = 0
                if neighbour_idx > 0 and neighbour_idx < len(artist_values):
                    # In range so calculate the distance with the neighbour
                    neighbour_artist = artist_values[neighbour_idx]

                    min_dist = min(
                        [abs(i - j) for i in [artist['start'], artist['end']]
                         for j in [neighbour_artist['start'], neighbour_artist['end']]]
                    )
                    move_dist = min(
                        min_dist,
                        self._data_nudge,
                        abs(artist['delta_pos'])
                    )
                    self._data_nudge
                elif neighbour_idx < 0:
                    # "start" artists wanting to move to the left
                    move_dist = abs(artist['delta_pos'])
                elif neighbour_idx == len(self._coords):
                    # "end" artist wanting to move to the right
                    move_dist = abs(artist['delta_pos'])

                if move_dist == 0:
                    continue

                artist['data_x'] = artist['data_x'] + (move_dir * move_dist)
                artist['start'] = artist['data_x']+artist['start_x_diff']
                artist['end'] = artist['start']+artist['span']
                artist['delta_pos'] = \
                    artist['requested_pos'] - artist['data_x']
                moves += 1
            iterations += 1
        for i in artist_values:
            i['artist'].set_position((i['data_x'], i['data_y']))

            # diff = i['coords'].end_pos - i['coords'].start_pos
            # i['coords'].set_pos(i['data_x'], i['data_x'] + diff)
        self._initialised = False

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_values(self):
        """
        """
        values = []
        for i in self._coords:
            data_x, data_y = i.artists[self.sky_artist_name].get_position()
            start, end = self.get_artist_x_span(i)

            values.append(
                dict(
                    data_x=data_x,
                    data_y=data_y,
                    delta_pos=i.delta_pos,
                    start=start,
                    end=end,
                    span=end-start,
                    start_x_diff=start - data_x,
                    requested_pos=i.requested_pos,
                    artist=i.artists[self.sky_artist_name],
                    coords=i
                )
            )
        return values

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_data_units(self, axes_x_coords, axes_y_coords):
        """Transform some axes x/y coordinates into data x/y coordinates.

        Parameters
        ----------
        axes_x_coords : `float`
            The x coordinates in Axes units (0-1)
        axes_y_coords : `float`
            The y coordinates in Axes units (0-1)

        Return
        ------
        data_x_coords : `float`
            The x coordinates in data units.
        axes_y_coords : `float`
            The y coordinates in data units.
        """
        return self._axes.transData.inverted().transform(
            self._axes.transAxes.transform((axes_x_coords, axes_y_coords))
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_used_space(self):
        """Get the total linear space that is used up by the span of the
        coord/artists (in Axes coordinates).

        Returns
        -------
        space : `float`
            The space used by the artists associated with the coordinates.

        Raises
        ------
        ValueError
            If all the artists do not fit into the Axes without colliding.
        """
        space = 0
        for i in self._coords:
            bb = i.artists[self.sky_artist_name].get_window_extent(
                self._renderer
            )
            bb = self._axes.transAxes.inverted().transform(bb)
            space += np.diff(bb[:, 0])[0]

        if space > 1:
            raise ValueError(
                "too many objects to fit, try fewer labels or smaller size"
            )
        return space

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_axes_min_distance(self, first_artist, second_artist):
        """
        """
        first_artist_span = self.get_artist_axes_x_span(first_artist)
        second_artist_span = self.get_artist_axes_x_span(second_artist)
        return min(
            [abs(j - i) for i in first_artist_span for j in second_artist_span]
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_min_distance(self, first_artist, second_artist):
        """
        """
        first_artist_span = self.get_artist_x_span(first_artist)
        second_artist_span = self.get_artist_x_span(second_artist)
        return min(
            [abs(j - i) for i in first_artist_span for j in second_artist_span]
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_axes_coords(self, artist):
        """Get the x, y coordinates of the artist expressed as Axes
        coordinates.

        Parameters
        ----------
        artist :  : `matplotlib.Artist`
            An object inheriting from `matplotlib.Artist`. Must have a
            ``get_position`` method

        Returns
        -------
        """
        return self._axes.transAxes.inverted().transform(
            self._axes.transData.transform(artist.get_position())
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_axes_delta_opt(self, artist):
        """Get the x, y coordinates of the artist expressed as Axes
        coordinates.

        Parameters
        ----------
        artist :  : `matplotlib.Artist`
            An object inheriting from `matplotlib.Artist`. Must have a
            ``get_position`` method

        Returns
        -------
        """
        return self._axes.transAxes.inverted().transform(
            self._axes.transData.transform(
                (abs(artist.skyline_delta_pos), abs(artist.skyline_delta_pos))
            )
        )[0]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_bounding_box(self, coord):
        """
        """
        bb = coord.artists[self.sky_artist_name].get_window_extent(
            self._renderer
        )
        return self._axes.transAxes.inverted().transform(bb)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_artist_size(self, coord):
        """
        """
        bb = coord.artists[self.sky_artist_name].get_window_extent(
            self._renderer
        )
        bb = self._axes.transAxes.inverted().transform(bb)
        return np.diff(bb[:, 0])[0]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_artist_axes_x_span(self, coord):
        """
        """
        bb = coord.artists[self.sky_artist_name].get_window_extent(
            self._renderer
        )
        bb = self._axes.transAxes.inverted().transform(bb)
        return bb[:, 0]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_artist_x_span(self, coord):
        """
        """
        bb = coord.artists[self.sky_artist_name].get_window_extent(
            self._renderer
        )
        bb = self._axes.transData.inverted().transform(bb)
        return bb[:, 0]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _left_limit(self, coord):
        """Get the left limit of the artist in Axes coordinates
        """
        bb = coord.artists[self.sky_artist_name].get_window_extent(
            self._renderer
        )
        bb = self._axes.transAxes.inverted().transform(bb)
        return bb[0, 0]



# # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# class Align1D(object):
#     """Align straight objects in the x (horizontal) dimension. This is mainly
#     used for label tracks.

#     Parameters
#     ----------
#     place_axes : `matplotlib.Axes`
#         The axes where the aligned artists will be placed.
#     artist_objs : `matplotlib.Artist`
#         An object inheriting from `matplotlib.Artist`.
#     nudge_prop : `float`
#         The proportion of the horizontal plotting space that each obect is
#         shifted when they are being aligned (in Axes coordinates).

#     Notes
#     -----
#     This requires access to a renderer.
#     """
#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def __init__(self, place_axes, artists_objs, nudge_prop=0.001):
#         self._axes = place_axes
#         self._artists = artists_objs
#         self._nudge_prop = nudge_prop
#         self._initialised = False

#         start, end = self._axes.get_xlim()
#         self._axes_length = end - start
#         self._data_nudge = self._axes_length * nudge_prop

#         # We get the figure and the renderer, we need a renderer to determine
#         # the size of the objects in display coordinates
#         self._figure = self._axes.get_figure()
#         self._renderer = self._figure.canvas.get_renderer()

#         # Initialise will line up (with equal spacing) all the artists and
#         # determine if there is enough space to plot them without overlap
#         self.initialise()

#         # Now align them as best we can
#         self.align()

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def initialise(self):
#         """
#         """
#         for idx, i in enumerate(self._artists):
#             try:
#                 i.skyline_priority
#             except AttributeError:
#                 i.skyline_priority = idx

#         # Now get the total linear space taken up by the artists
#         self.get_used_space()
#         self._artists.sort(key=self._left_limit)
#         x_place = 0.0
#         available_space = 1.0
#         placements = available_space/len(self._artists)
#         for idx, i in enumerate(self._artists, 1):
#             # print("* IDX: {0}".format(idx))
#             data_x, data_y = i.get_position()
#             new_data_x, new_data_y = self.get_data_units(x_place, 0)
#             i.set_position((new_data_x, data_y))
#             available_space -= self.get_artist_size(i)
#             # Adjust for uneven artist size
#             # placements = available_space/(len(self._artists) - idx)
#             i.skyline_delta_pos = i.skyline_requested_pos - new_data_x
#             x_place += placements
#         self._initialised = True

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def align(self):
#         """Perform the alignment on the objects. If not initialised this will
#         perform initialisation as well.
#         """
#         if self._initialised is False:
#             self.initialise()
#         iterations = 0
#         stop_it = 1000000
#         # print(iterations)
#         moves = 1
#         artist_values = self.get_values()
#         while moves > 0 and iterations < stop_it:
#             # print("************======== Iteration: {0} =========*************".format(iterations))
#             moves = 0

#             # This provides a list of the element index position of the
#             # object's distance away from their ideal location. So the index
#             # position of the elements that are furthest away from their
#             # ideal position are at the start and those closest to their ideal
#             # position are at the end. This means that the most offset objects
#             # are moved first.
#             delta_index = utils.index_sort(
#                 [abs(i['delta_pos']) for i in artist_values], reverse=True
#             )
#             for idx in delta_index:
#                 # Get the actual artist at that index (code clarity)
#                 artist = artist_values[idx]
#                 # pp.pprint(artist)
#                 # data_x, data_y = artist.get_position()
#                 # print("******* {1}: {0} *******".format(artist, idx))
#                 # print("* Data X: {0}".format(artist['data_x']))
#                 # print("* Data Y: {0}".format(artist['data_y']))
#                 # Get the direction it wants to move it to go towards it's
#                 # ideal location
#                 # print(
#                 #     "* Requested Pos: {0}".format(artist['requested_pos'])
#                 # )
#                 # print("* Delta Pos: {0}".format(artist['delta_pos']))
#                 if artist['delta_pos'] == 0:
#                     # print("* Optimised: {0}".format(artist))
#                     continue

#                 move_dir = int(math.copysign(1, artist['delta_pos']))
#                 # print("* Move Dir: {0}".format(move_dir))
#                 # span = self.get_artist_x_span(artist)
#                 # print("* Start: {0}".format(artist['start']))
#                 # print("* End: {0}".format(artist['end']))

#                 neighbour_idx = idx + move_dir
#                 move_dist = 0
#                 if neighbour_idx > 0 and neighbour_idx < len(artist_values):
#                     # In range so calculate the distance with the neighbour
#                     neighbour_artist = artist_values[neighbour_idx]
#                     # print("= neighbour ({1}): {0}".format(
#                     #     neighbour_artist, neighbour_idx)
#                     # )
#                     # nb_x, nb_y = neighbour_artist.get_position()
#                     # print("= NB X: {0}".format(data_x))
#                     # print("= NB Y: {0}".format(data_y))
#                     # nb_span = self.get_artist_x_span(neighbour_artist)
#                     # print("= NB Span: {0}".format(nb_span))
#                     # min_dist = self.get_min_distance(artist, neighbour_artist)
#                     min_dist = min(
#                         [abs(i - j) for i in [artist['start'], artist['end']]
#                          for j in [neighbour_artist['start'], neighbour_artist['end']]]
#                     )
#                     # print("*= Min Dist: {0}".format(min_dist))
#                     # move_dist = min(
#                     #     min_dist,
#                     #     (self._nudge_prop*abs(artist['delta_pos']))
#                     # )
#                     move_dist = min(
#                         min_dist,
#                         self._data_nudge,
#                         abs(artist['delta_pos'])
#                     )
#                     self._data_nudge
#                 elif neighbour_idx < 0:
#                     # "start" artists wanting to move to the left
#                     move_dist = abs(artist['delta_pos'])
#                     # print("CALLED")
#                 elif neighbour_idx == len(self._artists):
#                     # "end" artist wanting to move to the right
#                     move_dist = abs(artist['delta_pos'])
#                 #     print("CALLED")
#                 # print("*= Move Dist: {0}".format(move_dist))
#                 if move_dist == 0:
#                     continue

#                 artist['data_x'] = artist['data_x'] + (move_dir * move_dist)
#                 artist['start'] = artist['data_x']+artist['start_x_diff']
#                 artist['end'] = artist['start']+artist['span']
#                 # print("*= New loc data: {0}".format(new_loc))
#                 # artist.set_position((new_loc, data_y))
#                 # data_x, data_y = artist.get_position()
#                 artist['delta_pos'] = \
#                     artist['requested_pos'] - artist['data_x']
#                 moves += 1
#             # print("   = Moves: {0}".format(moves))
#             iterations += 1
#         for i in artist_values:
#             i['artist'].set_position((i['data_x'], i['data_y']))
#         # for i in self._artists:
#         #     print("{0}: {1}".format(i, i.skyline_delta_pos))
#         # print("Iterations={0}".format(iterations))
#         self._initialised = False

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def get_values(self):
#         """
#         """
#         values = []
#         for i in self._artists:
#             data_x, data_y = i.get_position()
#             start, end = self.get_artist_x_span(i)

#             values.append(
#                 dict(
#                     data_x=data_x,
#                     data_y=data_y,
#                     delta_pos=i.skyline_delta_pos,
#                     start=start,
#                     end=end,
#                     span=end-start,
#                     start_x_diff=start - data_x,
#                     requested_pos=i.skyline_requested_pos,
#                     artist=i
#                 )
#             )
#         return values

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def get_data_units(self, axes_x_units, axes_y_units):
#         """
#         """
#         return self._axes.transData.inverted().transform(
#             self._axes.transAxes.transform((axes_x_units, axes_y_units))
#         )

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def get_used_space(self):
#         """
#         """
#         space = 0
#         for i in self._artists:
#             bb = i.get_window_extent(self._renderer)
#             bb = self._axes.transAxes.inverted().transform(bb)
#             # space += np.diff(bb[:, con.BOTTOM])[0]
#             space += np.diff(bb[:, 0])[0]

#         if space > 1:
#             raise ValueError(
#                 "too many objects to fit, try fewer labels or smaller size"
#             )
#         return space

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def get_axes_min_distance(self, first_artist, second_artist):
#         """
#         """
#         first_artist_span = self.get_artist_axes_x_span(first_artist)
#         second_artist_span = self.get_artist_axes_x_span(second_artist)
#         return min(
#             [abs(j - i) for i in first_artist_span for j in second_artist_span]
#         )

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def get_min_distance(self, first_artist, second_artist):
#         """
#         """
#         first_artist_span = self.get_artist_x_span(first_artist)
#         second_artist_span = self.get_artist_x_span(second_artist)
#         return min(
#             [abs(j - i) for i in first_artist_span for j in second_artist_span]
#         )

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def get_axes_coords(self, artist):
#         """Get the x, y coordinates of the artist expressed as Axes
#         coordinates.

#         Parameters
#         ----------
#         artist :  : `matplotlib.Artist`
#             An object inheriting from `matplotlib.Artist`. Must have a
#             ``get_position`` method

#         Returns
#         -------
#         """
#         return self._axes.transAxes.inverted().transform(
#             self._axes.transData.transform(artist.get_position())
#         )

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def get_axes_delta_opt(self, artist):
#         """Get the x, y coordinates of the artist expressed as Axes
#         coordinates.

#         Parameters
#         ----------
#         artist :  : `matplotlib.Artist`
#             An object inheriting from `matplotlib.Artist`. Must have a
#             ``get_position`` method

#         Returns
#         -------
#         """
#         return self._axes.transAxes.inverted().transform(
#             self._axes.transData.transform(
#                 (abs(artist.skyline_delta_pos), abs(artist.skyline_delta_pos))
#             )
#         )[0]

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def get_bounding_box(self, artist):
#         """
#         """
#         bb = artist.get_window_extent(self._renderer)
#         return self._axes.transAxes.inverted().transform(bb)

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def get_artist_size(self, artist):
#         """
#         """
#         bb = artist.get_window_extent(self._renderer)
#         bb = self._axes.transAxes.inverted().transform(bb)
#         return np.diff(bb[:, 0])[0]

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def get_artist_axes_x_span(self, artist):
#         """
#         """
#         bb = artist.get_window_extent(self._renderer)
#         bb = self._axes.transAxes.inverted().transform(bb)
#         return bb[:, 0]

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def get_artist_x_span(self, artist):
#         """
#         """
#         bb = artist.get_window_extent(self._renderer)
#         bb = self._axes.transData.inverted().transform(bb)
#         return bb[:, 0]

#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     def _left_limit(self, artist):
#         """
#         """
#         bb = artist.get_window_extent(self._renderer)
#         bb = self._axes.transAxes.inverted().transform(bb)
#         return bb[0, 0]
#         # space += np.diff(bb[:, con.BOTTOM])[0]
