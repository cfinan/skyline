``skyline.patches`` sub-package
===============================

``skyline.patches.genes`` module
--------------------------------

.. automodule:: skyline.patches.genes
   :members:
   :undoc-members:
   :show-inheritance:
