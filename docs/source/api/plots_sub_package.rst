``skyline.plots`` sub-package
=============================

``skyline.plots.scatter`` module
--------------------------------

.. automodule:: skyline.plots.scatter
   :members:
   :undoc-members:
   :show-inheritance:

``skyline.plots.locus_view`` module
-----------------------------------

.. automodule:: skyline.plots.locus_view
   :members:
   :undoc-members:
   :show-inheritance:

``skyline.plots.common`` module
-------------------------------

.. automodule:: skyline.plots.common
   :members:
   :undoc-members:
   :show-inheritance:
