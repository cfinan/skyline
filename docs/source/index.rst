.. skyline documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to skyline
==================

`Skyline <https://gitlab.com/cfinan/skyline>`_ is a genomic plotting library initially built for making Manhattan style plots but is gradually being generalised to plot other things as well. It is built on top of `matplotlib <https://matplotlib.org/>`_. It is still early days for development but you should be able to use it to create some nice looking visualisations.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 3
   :caption: Example code

   examples/examples

.. toctree::
   :maxdepth: 3
   :caption: Programmer reference

   api

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
