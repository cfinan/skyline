Example code
============

Here is some example code illustrating the basic features of skyline. There are some higher level plotting functions available in the ``skyline.plots`` module. These are wrappers around the lower level plotting components which all inherit from ``maplotlib.Axes``. Examples of both are detailed below.

.. toctree::
   :maxdepth: 2
   :caption: Plots:

   plots/grid_plot
   plots/miami
   plots/locus_view

.. toctree::
   :maxdepth: 2
   :caption: Plot Components:

   components/genes
