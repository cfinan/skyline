# Getting Started with skyline

__version__: `0.2.1a0`

The skyline package is a genomic plotting toolkit. It was initially to put together to plot Manhattan plots but can be generalised to plot general genomic "track" based data. It is very early in development but currently you can:

1. Plot Manhattan style plots
2. Plot Miami style plots
3. Plot skyview plots (multiple Manhattans stacked and viewed from the top)
4. Locus view type plots

All the plots components inherit from `matplotlib.Axes` and is placed on a `skyview.GenomicFigure` figure that inherits from `matplotlib.Figure`. This means that all the usual `matplotlib` functionality is baked into skyline.

It is still early days for skyline development so the API is still pretty basic and not fully documented. There are some example scripts through, these demonstrate many of the features of skyline. Please keep in mind that it is alpha and whilst I will try not to change any of the interfaces, I can't guarentee it.

I started writing this as I wanted to learn some of the matplotlib internals and I also needed to plot a Manhattan plot for a project I was working on...and before that time had not performed a single plot with matplotlib!? So, I welcome any feedback, good or bad and particularly from seasoned matplotlib users.

Currently, the various plots have only really been tested against the file based backends and not within notebooks or with any interactivity. I will however, tackle this once I have some of the basic building blocks in place.

There are no functional command line endpoints for skyline although I hope to add some in future for users that can't program in Python. Currently, there is a placeholder script that will hopefully be functional in the future. 

There is (currently limited) [online](https://filedn.eu/lIeh2yR6LSw58g5HQMThnmR/skyline/index.html) documentation for skyline and offline PDF documentation can be downloaded [here](https://gitlab.com/cfinan/skyline/-/blob/master/resources/pdf/skyline.pdf).

## Installation instructions
At present, skyline is undergoing development and no packages exist yet on PyPy or in conda. Therefore it is recommended that it is installed in either of the two ways listed below. First, clone this repository and then `cd` to the root of the repository.

```
# Change in your package - this is not in git
git clone git@gitlab.com:cfinan/skyline.git
cd skyline
```

### Packages not in Pypi
Skyline, is able to use data from Ensembl within it's plots. For this it uses the Ensembl Rest API. I have written a Python package for this called [ensembl-rest-client](https://gitlab.com/cfinan/ensembl-rest-client), this has been added as a git endpoint in `requirements.txt` but will need to be installed via cloning and pip if you are using and Conda environments. In addition, the `skyline.plots` module uses [MeRIT](https://gitlab.com/cfinan/merit/-/tree/rebuild) to read in the GWAS data. MeRIT, is not currently publicly available and I hope to remove this dependency soon but please get in contact if you want access. MeRIT has also been added to the `requirements.txt`

### Installation not using any conda dependencies
If you are not using conda in any way then install the dependencies via `pip` and install skyline as an editable install also via pip:

Install dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

For an editable (developer) install run the command below from the root of the skyline repository (this is recommended at this stage):
```
python -m pip install -e .
```

### Installation using conda dependencies
A conda environment is provided in a `yaml` file in the directory `./resources/conda_env`. A new conda environment called `skyline` can be built using the command:

```
# From the root of the skyline repository
conda env create --file ./resources/conda_env/conda_create.yml
```

To add to an existing environment use:
```
# From the root of the skyline repository
conda env update --file ./resources/conda_env/conda_update.yml
```

For an editable (developer) install of skyline run the command below from the root of the skyline repository:
```
python -m pip install -e .
```

## Next steps...
There are no real tests with skyline as I am unclear how to actually test a plotting package! However, it is recommended that the user runs the example code and notebooks to make sure it is functional.

## Command endpoints
Installation will install the following endpoints. Usage of all these scripts can be listed with `<COMMAND> --help`.

* `skyline` - Currently this is a placeholder and has no functionality. I hope to implement something soon so non-Python users can benefit from the package.
