===============
``skyline`` API
===============

.. toctree::
   :maxdepth: 4

   api/plots_sub_package
   api/skyline_package
   api/tracks_sub_package
   api/patches_sub_package
