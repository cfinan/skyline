# Getting Started with skyline

__version__: `0.3.4a0`

The skyline package is a genomic plotting toolkit. It was initially to put together to plot Manhattan plots but can be generalised to plot general genomic "track" based data. It is very early in development but currently you can:

1. Plot Manhattan style plots
2. Plot Miami style plots
3. Plot skyview plots (multiple Manhattans stacked and viewed from the top)
4. Locus view type plots
5. Link thinks together with different sorts of linkers
6. Add label tracks

All the plots components inherit from `matplotlib.Axes` and are placed on a `skyview.GenomicFigure` figure that inherits from `matplotlib.Figure`. This means that all the usual `matplotlib` functionality is baked into skyline.

It is still early days for skyline development so the API is still pretty basic and not fully documented. There are some example scripts through, these demonstrate many of the features of skyline. Please keep in mind that it is alpha and whilst I will try not to change any of the interfaces, I can't guarentee it.

I started writing this as I wanted to learn some of the matplotlib internals and I also needed to plot a Manhattan plot for a project I was working on...and before that time had not performed a single plot with matplotlib!? So, I welcome any feedback, good or bad and particularly from seasoned matplotlib users.

Currently, the various plots have only really been tested against the file based backends and some notebooks.

There are no functional command line endpoints for skyline although I hope to add some in future for users that can't program in Python.

There is (currently limited) [online](https://cfinan.gitlab.io/skyline/) documentation for skyline.

## Installation instructions
At present, skyline is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that you install in either of the two ways listed below.

### Installation using conda
I maintain a conda package in my personal conda channel. To install from this please run:

```
conda install -c cfin -c bioconda -c conda-forge skyline
```

There are currently builds for Python v3.8, v3.9 and v3.10 for Linux-64 and Mac-osX. However, please keep in mind that all development is carried out on Linux-64 and Python v3.8/v3.9. I do not own a Mac so can't test on one, the conda build does run some import tests but that is it.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/skyline.git
cd skyline
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the repository. The difference with this is that you can just to a `git pull` to update, or switch branches without re-installing:
```
python -m pip install -e .
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/envs` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9 and v3.10.
