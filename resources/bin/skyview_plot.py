#!/usr/bin/env python
"""An example script demonstrating a skyview plot
"""
from skyline.example_data import examples
from matplotlib import gridspec
from matplotlib import colors as mplcol
from matplotlib.backends import backend_agg
from skyline import (
    figure,
    coords,
    utils,
    styles,
    colors,
    constants as con
)
from skyline.tracks import scatter, labels, linkers
from ensembl_rest_client import client
import os
import re
import pprint as pp


label_genes = [
    ('ENSG00000162458', 'FBLIM1', '1', 16083102, 16113089, 1, 13089),
    ('ENSG00000154358', 'OBSCN', '1', 228395831, 228566577, 1, 16577),
    ('ENSG00000232823', 'TXNP1', '10', 121442639, 121443335, -1, -6665),
    ('ENSG00000152268', 'SPON1', '11', 13983914, 14289646, 1, -16086),
    ('ENSG00000204842', 'ATXN2', '12', 111890018, 112037480, -1, 37480),
    ('ENSG00000230373', 'GOLGA6L5P', '15', 85051116, 85060045, -1, 1116),
    ('ENSG00000132376', 'INPP5K', '17', 1397865, 1420182, -1, -2135),
    ('ENSG00000262879', 'RP11-156P1.3', '17', 45061411, 45177689, -1, 27689),
    ('ENSG00000134775', 'FHOD3', '18', 33877677, 34360018, 1, 60018),
    ('ENSG00000049759', 'NEDD4L', '18', 55711599, 56068772, 1, 68772),
    ('ENSG00000011478', 'QPCTL', '19', 46195741, 46207247, 1, -4259),
    ('ENSG00000171303', 'KCNK3', '2', 26915619, 26956288, 1, 6288),
    ('ENSG00000079156', 'OSBPL6', '2', 179059208, 179264160, 1, 64160),
    ('ENSG00000099956', 'SMARCB1', '22', 24129150, 24176703, 1, -20850),
    ('ENSG00000255021', 'RP11-536I6.2', '3', 14313873, 14345345, -1, -4655),
    ('ENSG00000222719', 'AC116038.2', '3', 38841787, 38841885, -1, -8115),
    ('ENSG00000168386', 'FILIP1L', '3', 99548985, 99833357, -1, 83357),
    ('ENSG00000174891', 'RSRC1', '3', 157823644, 158263519, 1, 63519),
    ('ENSG00000007062', 'PROM1', '4', 15964699, 16086001, -1, -13999),
    ('ENSG00000120725', 'SIL1', '5', 138282409, 138629246, -1, 29246),
    ('ENSG00000204531', 'POU5F1', '6', 31132119, 31148508, -1, -1492),
    ('ENSG00000128596', 'CCDC136', '7', 128430811, 128462186, 1, 12186),
    ('ENSG00000180938', 'ZNF572', '8', 125985540, 125991631, 1, -8369),
    ('ENSG00000179950', 'PUF60', '8', 144898514, 144912029, -1, -1486)
]


OUTFILE = os.path.join(os.environ['HOME'], 'skyview_scatter.png')
"""Output to the root of the home directory (`str`)
"""

# ##### FIGURE ARGUMENTS ######
FIG_SIZE_MULTIPLIER = 1.5
"""Easily scale the figure size and keep the aspect ratio (`float`)
"""
FIG_WIDTH = 2 * FIG_SIZE_MULTIPLIER
"""The width of the figure in inches (`float`)
"""
FIG_HEIGHT = 3 * FIG_SIZE_MULTIPLIER
"""The height of the figure in inches (`float`)
"""
FIG_SIZE = (FIG_WIDTH, FIG_HEIGHT)
"""The figure size (width, height), (`tuple` (width, height))
"""
FIG_DPI = 150
"""The figure resolution (`int`)
"""
NCOLS = 1
"""The number of columns in the genomic tracks (`int`)
"""
WSPACE = 0
HSPACE = 0.05

# ##### GENERAL ARGUMENTS ######
ALPHA = 0.05
PVALUE_COL = 'pvalue'
XPAD_LEFT = 0.14
"""This extends the left x-lim by 0.14 of the entire coordinate range given by
the coordinates object. This is a fudge to adjust the left margin as that is
currently not possible to do in matplotlib. Note this can also be specified
in absolute base-pairs as well (`float` or `int`).
"""

# ##### REST CLIENT ARGUMENTS ######
REST_URL = "https://grch37.rest.ensembl.org"
BIOTYPYES = ['protein_coding']
SPECIES = 'human'


# ##### NEAREST GENE LABEL TRACK ARGUMENTS ######
# Th heights in the grid coordinates for the various tracks
NG_LABEL_TRACK_HEIGHT = 2
"""The label track height of nearest gene information (`int`)
"""
NG_LABEL_FONTSIZE = 4
NG_LABEL_FONTSTYLE = 'italic'
NG_LABEL_ROTATION = 90
NG_LABEL_HA = "center"
NG_LABEL_STYLE = dict(
    color=colors.CHR_LIGHT_GREY2,
    rotation=NG_LABEL_ROTATION,
    fontstyle=NG_LABEL_FONTSTYLE,
    fontsize=NG_LABEL_FONTSIZE,
    ha=NG_LABEL_HA
)


# ##### LINKER TRACK PARAMETERS ######
LINKER_HEIGHT = 1
LINKER_LINE_STYLE = 'dashed'
LINKER_LINE_ALPHA = 1.0
LINKER_LINE_WIDTH = 0.5
LINKER_TRACK_STYLE = dict(
    color=colors.CHR_LIGHT_GREY2,
    alpha=LINKER_LINE_ALPHA,
    linewidth=LINKER_LINE_WIDTH,
    linestyle=LINKER_LINE_STYLE
)


# ##### SCATTER ARGUMENTS ######
SKYVIEW_HEIGHT = 1
"""The height of each skyview scatter plot (`int`)
"""
DEFAULT_CHR_PAD = 1E6
"""The number of base pairs to extend each chromosome by in the coordinates
object. This is used primarily to pad each chromosome on dense genome-wide
plots to stop the points from one chromosome over-plotting the points from
another chromosome (`int`).
"""
MARKER = '.'
MARKER_SIZE = 0.5
CMAP_MIN = 0
CMAP_MAX = 300
CMAP_BREAKS = [(15.301, '#FEAE24'), (10.301, '#F04424'), (7.301, '#E14228')]

# Sort out the colours for the chromosome scatter plot
BASE_CHR_STYLE = dict(
    marker=MARKER, s=MARKER_SIZE, edgecolor=None
)
ODD_CHR_STYLE = {
    **BASE_CHR_STYLE,
    **dict(c=mplcol.to_rgba(colors.CHR_BLUE1, alpha=ALPHA))
}
EVEN_CHR_STYLE = {
    **BASE_CHR_STYLE,
    **dict(c=mplcol.to_rgba(colors.CHR_BLUE2, alpha=ALPHA))
}
# This adds the colours for the significant hits
utils.add_listed_cmap(ODD_CHR_STYLE, CMAP_MIN, CMAP_MAX, CMAP_BREAKS)
utils.add_listed_cmap(EVEN_CHR_STYLE, CMAP_MIN, CMAP_MAX, CMAP_BREAKS)

JITTER = [
    (0, 0.1), (1.3, 0.15), (2.3, 0.5), (4.3, 0.8), (5.3, 0.85), (8.3, 0.9),
    (10.3, 1.0)
]
"""The breakpoints for the extent of jittering, so more significant variants
can jitter more. You can also pass the number of jitter breakpoints and they
will be applied evenly across the dataset range. However, this is recommended
if you are plotting multiple datasets. In this case anything above -log10(P)
10.3 will have maximum jitter.
"""

SV_LABEL_PAD = 0
SV_LABEL_ROTATION = 0
SV_LABEL_FONT_SIZE = 4
SV_LABEL_FONT_WEIGHT = 'bold'
SV_LABEL_VERTICAL_ALIGN = 'center'
SV_LABEL_HORIZONTAL_ALIGN = 'left'
SV_LABEL_STYLE = dict(
    labelpad=SV_LABEL_PAD,
    rotation=SV_LABEL_ROTATION,
    fontsize=SV_LABEL_FONT_SIZE,
    fontweight=SV_LABEL_FONT_WEIGHT,
    va=SV_LABEL_VERTICAL_ALIGN,
    ha=SV_LABEL_HORIZONTAL_ALIGN
)


CHR_LABEL_HEIGHT = 1
CHR_LABEL_ROWS = 2

# ##### CHROMOSOME LABEL PARAMETERS ######
CHR_LABEL_ROTATION = 0
CHR_LABEL_FONT_SIZE = 4
CHR_LABEL_HORIZONTAL_ALIGN = 'center'
CHR_LABEL_STYLE = dict(
        rotation=CHR_LABEL_ROTATION,
        fontsize=CHR_LABEL_FONT_SIZE,
        ha=CHR_LABEL_HORIZONTAL_ALIGN
    )



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    # Get the example data to plot and some labels for it
    paired_gwas_labels = examples.get_data("paired_gwas_labels")
    paired_gwas = examples.get_data("paired_gwas")

    # Create the main figure object and set it's canvas backend properly
    gfig = figure.GenomicFigure(figsize=FIG_SIZE, dpi=FIG_DPI)
    # AttributeError: 'FigureCanvasBase' object has no attribute 'get_renderer'
    gfig.set_canvas(backend_agg.FigureCanvas(figure=gfig))

    # The row position of the first track
    offset = 0

    # The number of rows in our plot
    nrows = (
        NG_LABEL_TRACK_HEIGHT + LINKER_HEIGHT + CHR_LABEL_HEIGHT +
        (len(paired_gwas) * SKYVIEW_HEIGHT)
    )

    # Now set up the genomic tracks in the grid, these will hold the component
    # parts of the plot (Axes)
    genomic_tracks = gridspec.GridSpec(nrows + 1, NCOLS, wspace=WSPACE,
                                       hspace=HSPACE)

    # Set up the coordinate system
    genomic_coords = coords.HumanGRCh37(default_pad=DEFAULT_CHR_PAD)
    genomic_coords.set_all_inactive()

    # Get all the chromosomes represented in the data, I know this data has the
    # same numbers of chromosomes but if you didn't you might want to loop
    # through and intersect the unique from each one
    represented_chrs = paired_gwas[0][con.CHR_NAME].unique()
    genomic_coords.set_active(represented_chrs)

    # ##### Add a nearest gene track #####
    # Get some labels for the nearest genes
    # rest_client = client.Rest(url=REST_URL)
    # top_hits = utils.get_top_assocs(*paired_gwas, index_ascending=True)
    # label_genes = utils.get_nearest_genes(rest_client, SPECIES, top_hits)
    label_text = [(i[2], i[3], i[4], i[1]) for i in label_genes]
    gene_labels = labels.Labels(
        gfig, genomic_tracks[offset:offset+NG_LABEL_TRACK_HEIGHT],
        genomic_coords, label_text, xpad_left=XPAD_LEFT,
        label_format=NG_LABEL_STYLE
    )
    offset += NG_LABEL_TRACK_HEIGHT
    gfig.add_track(gene_labels)

    # ##### Add linker track ######
    # This separates and links the nearest gene labels to the plot peaks
    linker = linkers.LinkerTrack(
        gfig, genomic_tracks[offset:offset+LINKER_HEIGHT], genomic_coords,
        gene_labels.get_label_coords(), xpad_left=XPAD_LEFT,
        default_line=LINKER_TRACK_STYLE
    )
    offset += LINKER_HEIGHT
    gfig.add_track(linker)

    # ##### Generate the label track ######
    sky_scatter_tracks = []

    for i in range(0, len(paired_gwas), 1):
        print(paired_gwas_labels[i])
        gwas_label = re.sub(
            r'\s*-\s*', '\n', paired_gwas_labels[i]
        )
        sky_scatter = scatter.GenomicSkyviewScatter(
            gfig,
            genomic_tracks[offset:offset+SKYVIEW_HEIGHT],
            genomic_coords,
            paired_gwas[i],
            PVALUE_COL,
            jitter_breaks=JITTER,
            chromosome_style=(ODD_CHR_STYLE, EVEN_CHR_STYLE),
            data_label=(gwas_label, SV_LABEL_STYLE),
            # This only works when there is no xaxis plotted
            # as you can't set the margin limits independently in
            # matplotlib
            xpad_left=XPAD_LEFT
        )
        offset += SKYVIEW_HEIGHT
        sky_scatter_tracks.append(sky_scatter)

    for j in sky_scatter_tracks:
        gfig.add_track(j)

    # ##### Add a chromosome label track ######
    chr_labels = labels.ChrLabels(
        gfig,
        genomic_tracks[offset:offset+CHR_LABEL_HEIGHT],
        genomic_coords,
        label_format=[CHR_LABEL_STYLE],
        rows=CHR_LABEL_ROWS,
        verticalalignment='top',
        xpad_left=XPAD_LEFT
    )
    gfig.add_track(chr_labels)

    gfig.savefig(OUTFILE, bbox_inches='tight')
