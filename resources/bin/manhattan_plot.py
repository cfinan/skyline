#!/usr/bin/env python
import matplotlib
# matplotlib.use('agg', force=True)

from skyline.example_data import examples
from matplotlib import gridspec
from matplotlib.backends import backend_agg
# from ensembl_rest_client import client, utils as erc_utils
from skyline import (
    figure as skyfig,
    scatter,
    coords,
    utils,
    labels,
    linkers,
    constants as con
)
import pandas as pd
import os
import pprint as pp
# rc = client.Rest(url='https://grch37.rest.ensembl.org')

test_gwas = examples.get_data("downscaled_gwas")
test_labels = examples.get_data("test_labels")
species = 'human'
multiplier = 1.5
figsize = [10.4 * multiplier, 8.8 * multiplier]
dpi = 100
nrows = 20
ncols = 1
size = 1
size_offset = 10
default_pad = 1E7
marker = "."
col1 = '#52525203'
col2 = '#B3B3B303'
locus_width = 1E6
# green = '#00CD66'
# limits = [(10.301, '#9464AA')]
limits = [(8.301, '#9464AA')]
gfig = skyfig.GenomicFigure(figsize=figsize, dpi=dpi)

# AttributeError: 'FigureCanvasBase' object has no attribute 'get_renderer'
gfig.set_canvas(backend_agg.FigureCanvas(figure=gfig))

LABEL_FORMAT = dict(
    rotation=90,
    fontstyle='italic',
    color='#B3B3B3',
    horizontalalignment='center'
)
LINE_FORMAT = dict(
    color='#B3B3B3', alpha=0.5, linewidth=0.5
)

roi = [
    dict(
        chr_name='2', pos=21350000, locus_width=locus_width, c="#00CD66",
        s=size+size_offset, marker='.'
    ),
    dict(
        chr_name='5', pos=74550000, locus_width=locus_width, c="#008080",
        s=size+size_offset, marker='.'
    )
]

marker_styles = [
    utils.cmap_style_update(
        test_gwas, dict(marker=marker, c=col1, s=size, edgecolor=None), limits=limits
    ),
    utils.cmap_style_update(
        test_gwas, dict(marker=marker, c=col2, s=size, edgecolor=None), limits=limits
    )
]

genomic_tracks = gridspec.GridSpec(
    nrows, ncols, wspace=0, hspace=0
)
# gfig = skyfig.GenomicFigure(figsize=figsize, dpi=dpi)

b37_coords = coords.HumanGRCh37(default_pad=default_pad)
b37_coords.set_inactive(test_gwas)

label_args = []
for i in test_labels.itertuples():
    label_args.append(
        (i.chr_name, i.start_pos, i.start_pos, False, i.gene_external_name)
    )

label_track = labels.LabelTrack(
    gfig, genomic_tracks[0:2], b37_coords, label_args,
    label_format=LABEL_FORMAT
)

linker_track = linkers.LinkerTrack(
    gfig, genomic_tracks[2:3], b37_coords, label_track.get_label_coords(),
    default_line=LINE_FORMAT
)

scatter_track = scatter.GenomewideScatter(
    gfig,
    genomic_tracks[3:18],
    test_gwas,
    b37_coords,
    marker_styles=marker_styles
)
scatter_track.add_roi(roi)

# Add some down lines
x = []
ymin = []
ymax = []
for i in test_labels.itertuples():
    # print(i)
    subset = (test_gwas.chr_name == i.chr_name) & (test_gwas.start_pos == i.start_pos)
    pvalue = test_gwas.loc[subset, 'pvalue'].max()
    # print(pvalue)
    plot_start = b37_coords.get_coords(i.chr_name, i.start_pos)
    plot_end = b37_coords.get_coords(i.chr_name, i.start_pos)
    plot_center = utils.get_center(plot_start, plot_end)
    lower, upper = scatter_track.get_ylim()
    # print(lower, upper)
    # print(pvalue)
    if upper != pvalue:
        x.append(plot_center)
        ymin.append(pvalue)
        ymax.append(upper)
        # scatter_track.axvline(
        #     x=plot_center, ymin=pvalue, ymax=1, **LINE_FORMAT
        # )
        # scatter_track.axline(
        #     (plot_center, upper), (plot_center, pvalue), **LINE_FORMAT
        # )
LINE_FORMAT['linestyle'] = 'dashed'
scatter_track.vlines(x, ymin, ymax, **LINE_FORMAT)


# scatter_track.invert_yaxis()

chr_labels = labels.ChrAxis(
    gfig,
    genomic_tracks[18:19],
    b37_coords,
    rows=1,
    offset=0.5
)

gfig.add_track(label_track)
gfig.add_track(linker_track)
gfig.add_track(scatter_track)
gfig.add_track(chr_labels)

# pp.pprint(label_track.labels)
gfig.savefig(
    os.path.join(os.environ['HOME'], 'skyline_scatter.png'),
    bbox_inches='tight'
)


# print(test_gwas.shape)
# top_hits = utils.get_top_assocs(
#     test_gwas
# )
# print(top_hits)
# print(top_hits.shape)

# nearest_genes = []
# for chr_name, start_pos in top_hits[[con.CHR_NAME, con.START_POS]].itertuples(
#         index=False
# ):
#     print(chr_name, start_pos)
#     ng = erc_utils.get_nearest_gene(
#         rc, species, chr_name, start_pos, biotypes=['protein_coding']
#     )
#     nearest_genes.append(
#         dict(
#             chr_name=chr_name,
#             start_pos=start_pos,
#             gene_chr_name=ng['seq_region_name'],
#             gene_start_pos=ng['start'],
#             gene_end_pos=ng['end'],
#             ensembl_gene_is=ng['gene_id'],
#             gene_external_name=ng['external_name'],
#             gene_biotype=ng['biotype'],
#             gene_desc=ng['description'],
#             min_dist=ng['min_dist'],
#             abs_min_dist=ng['abs_min_dist']
#         )
#     )

# pd.DataFrame(nearest_genes).to_csv(
#     'test_nearest_genes.txt.gz', index=False, sep="\t"
# )

