#!/usr/bin/env python
"""Examples of style usage
"""
from skyline import styles

null = styles._NullSimpleStyle()
print(null)
# print(null['edgecolor'])
# print(null.mplkwargs)

print("START")
ss1 = styles.SimpleStyle(edgecolor='black')
print(ss1)
# ss2 = styles.SimpleStyle(edgecolor='blue')
# print("GETTING 1")
# print(ss1.edgecolor)
# # print(ss1.parent)

# print("GETTING 2")
# print(ss2.edgecolor)
# # print(ss2.parent)

# ss2.edgecolor = "green"
# print(ss2.edgecolor)
# print(ss1.edgecolor)
# print(ss1.mplkwargs)

# print("mplkwargs" in ss1.parent)
# print("mplkwargs" in ss1)

# print(ss1)

# ss3 = styles.SimpleStyle(parent=ss2)
# print(ss3)
# print(ss3['mplkwargs'])
