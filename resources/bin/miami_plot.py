#!/usr/bin/env python
from skyline.example_data import examples
from matplotlib import gridspec
from skyline import (
    figure as skyfig,
    scatter,
    coords,
    utils,
    labels
)
import os
import pprint as pp

test_gwas = examples.get_data("downscaled_gwas")
multiplier = 1.5
figsize = [10.4 * multiplier, 8.8 * multiplier]
dpi = 100
nrows = 41
ncols = 1
size = 10
size_offset = 10
default_pad = 1E7
marker = "."
col1 = '#6495ED'
col2 = '#00006C'
locus_width = 1E6
# green = '#00CD66'
limits = [(10.301, 'red'), (7.301, '#E14228')]

roi = [
    dict(
        chr_name='2', pos=21350000, locus_width=locus_width, c="#00CD66",
        s=size+size_offset, marker='.'
    ),
    dict(
        chr_name='5', pos=74550000, locus_width=locus_width, c="#008080",
        s=size+size_offset, marker='.'
    )
]

marker_styles = [
    utils.cmap_style_update(
        test_gwas, dict(marker=marker, c=col1, s=size), limits=limits
    ),
    utils.cmap_style_update(
        test_gwas, dict(marker=marker, c=col2, s=size), limits=limits
    )
]

genomic_tracks = gridspec.GridSpec(
    nrows, ncols, wspace=0, hspace=0
)
gfig = skyfig.GenomicFigure(figsize=figsize, dpi=dpi)

scatter_track = scatter.GenomewideScatter(
    gfig,
    genomic_tracks[0:19],
    test_gwas,
    coords.HumanGRCh37(default_pad=default_pad),
    marker_styles=marker_styles
)
scatter_track.add_roi(roi)

chr_labels = labels.ChrAxis(
    gfig,
    genomic_tracks[19:20],
    coords.HumanGRCh37(default_pad=default_pad),
    rows=2,
    offset=-0.7
)

miami_track = scatter.GenomewideScatter(
    gfig,
    genomic_tracks[21:39],
    test_gwas,
    coords.HumanGRCh37(default_pad=default_pad),
    marker_styles=marker_styles
)
miami_track.invert_yaxis()

gfig.add_track(scatter_track)
gfig.add_track(chr_labels)
gfig.add_track(miami_track)
gfig.savefig(os.path.join(os.environ['HOME'], 'miami_scatter.png'), bbox_inches='tight')
