#!/usr/bin/env python
"""How to plot a simple gene
"""
from skyline import figure, coords, styles
from skyline.tracks import genes
from ensembl_rest_client import client
from matplotlib import gridspec, colors
from matplotlib.backends import backend_agg
import os
import pprint as pp


SPECIES = 'human'
FEATURE = 'gene'
CHR_NAME = 13
# START_POS = 32315086
# END_POS = 32400268
START_POS = 31115086
END_POS = 32600268
WIDTH = 10.4
HEIGHT = 2.5
FIGURE_SIZE = [WIDTH, HEIGHT]
DPI = 100
GENE_LENGTH_PAD = 5000
LABEL_HEIGHT = 0.5
PLOT_STRAND = None
# TRACK_COLOURS = ['#D2D2D203', 'none']

odd_track = styles.SimpleStyle(
    facecolor=colors.to_rgba('#D2D2D2', alpha=0.3),
    edgecolor='none'
)
even_track = styles.SimpleStyle(
    facecolor='none', parent=odd_track
)
TRACK_COLOURS = [odd_track, even_track]
# TRACK_COLOURS = [dict(colors.to_rgba('#D2D2D2', alpha=0.3)), 'none']
# GENE_COLOUR = "#00006C"

GENE_COLOUR = "#6495ED"
style = styles.SkylineStyle(
    gene_patch=styles.SimpleStyle(
        facecolor=GENE_COLOUR, edgecolor='black', linewidth=0.5
    )
)
NROWS = 1
NCOLS = 1
GENES = [
    ('13', ((31311289, 31312921),), 1, 'ANKRD26P4', 'processed_pseudogene'),
    ('13', ((31134973, 31162388),), -1, 'HSPH1', 'protein_coding'),
    ('13', ((31063306, 31115699),), 1, 'WDR95P', 'unitary_pseudogene'),
    ('13', ((31199975, 31332276),), 1, 'B3GLCT', 'protein_coding'),
    ('13', ((32303699, 32315363),), -1, 'ZAR1L', 'protein_coding'),
    ('13', ((32384660, 32386108),), 1, 'IFIT1P1', 'processed_pseudogene'),
    ('13', ((32400723, 32428311),), -1, 'N4BP2L1', 'protein_coding'),
    ('13', ((32420390, 32420516),), -1, '13:32420390-32420516', 'snoRNA'),
    ('13', ((32425358, 32432954),), -1, '13:32425358-32432954', 'lncRNA'),
    ('13', ((32477274, 32478841),), 1, 'ATP8A2P2', 'processed_pseudogene'),
    ('13', ((32504506, 32509395),), -1, 'N4BP2L2-IT2', 'lncRNA'),
    ('13', ((31419989, 31437999),), 1, '13:31419989-31437999', 'lncRNA'),
    ('13', ((31483414, 31483699),), -1, '13:31483414-31483699',
     'unprocessed_pseudogene'),
    ('13', ((31739526, 31803389),), 1, 'RXFP2', 'protein_coding'),
    ('13', ((31838752, 31846539),), -1, '13:31838752-31846539', 'lncRNA'),
    ('13', ((31846713, 32299125),), 1, 'FRY', 'protein_coding'),
    ('13', ((31952580, 31953361),), 1, 'EEF1DP3',
     'transcribed_unprocessed_pseudogene'),
    ('13', ((31960325, 31961946),), -1, '13:31960325-31961946', 'TEC'),
    ('13', ((31975167, 31975448),), 1, 'Metazoa_SRP', 'misc_RNA'),
    ('13', ((32025314, 32031639),), -1, 'FRY-AS1', 'lncRNA'),
    ('13', ((32432417, 32538885),), -1, 'N4BP2L2', 'protein_coding'),
    ('13', ((32586452, 32778019),), 1, 'PDS5B', 'protein_coding'),
    ('13', ((32315086, 32400268),), 1, 'BRCA2', 'protein_coding')
]
USE_ENSEMBL = False


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_genes(rc, species, chr_name, start_pos, end_pos):
    """Get all the genes within a region

    Parameters
    ----------

    """
    region = "{0}:{1}-{2}".format(chr_name, start_pos, end_pos)

    genes = []
    for i in rc.get_overlap_region(FEATURE, region, species):
        try:
            name = i['external_name']
        except KeyError:
            # pp.pprint(i)
            name = "{0}:{1}-{2}".format(
                i['seq_region_name'], i['start'], i['end']
            )

        genes.append(
            (i['seq_region_name'], i['start'], i['end'], i['strand'], name,
             i['biotype'])
        )
    return genes


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_gene_bounds(exons):
    """
    """
    start = None
    end = None
    for i in exons:
        try:
            start = min(start, i[0])
            end = max(end, i[1])
        except TypeError:
            start = i[0]
            end = i[1]
    return start, end


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == "__main__":
    gene_coords = GENES
    if USE_ENSEMBL is True:
        rc = client.Rest()
        gene_coords = get_genes(rc, SPECIES, CHR_NAME, START_POS, END_POS)
    # pp.pprint(gene_coords)
    starts = [get_gene_bounds(i[1])[0] for i in gene_coords]
    ends = [get_gene_bounds(i[1])[1] for i in gene_coords]
    min_coord = min(starts)
    max_coord = max(ends)
    # pp.pprint(gene_coords)

    # Initialise the figure
    gfig = figure.GenomicFigure(figsize=FIGURE_SIZE, dpi=DPI)

    # AttributeError: 'FigureCanvasBase' object has no attribute 'get_renderer'
    gfig.set_canvas(backend_agg.FigureCanvas(figure=gfig))

    genomic_tracks = gridspec.GridSpec(
        NROWS, NCOLS, wspace=0, hspace=0
    )

    region = coords.ChrCoords(CHR_NAME, min_coord, max_coord)
    gene_track = genes.GeneTrack(
        gfig, genomic_tracks[0:], region, gene_coords,
        gene_length_pad=GENE_LENGTH_PAD, label_height=LABEL_HEIGHT,
        plot_strand=PLOT_STRAND, gene_track_colors=TRACK_COLOURS,
        style=style
    )
    gfig.add_track(gene_track)

    # pp.pprint(label_track.labels)
    gfig.savefig(
        os.path.join(os.environ['HOME'], 'skyline_genes.png'),
        bbox_inches='tight'
    )
