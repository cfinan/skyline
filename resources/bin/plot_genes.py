#!/usr/bin/env python
"""An example script demonstrating how to plot gene tracks in isolation
"""
from skyline import (
    features,
    figure,
    coords,
    colors,
    ensembl
)
from skyline.patches import genes as gp
from matplotlib import gridspec
from matplotlib.backends import backend_agg
from skyline.tracks import genes
from filecache import filecache
import os
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def plot_genes(plot_genes, outfile, min_gene_rows=1, figure_width=8,
               figure_height=4, figure_dpi=300, debug=False,
               gene_label_height=0.5, xpad=100):
    # Initialise the figure
    gfig = figure.GenomicFigure(
        figsize=(figure_width, figure_height), dpi=figure_dpi
    )
    # AttributeError: 'FigureCanvasBase' object has no attribute 'get_renderer'
    gfig.set_canvas(backend_agg.FigureCanvas(figure=gfig))
    tracks = gridspec.GridSpec(1, 1)

    chr_name = plot_genes[0].chr_name
    min_coord = min([i.start_pos for i in plot_genes])
    max_coord = max([i.end_pos for i in plot_genes])

    chr_coords = coords.ChrCoords(chr_name, min_coord - xpad, max_coord + xpad)

    gt = genes.GeneTrack(
        gfig, tracks[0:1], chr_coords, plot_genes, min_gene_rows=min_gene_rows,
        gene_row_style=[
            dict(edgecolor='none', facecolor='none'),
            dict(edgecolor='none', facecolor=colors.CHR_LIGHT_GREY1),
        ],
        gene_label_height=gene_label_height,
        gene_label_pad=0.08,
        gene_row_pad=0.05,
        # gene_patch=gp.ExonGene,
        # gene_length_pad=1000
    )
    gt.set_frame_on(True)
    gt.spines['bottom'].set_visible(True)
    gt.spines['top'].set_visible(True)
    gt.spines['right'].set_visible(True)
    gt.spines['left'].set_visible(True)
    gt.yaxis.set_visible(True)

    gfig.add_track(gt)
    gfig.savefig(outfile)
    if debug is True:
        gt.print_debug()
    for i in plot_genes:
        i.delete_coords_mapper()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@filecache
def get_genes(rc, locus, flank=100000):
    return ensembl.get_genes_within(
        rc, locus[0], locus[1] - flank, locus[1] + flank, canonical=True
    )


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    chr_name = '10'

    # Manually create some very simple gene objects
    gene1 = features.SimpleTranscript(
        chr_name, 1235000, end_pos=1235576, trans_id=1, strand=1, label="gene1"
    )

    gene2 = features.SimpleTranscript(
        chr_name, 1235200, end_pos=1235576, trans_id=2, strand=1, label="gene2"
    )
    gene2.gene_patch_style = dict(edgecolor="red", linewidth=0.5)

    gene3 = features.SimpleTranscript(
        chr_name, 1235400, end_pos=1235700, trans_id=3, strand=-1, label="gene3"
    )

    gene4 = features.SimpleTranscript(
        chr_name, 1235900, end_pos=1236210, trans_id=4, strand=-1, label="gene4"
    )

    all_genes = [gene1, gene2, gene3, gene4]

    # First we will create a very simple plot of genes with no
    # intron/exon structure
    manual_outfile = os.path.join(
        os.environ['HOME'], "gene_track", "manual_genes_mt{0}_lht{1}.png"
    )

    label_height = 0.5
    for i in list(range(4, 10)) + [50]:
        plot_genes(
            all_genes, manual_outfile.format(i, label_height),
            min_gene_rows=i, debug=False,
            gene_label_height=label_height
        )

    # label_height = 0.2
    # brca2_outfile = os.path.join(
    #     os.environ['HOME'], "gene_track", "brca2.png"
    # )

    # flank = 1000000
    # brca2_locus = ('13', 32315086, 32400268)
    # rc = ensembl.get_human_b38_rest_client()

    # brca2_genes = get_genes(
    #     rc, brca2_locus, flank=flank
    # )
    # pp.pprint(brca2_genes)
    # plot_genes(
    #     brca2_genes, brca2_outfile,
    #     min_gene_rows=10, debug=True,
    #     gene_label_height=label_height
    # )
