#!/usr/bin/env python
"""Plot a locus zoom plot of an associated region
"""
from merit.gwas import dataset
from skyline import figure, coords, styles
from skyline.tracks import genes, scatter, linkers, grids
from skyline.patches import genes as gp
from ensembl_rest_client import client, utils as rcutils
from matplotlib import gridspec, colors
from matplotlib.backends import backend_agg
from merit import config
from merit.genotypes import (
    cohort,
    engine
)
# from merit.mr import mendelian_randomization as mr
from merit.gwas import dataset, utils
# from merit.utils import analysis_tools
# from merit.ipd import bgen_handler, ipd_tools
import numpy as np
import pandas as pd
import pprint as pp
import os

# For testing
# zcat bolt.ARVC.RVEDV.bgen.corrected_PVAL.Excl_0.01MAF.txt.gz | body awk '{if ($1 == "12" && $3 >= 112656415 && $3 <= 113156415) {print $0}}' | gzip > ../test_region.txt.gz
# GWAS_FILE = "/data/gwas/cardiac_mri/original/bolt.ARVC.RVEDV.bgen.corrected_PVAL.Excl_0.01MAF.txt.gz"
# GWAS_FILE = "/data/gwas/cardiac_mri/test_region.txt.gz"
GWAS_FILE = "/scratch/cardiac_mri/test_region.txt.gz"

# Load up the top hits
TOP_HITS_PATH = "/home/rmjdcfi/analysis/CardiacMRI/results/gwas_hits/cmri_top_hits_genes_within_250kbp.txt.gz"

# A loci from the TOP hits file
LOCI_IDX = 9

# Not used
CHR_NAME = '12'
TRAIT = "RVEDV"
FLANK = 250000

# Inches
WIDTH = 10.4
HEIGHT = 10.5
FIGURE_SIZE = [WIDTH, HEIGHT]
DPI = 150
NROWS = 20
NCOLS = 1
GENE_LENGTH_PAD = 5000

# TODO: some sizing inaccuracies with other label heights, compare 0.3
#  with 0.2 etc...
LABEL_HEIGHT = 0.5
GENE_TRACK_PAD = 0.1
TRACK_TOP_PAD = 0
TRACK_BOTTOM_PAD = 0
PLOT_STRAND = None
MIN_TRACKS = 1
# TRACK_COLOURS = ['#D2D2D203', 'none']

ODD_TRACK = styles.SimpleStyle(
    facecolor=colors.to_rgba('#D2D2D2', alpha=0.3),
    edgecolor='none'
)
EVEN_TRACK = styles.SimpleStyle(
    facecolor='none', parent=ODD_TRACK
)
# TRACK_COLOURS = [ODD_TRACK, EVEN_TRACK]
TRACK_COLOURS = [EVEN_TRACK, EVEN_TRACK]

GENE_COLOUR = "#6666B1"
GENE_STYLE = styles.SkylineStyle(
    gene_patch=styles.SimpleStyle(
        facecolor=GENE_COLOUR,
        edgecolor=GENE_COLOUR,
        linewidth=0.5
    )
)
GENE_TRACE = styles.SimpleStyle(
        facecolor='#DBDBDB40',
        edgecolor='#DBDBDB40',
        linewidth=0.5
)

OUTFILE = "~/lz_plot.png"
LEAD_VARIANT = ('12', 112906415, 'G', 'A')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def load_genes(infile, loci_idx, flank=250000):
    """
    """
    rc = client.Rest(url='https://grch37.rest.ensembl.org')

    top_hit_loci = pd.read_csv(
        infile, sep="\t", dtype=dict(gene_chr_name=str)
    )
    # top_hit_loci = top_hit_loci.loc[
    #     (top_hit_loci.genes_within_input_idx == 9) &
    #     (top_hit_loci.gene_chr_name == '12') &
    #     (top_hit_loci.Trait == 'RVEDV')
    # ]
    top_hit_loci = top_hit_loci.loc[
        top_hit_loci.genes_within_input_idx == loci_idx
    ]

    genes = []
    for i in top_hit_loci.itertuples(index=False):
        # genes.append(
        #     (i.gene_chr_name, ((i.gene_start_pos, i.gene_end_pos),),
        #      i.gene_strand, i.external_gene_id, i.gene_biotype)
        # )
        gene_info = rcutils.get_exons(rc, i.ensembl_gene_id)
        exons = []
        for t in gene_info['Transcript']:
            if t['is_canonical'] == 1:
                # canonical = []
                for e in t['Exon']:
                    exons.append((e['start'], e['end']))
                genes.append(
                    (i.gene_chr_name, exons, i.gene_strand,
                     i.external_gene_id, i.gene_biotype)
                )
                break
        # pp.pprint(exons)
    return (
        genes,
        (top_hit_loci.start_pos.unique() - flank)[0],
        (top_hit_loci.start_pos.unique() + flank)[0]
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_gene_bounds(exons):
    """
    """
    start = None
    end = None
    for i in exons:
        try:
            start = min(start, i[0])
            end = max(end, i[1])
        except TypeError:
            start = i[0]
            end = i[1]
    return start, end


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_gwas_data(gwas_file):
    # 1 CHR
    # 2 SNP
    # 3 POS
    # 4 A1
    # 5 A2
    # 6 info
    # 7 maf
    # 8 beta
    # 9 SE
    # 10 P_FIXED
    # 11 TotalN
    indata = dataset.read_gwas(
        gwas_file, "beta", sep=" ", chr_name="CHR", start_pos="POS",
        effect_allele="A1", other_allele="A2", effect_size="beta",
        standard_error="SE", pvalue="P_FIXED", drop_palindromic_alleles=False,
        warning=False
    )
    return indata


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_ld_matrix():
    # Generate an LD matrix
    # ukbb_cohort = ipd_tools.Cohort(
    #     name='ukbb',
    #     samples=None,
    #     files={
    #         '12': '/data/ukbb/bgen/ukb_imp_chr12_v3_qc2_1.bgen',
    #         '17': '/data/ukbb/bgen/ukb_imp_chr17_v3_qc2_1.bgen',
    #         '20': '/data/ukbb/bgen/ukb_imp_chr20_v3_qc2_1.bgen'
    #     },
    #     file_type='bgen'
    # )
    # Get a cohort of samples for LD calc
    # ld_cohort = bgen_handler.get_bgen_ld_cohort(
    #     ukbb_cohort, sample_size=5000, seed=1984
    # )
    # corrmat, stats = bgen_handler.get_bgen_corr(
    #     ld_cohort,
    #     regions=indata.index.unique().tolist(),
    #     mask=True
    # )
    # ld_matrix = np.square(corrmat)
    # ld_matrix.to_csv("~/ld_matrix.txt", sep="\t")
    return pd.read_csv("~/ld_matrix.txt", sep="\t").set_index('uni_id')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_boolean_matrix():
    # Generate an LD matrix
    # ukbb_cohort = ipd_tools.Cohort(
    #     name='ukbb',
    #     samples=None,
    #     files={
    #         '12': '/data/ukbb/bgen/ukb_imp_chr12_v3_qc2_1.bgen',
    #         '17': '/data/ukbb/bgen/ukb_imp_chr17_v3_qc2_1.bgen',
    #         '20': '/data/ukbb/bgen/ukb_imp_chr20_v3_qc2_1.bgen'
    #     },
    #     file_type='bgen'
    # )
    # Get a cohort of samples for LD calc
    # ld_cohort = bgen_handler.get_bgen_ld_cohort(
    #     ukbb_cohort, sample_size=5000, seed=1984
    # )
    # corrmat, stats = bgen_handler.get_bgen_corr(
    #     ld_cohort,
    #     regions=indata.index.unique().tolist(),
    #     mask=True
    # )
    # ld_matrix = np.square(corrmat)
    # ld_matrix.to_csv("~/ld_matrix.txt", sep="\t")
    bool_data = pd.read_csv("~/boolean_matrix.txt", sep="\t").set_index('category')
    bool_data[bool_data.columns] = bool_data[bool_data.columns].astype(bool)
    return bool_data


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == "__main__":
    gwas_data = get_gwas_data(GWAS_FILE)
    gwas_data['pvalue'] = -np.log10(gwas_data['pvalue'])
    # print(gwas_data)

    ld_matrix = get_ld_matrix()
    print(ld_matrix)

    bool_matrix = get_boolean_matrix()

    gene_coords, region_start, region_end = load_genes(
        TOP_HITS_PATH, LOCI_IDX
    )
    pp.pprint(gene_coords)
    starts = [get_gene_bounds(i[1])[0] for i in gene_coords]
    ends = [get_gene_bounds(i[1])[1] for i in gene_coords]
    min_coord = min(starts)
    max_coord = max(ends)
    pp.pprint(gene_coords)
    # Initialise the figure
    gfig = figure.GenomicFigure(figsize=FIGURE_SIZE, dpi=DPI)

    # AttributeError: 'FigureCanvasBase' object has no attribute 'get_renderer'
    gfig.set_canvas(backend_agg.FigureCanvas(figure=gfig))

    genomic_tracks = gridspec.GridSpec(
        NROWS, NCOLS, wspace=0, hspace=0
    )

    region = coords.ChrCoords(CHR_NAME, region_start, region_end)
    # region = coords.ChrCoords(CHR_NAME, min_coord, max_coord)

    lz_scatter = scatter.LocusZoomScatter(
        gfig, genomic_tracks[1:9], region, gwas_data, 'pvalue',
        LEAD_VARIANT, ld_matrix
    )
    gfig.add_track(lz_scatter)
    print("X-axis offset text: {0}".format(lz_scatter.xaxis.get_offset_text()))
    print("Text object text: {0}".format(lz_scatter.xaxis.get_offset_text().get_text()))
    lz_scatter.xaxis.get_offset_text().set_text("bollocks")
    print("Text object text: {0}".format(lz_scatter.xaxis.get_offset_text().get_text()))
    # genomic_tracks[16:]
    gene_track = genes.GeneTrack(
        gfig, genomic_tracks[10:13], region, gene_coords,
        gene_length_pad=GENE_LENGTH_PAD,
        label_height=LABEL_HEIGHT,
        gene_track_pad=GENE_TRACK_PAD,
        track_top_pad=TRACK_TOP_PAD,
        track_bottom_pad=TRACK_BOTTOM_PAD,
        plot_strand=PLOT_STRAND,
        gene_track_colors=TRACK_COLOURS,
        gene_patch=gp.ExonGene,
        style=GENE_STYLE, min_tracks=MIN_TRACKS
    )
    gene_track.xaxis.set_visible(False)
    gene_track.spines['bottom'].set_visible(False)
    gene_track.add_gene_traces(GENE_TRACE, direction='down')
    gfig.add_track(gene_track)

    linker = linkers.PosToUniformLinkerTrack(
        gfig, genomic_tracks[13:15], region,
        [(i[0], i[1]) for i in gene_track.get_gene_bounds()],
        uniform_order='start'
    )
    gfig.add_track(linker)
    # Need to work out why this is not plotted
    # 'RPH3A'
    col_order = [
        'HECTD4', 'RP3-521E19.2', 'RN7SKP71', 'RPL7AP60', 'RP3-521E19.3',
        'RPL6', 'PTPN11', 'MIR1302-1'
    ]
    bool_matrix = bool_matrix.loc[:, col_order]
    print(bool_matrix)

    bool_grid = grids.BooleanGrid(
        gfig, genomic_tracks[15:], region, bool_matrix
    )
    gfig.add_track(bool_grid)
    gfig.savefig(
        os.path.join(os.environ['HOME'], 'lz_plot.png'),
        bbox_inches='tight'
    )
