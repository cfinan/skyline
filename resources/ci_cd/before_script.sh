#!/bin/sh
# This is assumed to be running from the root of the repo
echo "[before] running in: $PWD"
# For debugging
echo "CI_COMMIT_BRANCH: "$CI_COMMIT_BRANCH
echo "CI_DEFAULT_BRANCH: "$CI_DEFAULT_BRANCH
python --version
git branch

# Needed for Sphinx (make in build-base) and pysam (build-base + others)
apk add build-base bzip2-dev zlib-dev xz-dev openblas-dev linux-headers

# Package requirements
pip install -r requirements.txt

# Sphinx doc building requirements
pip install -r resources/ci_cd/sphinx.txt
# This seems to be really tricky to get working, I will remove but putting
# at the end seems to be the only way
pip install python3-wget

# Install the package being built/tested
pip install .
