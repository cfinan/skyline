jupyter>=1.*
matplotlib==3.3.1
numpy>=1.18, <1.24
pandas>=2
pytest>=5.*
pytest-dependency
python3-wget
pysam
git+https://gitlab.com/cfinan/ensembl-rest-client.git@master#egg=ensembl_rest_client
